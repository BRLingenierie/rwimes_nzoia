# use roxygen2:
# https://cran.r-project.org/web/packages/roxygen2/vignettes/rd.html
# run roxygen2::roxygenise() to generate documentation

###################################################################
###                                                             ###
####                   generateAdvisory                        ####
###          script to generate flood watch bulletins           ###
###                                                             ###
###################################################################
## Important !
## All variables used in rnw files must be declared in the Global.Env using assign()
##################################################################

#' Helper fonction used to clean intermediate files
remove = function(file){
  if ((substr(file,1,8) != "bulletin")|(substr(file,nchar(file)-3,nchar(file))!=".pdf")) {
    file.remove(file)
  }
}

#' Generate flood daily bulletin in the form of pdf file
#' 
#' @description
#' The script generates several folders below "./":
#' 
#' - ./advisories/: contains all the generated advisories
#' 
#' - ./advisories/YYYY/MM/DD: contains the advisory of the day
#' 
#' LateX template file bulletin.rnw is expected to present in "./" :
#' 
#' - in rserver docker container : /home/rstudio
#' 
#' - in host : corresponds to the mounted volume <app directory>/files/rserver
#' 
#' Default directory is set to /home/rstudio to ensure output pdf sharing with docker host.
#' 
#' The output pdf bulletin is also copied into the /advisories folder
#' 
#' Maximum 6 comments, but less can be used in wimes scripts.
#' 
#' @param comment1 comment to insert in the bulletin
#' @param comment2 comment to insert in the bulletin
#' @param comment3 comment to insert in the bulletin
#' @param comment4 comment to insert in the bulletin
#' @param comment5 comment to insert in the bulletin
#' @param comment6 comment to insert in the bulletin
#' 
#' @export
#' 
generateAdvisory = function(comment1 = "", 
                            comment2 = "", 
                            comment3 = "", 
                            comment4 = "", 
                            comment5 = "", 
                            comment6 = ""){
  
  library(rwimes)
  library(tools)
  
  # IMPORTANT! function local variable and parameter must be
  # assigned in the GlobalEnv to be used by Sweave
  assign("comment_1", comment1, envir = .GlobalEnv)
  assign("comment_2", comment2, envir = .GlobalEnv)
  assign("comment_3", comment3, envir = .GlobalEnv)
  assign("comment_4", comment4, envir = .GlobalEnv)
  assign("comment_5", comment5, envir = .GlobalEnv)
  assign("comment_6", comment6, envir = .GlobalEnv)
  
  logInfo("Starts function generateAdvisory()")
  
  ###################################################################
  # INITIALIZE CONSTANTS AND CREATE FOLDERS IF NEEDED
  
  baseFolder = "/home/rstudio"
  setwd(baseFolder)
  # create "./advisories/" folder if not exists
  advisoriesFolder <- paste(baseFolder, "advisories", sep='/')
  #dir.create(pdfFolder, showWarnings = FALSE)
  
  # get current UTC date
  now <- as.POSIXlt(Sys.time(), "GMT")
  
  # create /advisories/YYYY/MM/DD directory
  year <- format(now, "%Y")
  pdfFolder <- paste(advisoriesFolder, year, sep="/")
  dir.create(pdfFolder, showWarnings = FALSE)
  
  month <- format(now, "%m")
  pdfFolder <- paste(pdfFolder, month, sep="/")
  dir.create(pdfFolder, showWarnings = FALSE)
  
  day <- format(now, "%d")
  pdfFolder <- paste(pdfFolder, day, sep="/")
  # pdfFolder in now equals to .../advisories/YYYY/MM/DD
  dir.create(pdfFolder, showWarnings = FALSE)
  
  
  ###################################################################
  # FIND RNW FILE
  rnwFile<-paste(advisoriesFolder,"template","bulletin.rnw",sep="/")
  logInfo(paste("using template file",rnwFile))
  if ( !file.exists(rnwFile)){
    stop(paste("template file",rnwFile,"does not exist"))
  }
  # Copy rnw file to pdfFolder
  file.copy(rnwFile, pdfFolder, overwrite = TRUE)
  # Copy existence of rnw file 
  rnwFile <- paste(pdfFolder, "bulletin.rnw", sep="/")
  if ( !file.exists(rnwFile)){
    stop(paste(rnwFile, "is missing"))
  }
  
  logInfo(paste(rnwFile, "successfully copied"))
  
  texFile <- paste(pdfFolder, "bulletin.tex", sep="/")
  
  
  ###################################################################
  # HERE INSERT CODE TO RETRIEVE DATA FROM WIMES
  
  # IMPORTANT! In order to be used in Sweave,
  # use assign to set varible in the GlobaEnv, example:
  # assign("myVar", value, envir = .GlobalEnv)
  
  ###################################################################
  # CREATE PDF FILE
  
  updateProgression(10, "Creating lateX file")
  
  # Set pdfFolder as current directory
  setwd(pdfFolder)
  
  print(rnwFile)
  print(texFile)
  
  # generate lateX file
  tryCatch({
    logInfo(paste("Creating lateX file in", pdfFolder))
    Sweave(rnwFile,output=texFile)
    logInfo(paste(texFile, "successfully created"))
  }, error = function(e) {
    lapply(list.files("./"), remove)
    setwd(baseFolder)
    stop(paste("Error on creating lateX file", texFile))
  })
  
  updateProgression(50, "Creating PDF file")
  
  #print(getwd())
  #print(rnwFile)
  #print(texFile)
  
  # convert to pdf
  tryCatch({
    logInfo(paste("Creating PDF file in", pdfFolder))
    tools::texi2pdf(texFile)
    logInfo("PDF file successfully created")
  }, error = function(e) {
    lapply(list.files("./"), remove)
    setwd(baseFolder)
    stop("Error on creating PDF file")
  })
  
  
  ###################################################################
  # CLEAN USELESS FILES
  
  updateProgression(80, "Cleaning output directory")
  
  lapply(list.files("./"), remove)
  
  ###################################################################
  # COPY BULLETIN TO ADVISORIES FOLDER
  
  pdfFile <- paste(pdfFolder, "bulletin.pdf", sep="/")
  bulletin_date <- paste0("bulletin_",strftime(now,"%Y%m%d_%H%M"),".pdf")
  
  # copy file locally with timestamp
  file.copy(pdfFile,paste(pdfFolder,bulletin_date,sep="/"))
  
  #advisoriesFolder <- paste(baseFolder, "advisories", sep="/")
  pdfFileCopy <- paste(advisoriesFolder,"bulletin.pdf",sep="/")
  file.copy(pdfFile, pdfFileCopy, overwrite = TRUE)
  #print(pdfFile)
  

  
  if ( !file.exists(pdfFileCopy)){
    setwd(baseFolder)
    stop(paste("bulletin.pdf can't be copied into ", advisoriesFolder))
  }
  
  logInfo(paste("bulletin.pdf successfully copied to", advisoriesFolder))
  
  setwd(baseFolder)
  
  return(TRUE)
}