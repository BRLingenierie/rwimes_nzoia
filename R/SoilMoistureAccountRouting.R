# Created BY Nicolas GEHENIAU BRLi 07 jUNE 2019

#v3 : correction of the function getEvapSoilLayers. 

# |_| MAIN FUNCTION ----

#' Run the SMAR model. SMAR is the Soil Moisture Accounting and Routing model developped in Ireland. 
#'
#' @param dates POSIXct vector
#' @param rainfall numeric vector
#' @param ETP numeric vector
#' @param paramSMAR object created by the function "createParamSMAR"
#' @param initStates object created by the function "createInitStates"
#' @param outVerbose logical - if true, more messages are sent to the console. TODO: use the logInfo function of the WIMES framework
#'
#' @export
#' 
#' @description SMAR is the model used by the Kenya Met Department for the 3-day flow forecast of the Nzoia basin.
#' The SMAR model is included wihtin the GALWAY software (GFFS - Galway flow forecasting system)
#'
runSMAR=function(dates, rainfall, ETP, paramSMAR, initStates, outVerbose = F){
  #TODO later: verify the format of input data
  excessRain = getExcessRainfall(rainfall, ETP, paramSMAR["T"])
  nsteps=length(rainfall)
  adjustedETP = paramSMAR["T"] * ETP # multiply the ETP by the "Ratio of potential evapotranspiration to pan evaporation" called T
  
  # Initialize main time variables ----
  r1_directRunoff         = rep(0, length(rainfall)) #create a vector of same length as 'rainfall', filled with zero values
  infiltRunoff            = rep(0, length(rainfall))
  r2_excessInfilCapRunoff = rep(0, length(rainfall))
  r3_excessSoilCapMoist   = rep(0, length(rainfall))
  g_excessSoilCapMoist    = rep(0, length(rainfall))
  rg_groundwaterInflow    = rep(0, length(rainfall))
  Q_gwOutflow             = rep(0, length(rainfall))
  N_soillayers            = ceiling(paramSMAR["Z"]/25) # Max number of soil layers (integer)
  Z_soilLayersMoisture    = matrix(0, nrow=length(rainfall), ncol=N_soillayers)
  Rinf_soilLayers         = matrix(0, nrow=length(rainfall), ncol=N_soillayers)
  Evap_soilLayers         = matrix(0, nrow=length(rainfall), ncol=N_soillayers)
  Evap_coeffs             = c(1:N_soillayers)-1
  Evap_coeffs             = paramSMAR["C"]^Evap_coeffs
  maxSoilLayersCap        = getMaxSoilLayersCap(N_soillayers, paramSMAR["Z"])
  
  # Initial states ----
  Z_soilLayersMoisture[1,] = initStates$Z
  
  # Start simulation: for loop ----
  for (i in 2:nsteps) {
    soilSat = getSoilSat(maxSoilLayersCap, Z_soilLayersMoisture[i-1,])
    directRunoffCoeff = getDirectRunoffCoeff(soilSat, paramSMAR["H"]) # also called the parameter H' (H prime)
    r1_directRunoff[i] = excessRain[i] * directRunoffCoeff
    infiltRunoff[i] = excessRain[i] * (1 - directRunoffCoeff)
    r2_excessInfilCapRunoff[i] = getExcessInfilCapRunoff(infiltRunoff[i], paramSMAR["Y"])
    
    # .. compute soil layer evaporation ----
    Evap_soilLayers[i,] = getEvapSoilLayers(Evap_coeffs, N_soillayers, adjustedETP[i], rainfall[i],
                                            Z_soilLayersMoisture[i-1,],
                                            maxSoilLayersCap )
    # .. compute soil moisture infiltration ----
    Rinf_soilLayers[i,] = getInfiltrationFromShallowerLayer(Evap_soilLayers[i,], Z_soilLayersMoisture[i-1,],
                                                            N_soillayers, maxSoilLayersCap,
                                                            infiltRunoff[i], paramSMAR["Y"])
    
    # .. compute soil moisture storage ----
    Z_soilLayersMoisture[i,] = getSoilLayersMoistStorage(Z_soilLayersMoisture[i-1,],
                                                         Evap_soilLayers[i,],
                                                         Rinf_soilLayers[i,],
                                                         maxSoilLayersCap,
                                                         N_soillayers)
    
    
    
  }
  
  # Compute moisture in excess of soil capacity ----
  g_excessSoilCapMoist = getGWexcessSoilCapMoist(g_excessSoilCapMoist,
                                                 Z_soilLayersMoisture,
                                                 Rinf_soilLayers,
                                                 Evap_soilLayers,
                                                 maxSoilLayersCap)
  
  r3_excessSoilCapMoist = (1-paramSMAR["G"]) * g_excessSoilCapMoist
  
  # Compute groundwater routing component ----
  rg_groundwaterInflow = (paramSMAR["G"]) * g_excessSoilCapMoist
  Q_gwOutflow = runLinearRsv(paramSMAR["Kg"], rg_groundwaterInflow) # compute linear reservoir routing
  
  # Sum surface runoff components ----
  rs_surfRunoff = r1_directRunoff + r2_excessInfilCapRunoff + r3_excessSoilCapMoist
  routedSurfFlow = runNashCascade(rs_surfRunoff, paramSMAR["n"], paramSMAR["nK"], outVerbose)
  
  # Last sum : surface runoff + groundwater flow ----
  q_tot = routedSurfFlow + Q_gwOutflow
  
  outputSMAR=data.frame(date=dates, q_tot=q_tot, routedSurfFlow=routedSurfFlow,
                        Q_gwOutflow=Q_gwOutflow, Z_soil=Z_soilLayersMoisture )
  
  # Return the output dataframe ----
  return(outputSMAR)
}

# |_| LOCAL FUNCTIONS ----

#Function to put input parameters in good format
#' Creation of the input parameters for the SMAR model.
#'
#' @param inputparam 
#'
#' @export
#'
createParamSMAR=function(inputparam){
  paramSMAR=c(C=inputparam[1],
              G=inputparam[2],
              H=inputparam[3],
              Kg=inputparam[4],
              n=inputparam[5],
              nK=inputparam[6],
              T=inputparam[7],
              Y=inputparam[8],
              Z=inputparam[9])
  names(paramSMAR)= c("C",
                      "G",
                      "H",
                      "Kg",
                      "n",
                      "nK",
                      "T",
                      "Y",
                      "Z")
  return(paramSMAR)
}

#Main function to create initial states of the model
#' Create initial states of the SMAR model
#'
#' @param Z0 
#' @param paramSMAR 
#'
#' @export
#'
createInitStates = function(Z0, paramSMAR){
  Zmax=paramSMAR["Z"]
  if (Z0>Zmax){
    message("initial moisture content is higher than max soil capacity...")
    message(paste0("... max soil cap used instead: ", Zmax))
    Z0=Zmax
  }
  
  NsoilLayersWet = ceiling(Z0/25)
  N_soillayers = ceiling(Zmax/25)
  maxSoilLayersCap = getMaxSoilLayersCap(N_soillayers, Zmax)
  Z = maxSoilLayersCap *0
  Z[1:NsoilLayersWet] = getMaxSoilLayersCap(NsoilLayersWet, Z0)
  
  initStates = data.frame(Z=Z)
}


getExcessRainfall=function(rainfall, ETP, ratioPotEvap){
  excessRain = rainfall - ratioPotEvap * ETP
  excessRain = pmax(excessRain, 0)
  return(excessRain)
}

getSoilSat=function(maxSoilLayersCap, Z_soilLayersMoistureCurrent){
  soilSat=sum(Z_soilLayersMoistureCurrent) / sum(maxSoilLayersCap)
  #soilSat=0.75
  return(soilSat)
}

getDirectRunoffCoeff=function(soilSat, Hcoeff){
  HprimeCoeff= soilSat * Hcoeff
  return(HprimeCoeff)
}

getExcessInfilCapRunoff=function(infiltRunoff, maxInfiltrationCap){
  subsurfRunoff = infiltRunoff - maxInfiltrationCap
  subsurfRunoff = pmax(0, subsurfRunoff)
  return(subsurfRunoff)
}

runNashCascade=function(rs_surfRunoff, n, nK, outVerbose){
  K = nK / n
  unitHydrograph=getNCUnitHydrograph(n, K, outVerbose)
  lookback = length(unitHydrograph)
  routedSurfFlow = numeric(length(rs_surfRunoff))
  
  for (j in seq(along = rs_surfRunoff)){
    istart = max(0, j-lookback)+1
    n_lookback = j - istart + 1
    routedSurfFlow[j] = sum(rev(rs_surfRunoff[istart:j]) * unitHydrograph[1:n_lookback])
  }
  # windows(20,20)
  # plot(seq(along = rs_surfRunoff),rs_surfRunoff)
  # lines(seq(along = rs_surfRunoff), routedSurfFlow)
  return(routedSurfFlow)
}

getNCUnitHydrograph=function(n, K, outVerbose){
  step=1
  u=c(0)
  repeat {
    u_add=pgamma(step/K, shape=n, scale=1)
    if (u[length(u)]==0){
      u = u_add
    } else {
      u = c(u, u_add)
    }
    step = step+1
    if ((u[length(u)]>0.999) | step>180){
      break
    }
  }
  if (outVerbose) {
    message(paste0("The Nash Cascade of linear reservoir needs a unit hydrograph of length: ", length(u), " time steps."))
  }
  uh = u
  uh[2:length(uh)] = u[2:length(u)] - u[1:(length(u)-1)]
  
  return(uh)
}

runLinearRsv=function(K, Qin, outVerbose=F){
  if (outVerbose){
    message(paste(Sys.time(),"=== START local function runLinearRsv ==="))
    print(paste("... length of Qin is: ", length(Qin)))
  }
  Qout = rep(0, length(Qin)) # intialization of Qout, with zero values
  for (i in 2:length(Qin)) {
    Qout[i] = 1/(2*K + 1) * (Qin[i-1]+Qin[i]) + (2*K-1)/(2*K+1)*Qout[i-1]
  }
  if (outVerbose){
    message(paste(Sys.time(),"=== END local function runLinearRsv ==="))
  }
  return(Qout)
}

getEvapSoilLayers = function(Evap_coeffs, N_soillayers, adjustedETP_i, rainfall_i , 
                             Z_soilLayersMoisture_previous, maxSoilLayersCap){
  adjustedETPLayers = adjustedETP_i * Evap_coeffs # compute the adjusted evapotranspiration for each soil layer
  remainMoistureAvailLayers = maxSoilLayersCap - Z_soilLayersMoisture_previous # calculate the remaining soil capacity of each soil layer
  currentEvapSoilLayers = adjustedETPLayers
  # deal with layers No. 2 and deeper
  if (N_soillayers>=2){
    currentEvapSoilLayers[2:N_soillayers] = pmin(Z_soilLayersMoisture_previous[2:N_soillayers],
                                                 remainMoistureAvailLayers[1:(N_soillayers-1)],
                                                 currentEvapSoilLayers[2:N_soillayers])
  }
  # compute the first soil layer, which has a different behaviour
  excessRainfall = max(0, rainfall_i - adjustedETP_i)
  if (excessRainfall==0){
    currentEvapSoilLayers[1] = min(Z_soilLayersMoisture_previous[1], 
                                   adjustedETPLayers[1] - rainfall_i)
  } else {
    currentEvapSoilLayers[1] =0
  }
  
  return(currentEvapSoilLayers)
}


getMaxSoilLayersCap = function(N_soillayers, Z){
  maxSoilLayersCap = rep(0, N_soillayers)
  
  if (N_soillayers==1) {
    maxSoilLayersCap <- Z
    return(maxSoilLayersCap)
  }
  
  maxSoilLayersCap[1] = min (25, Z)
  
  for (i in 2:N_soillayers){
    maxSoilLayersCap[i] = min(25, Z-sum(maxSoilLayersCap[1:i-1]))
  }
  
  return(maxSoilLayersCap)
  
}

getInfiltrationFromShallowerLayer=function(Evap_soilLayers_i, Z_soilLayersMoisture_previous,
                                           N_soillayers, maxSoilLayersCap,
                                           infiltRunoffCurrent, maxInfiltrationCap){
  
  Rinf_soilLayers_i = Evap_soilLayers_i * 0
  
  #deal with the 1st layer (the top soil layer)
  Rinf_soilLayers_i[1] = min(infiltRunoffCurrent, maxInfiltrationCap)
  
  # Deal with layers No. 2 and deeper
  if (N_soillayers>=2){
    for (j in 2:N_soillayers){
      Rinf_soilLayers_i[j] = (Z_soilLayersMoisture_previous[j-1]
                              - Evap_soilLayers_i[j-1]
                              + Rinf_soilLayers_i[j-1]
                              + Evap_soilLayers_i[j]
                              - maxSoilLayersCap[j-1])
      Rinf_soilLayers_i[j] = max(0, Rinf_soilLayers_i[j])
    }
  }
  return(Rinf_soilLayers_i)
}



getSoilLayersMoistStorage = function(Z_soilLayersMoisturePrev, Evap_soilLayersCurrent,
                                     Rinf_soilLayersCurrent, maxSoilLayersCap, N_soillayers){
  N = N_soillayers
  Z_Current = Z_soilLayersMoisturePrev
  # for layers from 1 to N-1, the deepest has its own calculation
  if (N>=2){
    Z_Current[1:(N-1)] = ( Z_Current[1:(N-1)]
                           + Rinf_soilLayersCurrent[1:(N-1)]
                           - Evap_soilLayersCurrent[1:(N-1)]
                           + Evap_soilLayersCurrent[2:N])
  }
  # special behaviour of the deepest layer, because there is no layer deeper to "receive" evaporation  from
  Z_Current[N] = ( Z_Current[(N)]
                   + Rinf_soilLayersCurrent[(N)]
                   - Evap_soilLayersCurrent[(N)])
  
  Z_Current = pmin(Z_Current, maxSoilLayersCap)
  
  return(Z_Current)
  
}


getGWexcessSoilCapMoist = function(g_excessSoilCapMoist, Z_soilLayersMoisture,
                                   Rinf_soilLayers,
                                   Evap_soilLayers,
                                   maxSoilLayersCap) {
  nsteps = dim(Z_soilLayersMoisture)[1]
  N_soillayers = dim(Z_soilLayersMoisture)[2]
  
  g_excessSoilCapMoist[2:nsteps] = (Z_soilLayersMoisture[1:(nsteps-1),N_soillayers]
                                    + Rinf_soilLayers[2:nsteps,N_soillayers]
                                    - Evap_soilLayers[2:nsteps,N_soillayers]
                                    - maxSoilLayersCap[N_soillayers])
  
  g_excessSoilCapMoist = pmax(g_excessSoilCapMoist, 0)
  
  return(g_excessSoilCapMoist)
}





