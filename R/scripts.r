# use roxygen2:
# https://cran.r-project.org/web/packages/roxygen2/vignettes/rd.html
# run roxygen2::roxygenise() to generate documentation

###################################################################
###                                                             ###
####              NZOIA_WIMES_script_v9                        ####
###     script to simulate Nzoia flows at various nodes         ###
###                                                             ###
###################################################################
# Created by Nicolas GEHENIAU (BRLi, FRANCE) - 19 April 2019
# Contributors (BRLi): Romain RECOUVREUR (Muskinghum routing), Sothea HONG, Yoan AUBERT and Fabrice CEBRON
# v4: add "raster::as.matrix" (JWI)
# v5: fix bvs not defined
# v6: convert POSIXct dates into POSIXct dates.
#     and fix 'NA' values sent to WIMES within the function 'runSimuBasinsFlowsGR'
# v7: correction in the function 'fcstRainAllBasinCalculation' replace variable 'm' into 'm_GFS_grid'
#      and delete the parameter 'maxItems' within the rwimes 'getProductionDates' called in function 'runSimuBasinsFlowsGR'
# v8: delete url and key in function arguments. 
#     delete the getGridCurl function because it needs the url and key
# v9: fix "match" parameters inversion
#     add some exceptions management (empty data sets, empty grid names ,etc.)
#     add continuity correction
#     add KMD + GPM basin rainfall combination
# v10: add additional parameters to 'runSimuBasinsFlowsGR' and 'runCumulatedRoutedFlows' so as to be able to easily run hindcast or reforecast
# v11: add possibility to choose the QPF (GFS or WRF) to use for the simulation

# The script is divided into three main functions:
#    1- Inverse Distance Weighting interpolation of rainfall data from ground stations (AWS) --> create rasters
#    2- Computation of aeral sub-basins precipitation --> create time series
#    3- Computation of Nzoia flows at various nodes using a GR4H semi-distributed model 
#       (includes Muskingum routing) --> create time series


###################################################################
###                                                             ###
###                    MAIN SCRIPT                              ###
###                                                             ###
###################################################################

#' MAIN R SCRIPT of the Nzoia Flood Early Warning WIMES system
#'
#' 
#' @details This script is the main 'autonomous' script that can be called by the WIMES application
#' It follows the main following phases:
#' Phase 1: Load libraries
#' Phase 2: Compute basin rainfall
#' Phase 3: Compute basin ETP (optionnal)
#' Phase 4: Compute basin flow forecast
#' Phase 5: Compute routed flows forecast
#' It is expected to run this script every 6 hours, 7 days a week
#'
#' @export
#'
main_wimes_Nzoia = function(QPF="GFS"){ # by default, if the script is run without this parameter defined in the JSON file, use GFS QPF
  #assign("QPF", QPF, envir = .GlobalEnv)
  library(rwimes)   # this library is used to communicate with the WIMES application
  logWarning(paste0("The forecast will be run with ",QPF," QPF: be sure that such data is effectively available!"))
  updateProgression(0, "Starting 'main_wimes_Nzoia'")
  # Phase 1: Load libraries --------------------
  updateProgression(0, "start Phase 1: Load libraries")
  logInfo("Phase 1: Load libraries")
  library(sp)       # this library is used to manipulate geopspatial objects
  library(raster)   # this library is used to specifically manipulate geopspatial raster objects
  library(rgdal)    # this library is used to manipulate geopspatial objects
  library(gstat)    # this library is used of inverse distance weighting calculation
  library(geojsonR) # this library is used to read GeoJSON file from WIMES
  # library(airGR)    # this library is used to run GR4J and GR4H models
  library(airGR2)    # this library is used to run GR4J and GR4H models (BRL version of AirGR library). Turn it on when the airGR2 will be installed
  library(curl)     # this library can be used for alternative download options (especially for GFS grids)
  library(lubridate)# this library is used to handle date and times more easily
  
  #Some shared parameters
  working_crs="epsg:21037"  #This is the geographic projection used in the project (it cannot be WGS84, but UTM, Lambert-93, etc.)
  
  # Phase 2: Compute basin rainfall ------------
  updateProgression(1, "start Phase 2: Compute basin rainfall")
  logInfo("Phase 2: Compute basin rainfall")
  # .. 2.0 - Get the basin / watershed shapefile as OGR geometry ----
  label_pattern = "....._BAS"
  Basin_Sites = getBasinSitesList(label_pattern) # return a data frame with the list of basin sites which match the label_pattern
  bvs = getBasinsGeometry(Basin_Sites, working_crs) # return an object of class 'SpatialPolygons', each element cooresponds to a polygon contour of one basin
  Basin_Sites=data.frame(Basin_Sites, Surf_area_km2=(area(bvs)/10^6)) # add an attribute corresponding to the polygon surface area
  
  
  # .. 2.1 - Compute basin rainfall based on rainfall stations on the ground ----
  logInfo(".. Phase 2.1 - Compute basin rainfall based on rainfall stations on the ground")
  updateProgression(3, ".. start Phase 2.1 - Compute basin rainfall based on rainfall stations on the ground")
  timestep = '1h'
  TS_code_pattern = paste0("KMD_",label_pattern ,"_P_obs_") #(e.g. KMD_1FG02_BAS_P_obs_1h) basin rainfall timeseries code pattern
  obsRainStnPattern = "KMD_......._..._P_obs_1h$" # Observed rainfall stations rainfall TS code pattern
  statusIDW = runInterpolationIDW(timestep, Basin_Sites, bvs, TS_code_pattern, obsRainStnPattern, outVerbose = TRUE)
  
  # .. 2.2 - Compute basin rainfall based on GPM data ----
  logInfo("Phase 2.2 - Compute basin rainfall based on GPM data")
  updateProgression(10,"..start Phase 2.2 - Compute basin rainfall based on GPM data" )
  GPM_TS="GPM_GRID_P_30m" # the code of the GPM grid timeseries
  GPM_timestep = "30m"
  statusGPMRain=computeGPMbasinRainfall(bvs, Basin_Sites, GPM_timestep, GPM_TS, outVerbose=TRUE)
  
  # .. 2.3 - Compute forecast rainfall based on WRF data ----
  logInfo("Phase 2.3 - Compute forecast rainfall based on WRF data")
  updateProgression(25,"..start Phase 2.4 - Compute forecast rainfall based on GFS data" )
  fcst_grid_TS="WRF_APCP$" # the code of the forecast grid timeseries
  fcstTSpattern="WRF_....._BAS_P_fcst_1h$" #the code pattern for the forecasted basin rainfall
  biasFactorPattern = "WRF_....._BAS_P_BiasFact$" # the code pattern for the forecasted rainfall bias factor
  statusFcstRain = fcstRainAllBasinCalculation(bvs, 
                                               Basin_Sites, 
                                               fcstTSpattern, 
                                               fcst_grid_TS,
                                               biasFactorPattern,
                                               outVerbose = TRUE)
  
  # .. 2.4 - Compute forecast rainfall based on GFS data ----
  logInfo("Phase 2.4 - Compute forecast rainfall based on GFS data")
  updateProgression(25,"..start Phase 2.4 - Compute forecast rainfall based on GFS data" )
  fcst_grid_TS="GFS_GRID_FC_P_3H$" # the code of the forecast grid timeseries
  fcstTSpattern="GFS_....._BAS_P_fcst_3h$" #the code pattern for the forecasted basin rainfall
  biasFactorPattern = "GFS_....._BAS_P_BiasFact$" # the code pattern for the forecasted rainfall bias factor
  statusFcstRain = fcstRainAllBasinCalculation(bvs, 
                                               Basin_Sites, 
                                               fcstTSpattern, 
                                               fcst_grid_TS,
                                               biasFactorPattern,
                                               outVerbose = TRUE)
  
  
  # Phase 3: Compute basin ETP (optionnal) -----
  #TODO
  
  # Phase 4: Simulate independent basins' flow forecast -------
  logInfo("Phase 4: Simulate independent basins' flow forecast")
  updateProgression(40, "start Phase 4: Simulate independent basins' flow forecast")
  # SIMUL PARAMETERS for the function runSimuBasinsFlowsGR below
  ModelTool      ="GR4H"  #choose between 'GR4J' or 'GR4H'
  Rain_input     ="IDW_rainfall" #for now, only "IDW_rainfall" is available
  obsRainTSpattern1 = "KMD_....._BAS_P_obs_1h$" # e.g. KMD_1EF01_BAS_P_obs_1h
  obsRainTSpattern2 = "GPM_....._BAS_P_sat_30m$" # e.g. GPM_1EF01_BAS_P_sat_30m
  combineOption <- "" #use default combination option which is based on the number of available rain gages
  ETP_input      = "Oudin" # for now, only the Oudin function is available
  Rain_forecast_input = QPF
  if (QPF=="WRF")
	{
	fcstRainTSpattern = "WRF_....._BAS_P_fcst_1h$" # e.g. WRF_1BD02_BAS_P_fcst_1h
	tstepFcstHour <- 1
	}
  else
	{
	fcstRainTSpattern = "GFS_....._BAS_P_fcst_3h$" # e.g. GFS_1BD02_BAS_P_fcst_3h
	tstepFcstHour <- 3
	}
  qq_sim_df = runSimuBasinsFlowsGR(ModelTool, Rain_input, obsRainTSpattern1, obsRainTSpattern2,combineOption,
                                   ETP_input, Rain_forecast_input, fcstRainTSpattern,tstepFcstHour,
                                   Basin_Sites, exportStorDurationDays=10, currentDateTime = NULL, lookback_days = 365,outVerbose = TRUE)
  
  # Phase 5: Compute routed flows forecast at specific nodes -----
  logInfo("Phase 5: Compute routed flows forecast at specific nodes")
  updateProgression(70, "start Phase 5: Compute routed flows forecast at specific nodes")
  obsTScode_pattern = c("WRA_....._..._Q_1h$", "KMD_1EF01_Nzoia_Qh$") # example: 'WRA_1ED01_LUS_Q_1h'
  fcstTScode_pattern = "GR4_....._..._Q_sim_1h$" # example 'GR4_1EF01_NZO_Q_sim_1h'
  qq_cum_df = runCumulatedRoutedFlows(qq_sim_df= qq_sim_df, 
                                      obsTScode_pattern = obsTScode_pattern,
                                      fcstTScode_pattern = fcstTScode_pattern,
                                      correctForecast = TRUE,
                                      currentDateTime = NULL,
                                      outVerbose = TRUE) # run the dedicated function
  
  updateProgression(99, "END Phase 5: Compute routed flows forecast at specific nodes")
  updateProgression(100, "END of 'main_wimes_Nzoia'")
}

###################################################################
###                                                             ###
###                PRIMARY FUNCTION                             ###
###               runInterpolationIDW                             ###
###                                                             ###
###################################################################
#
#
# DESCRIPTION of the function
#    script to interpolate rainfall data
#    from rainfall ground stations

timestep="1h" # this is an argument specific for the function runInterpolationIDW

#' Function to interpolate rainfall data based on rainfall ground stations 
#' It also computes basin rainfall amounts and WRITE them into the WIMES application
#'
#' @export
#' @param timestep String - Time step of interpolation ("1d": one day; "1h": one hour ; "5m": five minutes)
#' @param Basin_Sites data frame - object returned by the function 'getBasinSitesList'
#' @param bvs SpatialPolygons - object returned by the function 'getBasinsGeometry'
#' @param TS_code_pattern String - describes the code pattern for basin rainfall time series (e.g. for getting TS 'KMD_1FG02_BAS_P_obs_1h', the label pattern could be "KMD_....._BAS_P_obs_1h")
#' @param obsRainStnPattern String - describes the code pattern for station rainfall time series (e.g. for getting TS 'KMD_8934059_UHO_P_obs_1h', the label pattern could be "KMD_......._..._P_obs_1h")
#' @param outVerbose logical (optionnal, default FALSE) - defines whether information messages should be printed (e.g. max celerity, dx, etc..
#' 
#' @return logical. Return TRUE if the function went through all steps, that is to say if basin rainfall values were computed and sent to WIMES. Return FALSE otherwise.
#' 
#' @examples
#' runInterpolationIDW(timestep, Basin_Sites, bvs, TS_code_pattern)
#' @note Function to interpolate rainfall data based on rainfall ground stations 
#' It also computes basin rainfall amounts and WRITE them into the WIMES application
runInterpolationIDW = function (timestep, Basin_Sites, bvs, TS_code_pattern, obsRainStnPattern, outVerbose=FALSE)
{
  logInfo("=== Starts function runInterpolationIDW ===")
  
  IDW_method=1 #if method=1 : use IDW function included in GSTAT package, if method=2, use the BRLi home made function, for now only method=1 is available (#TODO)
  
  # 1 - READ BASIN RAINFALL TIME SERIES from WIMES ----------------------------
  logInfo("runInterpolationIDW - 1 - READ BASIN RAINFALL TIME SERIES from WIMES")
  
  # .. 1.1 - Define the basin aeral rainfall lookback period ----
  now_date=Sys.time()
  as.POSIXct(now_date) # make sure the date is in POSIXct format
  #now_date=strptime(x="2019-04-25 17:00:00", format="%Y-%m-%d %H:%M:%S", tz="UTC") # this is only for the example
  attributes(now_date)$tzone<- "UTC"
  lookback_days=21 # number of days of the lookback period, we usually should take one month
  date_end = round(now_date, units="hours") #should be modified to take into account various timesteps
  date_start = date_end - as.difftime(lookback_days,units="days")
  date_start = as.POSIXct(date_start) # make sure the date is in POSIXct format
  
  logInfo(paste0("The basin aeral rainfall lookback period is FROM ",date_start," TO ",date_end))
  
  # .. 1.2 - Define the timestep of basin areal rainfall time series ----
  if (timestep == "1h") {
    
  }
  
  # .. 1.3 - Get observed basin rainfall data from WIMES based on TS code pattern ----
  TScode_pattern = paste0(TS_code_pattern,timestep) # the TS code pattern should also make use of the timestep (e.g. "1h")
  tstep = textToDifftime(timestep) # conversion into a difftime object of the string "timestep"
  basin_rains_df = getObsDataByTScodepattern(date_start, date_end, Basin_Sites, TScode_pattern, tstep)
  basin_rains_dates = basin_rains_df$date
  basin_rains = basin_rains_df[,2:dim(basin_rains_df)[2]]
  
  
  basin_TS=getAllTimeSeries(sitesCodes = Basin_Sites$code) #get all the timeseries wtihin the WIMES app that match basins' codes (e.g. 1EF01_BAS)
  m_basin_TS=grep(pattern=paste0(TS_code_pattern,timestep), x=basin_TS$code) # Let's keep only timeseries which names have a specific pattern, it depends on the timestep (argument of the function)
  if (length(m_basin_TS)==0){
    logWarning(paste0("No basin rainfall time series were found for the label pattern: ", paste0(TS_code_pattern,timestep)))
    return(FALSE)
  }
  basin_TS = basin_TS[m_basin_TS,] # Time series of only required
  
  
  # .. 1.4 - Get the oldest date for which there is no basin rain data ------------
  
  # For example :
  #             if the last data of TS      'KMD_1BE06_BAS_P_obs_1h' is '2019-03-26 17:00:00'
  #              and if the last data of TS 'KMD_1ED01_BAS_P_obs_1h' is '2019-03-26 21:00:00'
  #              then min_last_avail_date should be equal to '2019-03-26 17:00:00'
  
  min_last_avail_date = date_end # first initialize to the maximum date 
  for (i in 1:length(basin_TS$code)) {
    mindate = min(basin_rains_dates[is.na(basin_rains[,i])])
    
    if (outVerbose){
      logInfo(paste("mindate= ",mindate))    
      logInfo(paste("min_last_avail_date= ",min_last_avail_date))
    }
    
    if (!is.na(mindate)) {
      if (length(is.na(basin_rains[,i])) != 0) {
        if (mindate < min_last_avail_date) {
          min_last_avail_date <- mindate
        }
      }
    }
  }
  min_last_avail_date = as.POSIXct(min_last_avail_date)
  
  # 2 - Get the list of rainfall ground stations --------------------------------------------- 
  logInfo("runInterpolationIDW - 2 - Get the list of rainfall ground stations")
  Sites <- getSites(  ) # get all the sites within the WIMES application
  m_rain_sites=grep(pattern="*Weather Station", x=Sites$name)
  rain_Sites = Sites[m_rain_sites,] # rain_Sites is only the 'Point' sites of which the code finishes with the pattern "Weather Station"
  
  # CHECK that all selected sites (rain_Sites) are of Point type
  #TODO: use the checkGeom function
  m_nopoly_init = rep(NA,length(rain_Sites$code)) #initialization of a vector of same length as rain_Sites
  m_nopoly = m_nopoly_init
  for (i in 1:length(rain_Sites$code)) {
    site <- getSite(rain_Sites$code[i])
    if (!checkGeom(site, "Point")){
      m_nopoly[i]=i
    } 
  }
  if (!identical(m_nopoly, m_nopoly_init)) {rain_Sites = rain_Sites[-m_nopoly,]} #reject every sites that is not recognised as a 'Point' 
  
  
  # 3 - Read rain stations SHP from WIMES --------------------------------------------- 
  logInfo("runInterpolationIDW - 3 - Read rain stations SHP from WIMES")
  wgs84_crs="epsg:4326" #same as above, but it will be used in the ASCII file names
  stationP <- list() # points coordinates as a list
  Rgeom_mat = matrix(data=NA, nrow=length(rain_Sites$code), ncol=2)  # initialization of the matrix containing the XY coordinates
  
  for (i in 1:length(rain_Sites$code))
  {
    if (outVerbose){
      logInfo(paste0("Try to get geometry coordinates of site : ", rain_Sites$code[i]))
    }
    Rgeom = getSite(rain_Sites$code[i])$geometry$coordinates
    Rgeom = matrix(data=Rgeom, nrow=1)
    Rgeom_mat[i,]=Rgeom
  }
  Rgeom_df=data.frame(code=rain_Sites$code) #converting to data frame and inserting station's codes
  stationP <- SpatialPointsDataFrame(coords = Rgeom_mat, data=Rgeom_df , proj4string=CRS(paste0("+init=",wgs84_crs)))
  stationP=spTransform(stationP, crs(bvs)) #use the bvs's CRS as a new projection
  
  
  # 4 - READ WEATHER STATIONS RAINFALL TIME SERIES from WIMES ----------------------------
  logInfo("runInterpolationIDW - 4 - READ WEATHER STATIONS RAINFALL TIME SERIES from WIMES")
  
  #BIG TODO: use the function getObsDataByTScodepattern
  #BIG TODO: in the function to get data, we should check the quality status of the data, and reject dubious or bad data
  
  rain_TS=getAllTimeSeries(sitesCodes = rain_Sites$code)
  
  m_rain_TS=grep(pattern=obsRainStnPattern, x=rain_TS$code) # Let's keep only timeseries which names have a specific pattern, it depends on the timestep (argument of the function)
  rain_TS = rain_TS[m_rain_TS,] # Time series of only required
  
  # Check whether the interpolation period of each TS match with the timestep (argument of the function)
  for (i in 1:length(rain_TS$code)) {
    checkInterpolationPeriod(rain_TS$code[i], timestep)
  }
  
  # Get the observed data within a given time window
  date_end = round(now_date, units="hours") #should be modified to take into account various timesteps
  #TODO LATER: the time window should take into account whether the data has been corrected since last run. if yes run again
  
  date_start = date_end - as.difftime(lookback_days, units="days") # the start date for the observed rainfall time window should be at least three days long
  date_start = as.POSIXct(date_start)
  if (outVerbose) {
    logInfo(paste("date_start= ", date_start))
    logInfo(paste("min_last_avail_date= ", min_last_avail_date))
  }
  
  date_start = min(date_start, min_last_avail_date) # the time window should start at the 'min_last_avail_date' which corresponds to the last date time where basin aeral rainfall is available
  date_start = as.POSIXct(date_start)
  
  logInfo(paste0("The weather stations rainfall lookback period is FROM ",date_start," TO ",date_end))
  
  rainfall_dates = seq(from = date_start, to = date_end, by="hour")
  attributes(rainfall_dates)$tzone = "UTC" # make sure we are in "UTC" time zone
  rainfall_dates = rev(rainfall_dates) # we have to reverse dates because the RWIMES API provide data from the most recent to the less recent observation
  rainfall = matrix(NA, nrow=length(rainfall_dates), ncol=length(rain_TS$code))
  colnames(rainfall)=rain_TS$code
  
  #'For' loop on each rain time series: get the observed values from WIMES
  
  nRainStationsWithoutData = 0 # integer that counts how many stations are found without any data in it during the lookback period
  
  for (i in 1:length(rain_TS$code)) {
    raindata=getObservedValues(rain_TS$code[i], beginDateTime=date_start , endDateTime=date_end)
    #only keep data qualified as GOOD in case of correctable (check first that the result is not empty)
    if (!(is.null(dim(raindata)) | is.null(raindata$value))) {
      nObsRaw<-dim(raindata)[1]
      if (rain_TS$correctable[i]) {
        raindata=subset(raindata,qualityControlStatus=="GOOD")
      } else {#otherwise do some rough verification based on threshold (to be tuned accordingly...)
        raindata=subset(raindata,value<150)
      }
    #if no consistent data has been found, switch to NULL to accommodate with the subsequent check before further processing
     if (dim(raindata)[1]==0) {
        raindata<-NULL
      } else {
        nObsGood<-dim(raindata)[1]
        if (nObsGood<nObsRaw) {
          logInfo(paste0(nObsRaw-nObsGood," bad or dubious data have been left aside from TS ",rain_TS$code[i] ))
        }
      }
    }
    if (is.null(dim(raindata)) | is.null(raindata$value)){
      logInfo(paste0("No data could be retrieved from TS ",rain_TS$code[i] ))
      nRainStationsWithoutData = nRainStationsWithoutData + 1 # increment the counter
    } else {
      logInfo(paste0("..data is being retrieved from TS ",rain_TS$code[i] ))
      raindata_date = raindata$observationDateTime
      raindata_date = raindata_date
      m_data = match(as.character(rainfall_dates, format="%Y%m%dT%H%M%S"), as.character(raindata_date, format="%Y%m%dT%H%M%S"))
      if (length(m_data)==0){
        logWarning(paste0("..a problem encountered when retrieving data from TS ",rain_TS$code[i] )) #this line added when a bug occured on 27 oct 2019 
        logInfo(paste0("No data could be retrieved from TS ",rain_TS$code[i] ))
        nRainStationsWithoutData = nRainStationsWithoutData + 1 # increment the counter
      } else {
        rainfall[,i]=raindata$value[m_data]
      }
      
    }
  }
  
  numberNoNAvalues = length(rain_TS$code) - nRainStationsWithoutData
  if (nRainStationsWithoutData == length(rain_TS$code)) {
    logWarning("No rainfall data could be retrieved from weather stations TS. The function runInterpolationIDW will be stopped")
    return(FALSE)
  }
  
  if (outVerbose){
    logInfo(paste0("A total number of ", numberNoNAvalues, " rainfall station TS have been retreived. They will be used for IDW interpolation."))
  }
  
  rainfall_df=data.frame(rainfall_dates, rainfall)
  n_steps=length(rainfall_df$rainfall_dates) # we adjust the number of time step so that it perfectly matches the length of 'rainfall_dates' (or 'rainfall_df$rainfall_dates')
  
  # 5 - Creation of a domaine RASTER in the study area ----------------------------------
  logInfo("runInterpolationIDW - 5 - Creation of a domaine RASTER in the study area")
  
  stationP_extent=extent(stationP)
  bvs_extent=extent(bvs)
  domaine_extent=stationP_extent
  for (i in c(1,3)) domaine_extent[i]<-min(bvs_extent[i],stationP_extent[i])
  for (i in c(2,4)) domaine_extent[i]<-max(bvs_extent[i],stationP_extent[i])
  domaine_extent=round(domaine_extent)
  domaine=raster(domaine_extent)
  crs(domaine) <- crs(bvs)
  domaine = projectRaster(domaine,crs=crs(bvs),res=c(1000,1000))
  # usually we get a warning here because there is no data in the domaine raster
  
  
  # 6 - RUN IDW interpolation for each single time step and return a raster stack ------------------------------
  logInfo("runInterpolationIDW - 6 - RUN IDW interpolation for each single time step and return a raster stack")
  dateFormat="D%Y%m%dT%H%M%SZ"
  if (IDW_method==1) {
    IDWresultStack = runIdwGstatMethod(rainfall_df, stationP, rain_TS, domaine, dateFormat) #run the function 'runIdwGstatMethod'
    
  } else if (IDW_method==2) {
    logWarning("the IDW method no. 2 is called, but not yet implemented ")
    logWarning("runInterpolationIDW has stopped the script because IDW method no.2 is not implemented ")
    return(FALSE)
  }
  
  ####---===---===---===---===---===---===---===---===---===---===---===
  # 7 - COMPUTE basin aeral precipitation---------------------------
  logInfo("runInterpolationIDW - 7 - COMPUTE basin aeral precipitation")
  domaine     = IDWresultStack[[1]]
  basinRainsDF = getBasinAreaRainfall(bvs, domaine, IDWresultStack, basin_TS, dateFormat, outVerbose) #call the local function necessary to compute basin area rainfall
  
  
  # 8 - WRITE results into WIMES ----
  logInfo("runInterpolationIDW - 8 - WRITE results into WIMES")
  n_upsert=0 # this integer variable is used to count how many values will be sent to WIMES
  outputDF = NULL
  #start a loop on each basin within bvs
  for (k in seq(bvs)) {
    # upsertQuantValues(data.frame(timeSeriesCode=c("BRL_BRL_FY_Qs","BRL_BRL_FY_Qd"),
    #                              observationDateTime=c(as.POSIXct("2018-02-12 22:00:00", format="%Y-%m-%d %H:%M:%S"),as.POSIXct("2018-02-12 23:00:00", format="%Y-%m-%d %H:%M:%S")), 
    #                              value=c(82,81)))
    outputDF_temp = data.frame(timeSeriesCode      = names(basinRainsDF)[k+1],
                               observationDateTime = basinRainsDF$date,
                               value               = basinRainsDF[,k+1]) #k+1 because first column of the data frame is the date column
    if (is.null(outputDF)){
      outputDF = outputDF_temp
    } else {
      outputDF = rbind(outputDF, outputDF_temp)
    }
  }
  #SEND values to WIMES
  upsertQuantValuesByChunks(outputDF,500) #switch to the "ByChunks" version for more flexibility
  n_upsert = length(outputDF$value)
  logInfo(paste0("A total number of ",as.character(n_upsert), " values have just been sent to WIMES for basin rainfall."))
  
  # 9 - CLEAN temporary raster files ----
  logInfo("runInterpolationIDW - 9 - Use 'removeTmpFiles' function to clean temporary raster files.")
  removeTmpFiles(h=0)
  
  # 10 - END of the function ----
  logInfo("END of the function 'runInterpolationIDW', logical TRUE is returned.")
  logInfo("====================")
  logInfo(" ")
  return(TRUE)
}

###################################################################
###                                                             ###
###                PRIMARY FUNCTION                             ###
###                GPMbasinRainfall                             ###
###                                                             ###
###################################################################


#' GPMbasinRainfall
#'
#' @param bvs SpatialPolygons - object returned by the function 'getBasinsGeometry'
#' @param Basin_Sites data frame - object returned by the function 'GetBasinSitesList'
#' @param timestep 
#' @param GPM_TS string - time series code of the GPM data
#' @param outVerbose logical (optionnal, default FALSE) - defines whether information messages should be printed
#'
#' @export
#'
computeGPMbasinRainfall=function(bvs, Basin_Sites, timestep, GPM_TS, outVerbose=FALSE){
  logInfo("=== STARTS running the function 'computeGPMbasinRainfall' ===")
  
  wgs84_crs="epsg:4326" #WGS84 coordinates system (in decimal degrees)

  # 1 - Defines the lookback period time window ----
  logInfo("computeGPMbasinRainfall - 1 - Defines the lookback period time window")
  now_date=Sys.time()
  #now_date=strptime(x="2019-04-25 17:00:00", format="%Y-%m-%d %H:%M:%S", tz="UTC") # this is only for the example
  attributes(now_date)$tzone<- "UTC"
  lookback_days=180 # number of days of the lookback period, we usually should take 180 days
  date_end = round(now_date, units="hours") #TODO: should be modified to take into account various timesteps
  date_start = date_end - as.difftime(lookback_days,units="days")
  date_start = as.POSIXct(date_start) # make sure the date is in POSIXct format
  logInfo(paste0("The GPM basin rainfall lookback period is FROM ",date_start," TO ",date_end))
  
  tstep = textToDifftime(timestep) # tstep is the time step of data (a difftime object, while "timestep" is a string)
  
  # 2 - Read basin GPM timeseries data based on a TScode pattern ----
  logInfo("computeGPMbasinRainfall - 2 - Read basin GPM timeseries data based on a TScode pattern")
  TScode_pattern="GPM_....._BAS_P_sat_" # defines the timeseries code pattern (e.g. GPM_1BD02_BAS_P_sat_1d)
  TScode_pattern = paste0(TScode_pattern,timestep)
  GPM_basin_rains_df = getObsDataByTScodepattern(date_start, date_end, Basin_Sites, TScode_pattern, tstep)
  
  if (is.null(GPM_basin_rains_df)){
    logInfo("Without any time series related to GPM basin rainfall, the function 'computeGPMbasinRainfall' is cancelled.")
    return(FALSE)
  }
  
  obs_TS     <- getAllTimeSeries(sitesCodes=Basin_Sites$code)
  m_obs       <- grep(pattern=TScode_pattern, obs_TS$code)
  obs_TS     <- obs_TS[m_obs,]
  if (outVerbose){
    logInfo(paste0("A total number of ", length(m_obs)," time series have just been identified as GPM basin rainfall."))
  }
  
  if (length(m_obs)==0){
    logWarning("Zero time series have been identified for GPM basin rainfall.")
    logInfo("The function 'computeGPMbasinRainfall' is cancelled. A FALSE value is returned to main_wimes_Nzoia")
    return(FALSE)
  }
  
  # 3 - Define the time window that require completion ----
  logInfo("computeGPMbasinRainfall - 3 - Define the time window that require completion")
  ObsEndDate = date_end
  ObsStartDate = date_end - as.difftime(1, units="days") # initialization, we want at leat to compute one day
  mindate = ObsStartDate
  # Get a finer time window. We do not want to compute twice the basin rainfall older than one day.
  for (cd in 2:(dim(GPM_basin_rains_df)[2])){
    m_no_na=which(!is.na(GPM_basin_rains_df[,cd]))
    if (length(m_no_na)==0){
      logInfo(paste0("There is not any valid value within timeseries TS ",names(GPM_basin_rains_df)[cd]))
      logInfo("The time window duration will be maximized.")
      ObsStartDate = date_start
    } else {
      mindate = max(GPM_basin_rains_df[m_no_na,1]) # get the most recent date for which there are valid values
      ObsStartDate = min(mindate, ObsStartDate) # get the less recent date
      ObsStartDate = as.POSIXct(ObsStartDate) # ensure the date format is POSIXct
    }
  }
  
  logInfo(paste0("GPM basin rainfall: the time window that require completion is FROM ",ObsStartDate," TO ",ObsEndDate))
  
  # 4 - Get the values of the GPM grid within the required period as a raster stack ----
  logInfo("computeGPMbasinRainfall - 4 - Get the values of the GPM grid within the required period as a raster stack")
  dateFormat = "D%Y%m%dT%H%M%SZ"
  ObsRasterStack=getObservRasterStack(GPM_TS, ObsStartDate, ObsEndDate, dateFormat)
  #add one second to the date of GPM raster (as E-165959 is used instead of E-170000)
  names(ObsRasterStack) <- strftime(as.POSIXct(names(ObsRasterStack),dateFormat,tz="UTC")+as.difftime(1,unit="secs"),dateFormat,tz="UTC")
  
  if (length(ObsRasterStack)==0){
    logWarning("There isnt any raster wihtin the GPM raster stack")
    logInfo("No data sent to WIMES")
    logInfo("END of the function 'FcstRainAllBasinCalculation'")
    return(FALSE)
  } 
  
  # 5 - Compute GPM basin aeral precipitation  ----
  logInfo("computeGPMbasinRainfall - 5 - Compute forecasted basin aeral precipitation")
  
  domaine      = ObsRasterStack[[1]] #the first raster layer is taken as defining the extent of all the raster stack
  logInfo("Compute observed basin areal rainfall...")
  basinRainsDF = getBasinAreaRainfall(bvs, domaine, ObsRasterStack, obs_TS, dateFormat, outVerbose) #call the local function necessary to compute basin area rainfall
  
  numberOfTS = dim(basinRainsDF)[2] - 1 # number of time series within basinRainsDF. -1 because the first column corresponds to date-time
  
  if (timestep=="1d"){
    logInfo("We are using one-day GPM rainfall, as it is a 3-hour overlapping data, some computation has to be done")
    # if using the one day rainfall, we should apply 2 corrections: 
    # + delay of 30 minutes (we want a preceding total time series)
    # + computation of 3hours cumulated rainfall. indeed rainfall is updated every 3 hours
    basinRainsDF_uncorr = basinRainsDF # keep in memory the uncorrected dataframe
    #basinRainsDF$date = basinRainsDF$date + as.difftime(30, units="mins") #used when GPM data were "SUCCEDING TOTAL"
    basinRainsDF$date = basinRainsDF$date + as.difftime(1, units="secs") #to use since GPM data are "PRECEDING TOTAL" but with "E-165959" instead of "E-170000"
    
    for (j in length(basinRainsDF$date):1){
      if (j >= (length(basinRainsDF$date) - 8)) {
        basinRainsDF[j,2:(numberOfTS+1)] = 0
      } else {
        date24hAgo = basinRainsDF$date[j] - as.difftime(24, units="hours")
        previous7MaxIndexes = which((basinRainsDF$date < basinRainsDF$date[j]) & (basinRainsDF$date > date24hAgo))
        #previous7MaxIndex = min(length(basinRainsDF$date), j+7)
        if (length(previous7MaxIndexes) == 0){
          sum7indexes = 0
        } else {
          sum7indexes = colSums(basinRainsDF[previous7MaxIndexes, 2:(numberOfTS+1)])
        }
        basinRainsDF[j,2:(numberOfTS+1)] = basinRainsDF[j,2:(numberOfTS+1)] - sum7indexes
        basinRainsDF[j,2:(numberOfTS+1)] = pmax(0, basinRainsDF[j,2:(numberOfTS+1)])
      }
    }
  }
  
  
  
  # 6 - CREATE the data frame that will be sent to WIMES ----
  logInfo("computeGPMbasinRainfall - 6 - CREATE the data frame that will be sent to WIMES")
  #start a loop on each basin rainfall time series
  outputDF = NULL
  for (k in 1:numberOfTS) {
    # check whether each time series is not just a list of NA values
    if (sum(is.na(basinRainsDF[,k+1])) != length(basinRainsDF[,k+1])) {
      outputDF_temp = data.frame(timeSeriesCode         = names(basinRainsDF)[k+1],
                                 observationDateTime    = basinRainsDF$date,
                                 value                  = basinRainsDF[,k+1])
      if (is.null(outputDF)){
        outputDF = outputDF_temp
      } else {
        outputDF = rbind(outputDF, outputDF_temp)
      }
    }
  }
  # library(ggplot2)
  # qplot(observationDateTime, value, data=outputDF, geom="line") + facet_wrap(~ timeSeriesCode)
  
  # 7 - WRITE results into WIMES ----
  logInfo("computeGPMbasinRainfall - 7 - WRITE results into WIMES")
  
  if (is.null(outputDF)){
    logWarning("There is no GPM basin rainfall to send to WIMES. The data.frame is null")
    return(FALSE)
  }
  logInfo("Starts sending observed GPM basin rainfall to WIMES... hope it will not last long...")
  upsertQuantValues(outputDF) 
  n_upsert = length(outputDF$value)
  
  logInfo(paste0("A total number of ",as.character(n_upsert), " values have just been sent to WIMES for observed GPM basin rainfall."))
  
  # 8 - CLEAN temporary raster files ----
  logInfo("computeGPMbasinRainfall - 8 - Use 'removeTmpFiles' function to clean temporary raster files.")
  removeTmpFiles(h=0)
  
  # 9 - END of the function ----
  logInfo("END of the function 'GPMbasinRainfall', logical TRUE is returned.")
  logInfo("====================")
  logInfo(" ")
  
  return(TRUE)
}

###_###############################################################
###                                                             ###
###                PRIMARY FUNCTION                             ###
###         fcstRainAllBasinCalculation                         ###
###                                                             ###
###_###############################################################
#' Forecast Rainfall Basin Calculation on all basins of the Nzoia FEWS
#'
#' @param bvs SpatialPolygons - object returned by the function 'getBasinsGeometry'
#' @param Basin_Sites data frame - object returned by the function 'getBasinSitesList'
#' @param fcstTSpattern string - it defines the pattern of timeseries codes that we want to select (e.g. observed rainfall timeseries codes are like 'GFS_1DA02_BAS_P_fcst_3h', so the pattern could be "GFS_....._BAS_P_fcst_3h")
#' @param fcst_grid_TS string - it defines the grid Timeserie code of the forecast raster rainfall
#' @param outVerbose logical (optionnal, default FALSE) - defines whether information messages should be printed (e.g. max celerity, dx, etc..
#' @param biasFactorPattern string - it defines the pattern of timeseries codes of bias factors we want to select. The bias factor should be divided to the rainfall in order to get debiased rainfall
#'
#' @return logical - TRUE if the function goes till the end.
#' @export
#'
fcstRainAllBasinCalculation=function(bvs, Basin_Sites, 
                                     fcstTSpattern, fcst_grid_TS, 
                                     biasFactorPattern = NULL,
                                     outVerbose=FALSE){
  logInfo("=== STARTS running the function 'fcstRainAllBasinCalculation' ===")
  # check the number of basin polygons within 'bvs' (bvs is a SpatialPolygon object)
  if (length(bvs)==0) {
    logWarning("The object 'bvs' is of length 0 ")
    stop("The object 'bvs' is of length 0 ")
  }
  wgs84_crs="epsg:4326" #same as above, but it will be used in the ASCII file names
  
  # 1 - Get timeseries codes of basin forecast rainfall ----
  FrcstRain_TS=getAllTimeSeries(  sitesCodes = names(bvs))
  m_frcst = which((FrcstRain_TS$forecast=="TRUE") & (FrcstRain_TS$parameterNameCode=="P"))
  FrcstRain_TS = FrcstRain_TS[m_frcst,]
  m_GFS=grep(pattern=fcstTSpattern, x=FrcstRain_TS$code)
  FrcstRain_TS = FrcstRain_TS[m_GFS,]
  if (outVerbose){
    logInfo(paste0("A total number of ",length(FrcstRain_TS$code),
                   " forecast basin rainfall time series have been retrieved from WIMES"))
  }
  
  Start_ProdDate = as.POSIXct(Sys.time()) #start production date time of forecast grids
  attributes(Start_ProdDate)$tzone <- "UTC" # convert the date time into "UTC" time zone
  Start_ProdDate = Start_ProdDate - as.difftime(150, units="days")
  Start_ProdDate = as.POSIXct(Start_ProdDate) #convert date into POSIXct
  LastProdDate   = getProductionDates(FrcstRain_TS$code[1], beginDateTime=Start_ProdDate)
  
  if (length(LastProdDate)==0) {
    LastProdDate=Start_ProdDate # by default, the Last production date is set to Start_ProdDate
    logWarning(paste0("Production dates more recent than ",as.character(Start_ProdDate, format="%Y-%m-%d")," have NOT been found."))
    #logInfo("...consequently, the function 'fcstRainAllBasinCalculation' is cancelled")
    #return(FALSE)
	LastProdDate <- Start_ProdDate
  } else {
    LastProdDate = LastProdDate$productionDateTime #extract only the attribute 'productionDateTime' within the dataframe
    LastProdDate = max(LastProdDate) #get only the last value
  }
  
  if (outVerbose){
    logInfo(paste0("the last production date is found: ",as.character(LastProdDate, format="%Y-%m-%dT%H:%M:%S")))
  }
  
  # 2 - Get forecast grid timeseries ----
  
  # Get all time series codes within WIMES
  timeSeries <- getAllTimeSeries(  )
  #Select timeseries for which the siteCode has 'GFS' in its code
  m_GFS_grid=grep(pattern=fcst_grid_TS, x=timeSeries$code)
  #Check how many timeseries were grabbed. Hope just one was found
  if (length(m_GFS_grid)==0){
    logWarning(paste0("No time series was found corresponding to a code called: ", fcst_grid_TS))
    logInfo("The function fcstRainAllBasinCalculation is cancelled. A False value is returned")
    return(FALSE)
  } else if (length(m_GFS_grid)>=2){
    logWarning(paste0("More than one timeseries was found corresponding to a code called: ", fcst_grid_TS))
    logInfo("The function fcstRainAllBasinCalculation is cancelled. A False value is returned")
    return(FALSE)
  }
  GFS_grid_TS=timeSeries[m_GFS_grid,]
  
  # 3 - Define the forecast production date ----
  logInfo("fcstRainAllBasinCalculation - 3 - Define the forecast production date")
  GFS_prod_dates = getProductionDates(timeSeriesCode=GFS_grid_TS$code, beginDateTime=Start_ProdDate) # getPRoductionDates returns a dataframe with the list of production dates
  if (length(GFS_prod_dates)==0) {
    logWarning(paste0("No forecast grid data is available at more recent date than ", Start_ProdDate))
    logWarning("The function is cancelled. A False value is returned.")
    GFS_prod_dates=Start_ProdDate # by default, the Last production date is set to Start_ProdDate
    return(FALSE)
  } else {
    GFS_prod_dates=GFS_prod_dates$productionDateTime #extract only the attribute 'productionDateTime' within the dataframe
  }
  ProductionDate = max(GFS_prod_dates,na.rm=TRUE) # get the more recent date
  if (outVerbose){
    logInfo(paste0("GFS_prod_dates = ",GFS_prod_dates))
    logInfo(paste0("ProductionDate = ",ProductionDate))
  }
  
  if (ProductionDate<=LastProdDate){
    logInfo("The last available GFS grid data has already been converted to forecast basin rainfall. It will be converted again.")
  }
  
  # 4 - GET rainfall forecast data as a raster stack ------------------------------
  logInfo("fcstRainAllBasinCalculation - 4 - GET rainfall forecast data as a raster stack")
  dateFormat = "D%Y%m%dT%H%M%SZ"
  FcstRasterStack = getForecastRasterStack(GFS_grid_TS$code, ProductionDate, dateFormat)
  if (length(FcstRasterStack)==0){
    logWarning("There isnt any raster within the GFS raster stack")
    logInfo("No data sent to WIMES")
    logInfo("END of the function 'fcstRainAllBasinCalculation'")
    return(FALSE)
  }
  
  ####---===---===---===---===---===---===---===---===---===---===---===
  # 5a - COMPUTE forecasted basin aeral precipitation and WRITE INTO WIMES---------------------------
  logInfo("fcstRainAllBasinCalculation - 5a - COMPUTE forecasted basin aeral precipitation and WRITE INTO WIMES")
  
  domaine         = FcstRasterStack[[1]]
  
  logInfo("Compute forecasted basin areal rainfall...")
  basinRainsDF = getBasinAreaRainfall(bvs, domaine, FcstRasterStack, FrcstRain_TS, dateFormat, outVerbose) #call the local function necessary to compute basin area rainfall
  #logInfo(paste("the first basin name of basinRainsDF is ",names(basinRainsDF)[2],sep=""))
  numberOfTS = dim(basinRainsDF)[2] - 1 #§ number of time series within basinRainsDF. Minus 1 because the first column are the dates
  
  # 5b - DEBIAS forecasted rainfall ----
  logInfo("fcstRainAllBasinCalculation - 5b - DEBIAS forecasted rainfall")
  # if the de-bias factor is of length 1, then we have to make it the same length as the number of 
  if (!is.null(biasFactorPattern)){
    deBiasFactor = getLastValByTScodepattern(date_now = ProductionDate, 
                                             Sites = Basin_Sites, 
                                             TScode_pattern = biasFactorPattern)
    #reorder results according to basinRainsDF
    basinRainsNames<-substr(names(basinRainsDF)[-1],6,10)
    basinDebias<-substr(names(deBiasFactor)[-1],6,10)
    m<-match(basinRainsNames,basinDebias)
    deBiasFactor[,-1]<-deBiasFactor[,-1][,m]
    for (k in 1:numberOfTS) {
      logInfo(paste("the bias factor used for ",names(basinRainsDF)[k+1]," is ",deBiasFactor[1,k+1],sep=""))
      basinRainsDF[,k+1] = basinRainsDF[,k+1] / deBiasFactor[1,k+1]
    }
  }
  
  
  # 6 - CREATE the data frame that will be sent to WIMES ----
  logInfo("fcstRainAllBasinCalculation - 6 - CREATE the data frame of forescated basin rainfall that will be sent to WIMES")
  #start a loop on each basin within bvs
  outputDF = NULL
  for (k in 1:numberOfTS) {
    if (sum(is.na(basinRainsDF[,k+1])) != length(basinRainsDF[,k+1])) {
      outputDF_temp = data.frame(timeSeriesCode      = names(basinRainsDF)[k+1],
                                 productionDateTime  = ProductionDate,
                                 forecastDateTime    = basinRainsDF$date,
                                 value               = basinRainsDF[,k+1])
      if (is.null(outputDF)){
        outputDF = outputDF_temp
      } else {
        outputDF = rbind(outputDF, outputDF_temp)
      }
    }
  }
  # 7 - WRITE results into WIMES ----
  
  if (is.null(outputDF)){
    logWarning("There is no GFS forecast basin rainfall to send to WIMES. The data.frame is null")
    return(FALSE)
  }
  
  logInfo("Starts sending forecast basin rainfall to WIMES... hope it will not last long...")
  upsertQuantForecastValues(outputDF) 
  n_upsert = length(outputDF$value)
  
  logInfo(paste0("A total number of ", as.character(n_upsert), " values have just been sent to WIMES for forecasted basin rainfall."))
  
  # 8 - CLEAN temporary raster files ----
  logInfo("fcstRainAllBasinCalculation - 8 - Use 'removeTmpFiles' function to clean temporary raster files.")
  removeTmpFiles(h=0)
  
  # 9 - END of the function ----
  logInfo("END of the function 'fcstRainAllBasinCalculation', a logical TRUE is returned.")
  logInfo("====================")
  logInfo(" ")
  
  return(TRUE)
  
}


###_###############################################################
###                                                             ###
###                PRIMARY FUNCTION                             ###
###               runSimuBasinsFlowsGR                           ###
###                                                             ###
###_###############################################################


#' runSimuBasinsFlowsGR
#'
#' @param ModelTool string - The GR ModelTool to be chosen between 'GR4J' or 'GR4H'
#' @param Rain_input string - for now, only "IDW_rainfall" is available. It defines the kind of rainfall data is used as input. TODO: 
#' @param ETP_input string - for now, only the "Oudin" is available. It defines the kind of ETP data used as input
#' @param Rain_forecast_input string - not used now. TODO: "WRF", "GFS", "zero", "climato"
#' @param Basin_Sites data frame - object returned by the function 'getBasinSitesList'
#' @param outVerbose logical (optionnal, default FALSE) - defines whether information messages should be printed (e.g. max celerity, dx, etc..
#' @param obsRainTSpattern1 string - it defines the pattern of timeseries codes that we want to select for observed rainfall.
#' E.g. observed rainfall timeseries codes are like 'GPM_1BD02_BAS_P_sat_30m', so the pattern could be "GPM_....._BAS_P_sat_30m")
#' @param obsRainTSpattern2 string - it defines the pattern of timeseries codes that we want to select for observed rainfall.
#' E.g. observed rainfall timeseries codes are like 'GPM_1BD02_BAS_P_sat_30m', so the pattern could be "GPM_....._BAS_P_sat_30m")
#' @param combineOption string - defines whether you use the combination function (weights based on the number of rain gages), or only KMD, or only GPM or a fifty-fifty combination
#' @param fcstRainTSpattern string - it defines the pattern of timeseries codes that we want to select for forecasted rainfall. (if omitted, then 0 rainfall is used for future in case of reforecast and observed rainfall will be used in case of hindcast)
#' @param tstepFcstHour integer - time step of forecast QPF (raster) in number of hours
#' E.g. observed rainfall timeseries codes are like 'GFS_1BD02_BAS_P_fcst_3h', so the pattern could be "GFS_....._BAS_P_fcst_3h")
#' @param exportStorDurationDays integer - defines the number of days of the lookback period for both Routing and Production stores.
#' By default, this parameter is set to '10'. It means that the past values which are less than 10 days old are exported to WIMES.
#' Usually those values are re-written for most of them, except the last 6 or 3 hours (since last run). 
#' It is aimed at updating these simulated stores when the input rainfall is updated. 
#' It is important when a raingauge cannot send data for a few days, and afterwards it can send the data.
#' @param currentDateTime POSIXct - indicates the current date and time (otherwised it is assumed to be now)
#' @param lookback_days integer - the number of days to look back to start the simulation
#' 
#' @details The output time series code for GR4H production store should have the following format: "GR4_xxxxx_S_sim_1h" (GR4_1BD02_BAS_S_sim_1h), 
#' where "xxxxx" corresponds to the sub basin code as defined in the input variable "Basin_Sites". 
#' And the same applies for routing store time series : "GR4_xxxxx_R_sim_1h" (e.g. GR4_1BD02_BAS_R_sim_1h)
#' And the same applies also for the output basin discharge time series: "GR4_xxxxx_Q_sim_1h" (e.g. GR4_1BD02_BAS_Q_sim_1h)
#' @note
#' if currentDateTime!=NULL and fcstRainTSpattern==NULL ==> hindcast
#' if currentDateTime!=NULL and fcstRainTSpattern!=NULL ==> reforecast
#' if currentDateTime==NULL and fcstRainTSpattern==NULL ==> forecast with 0 forecasted rain 
#' if currentDateTime==NULL and fcstRainTSpattern!=NULL ==> forecast
#' @export
#'
runSimuBasinsFlowsGR = function (ModelTool, Rain_input, obsRainTSpattern1, obsRainTSpattern2,combineOption,
                                 ETP_input, 
                                 Rain_forecast_input, fcstRainTSpattern = NULL,tstepFcstHour = 3, 
                                 Basin_Sites, 
                                 exportStorDurationDays = 10,
                                 currentDateTime = NULL,
								 lookback_days = 365,
                                 outVerbose=FALSE) {
  
  Sites <- getSites(  ) # get all the sites within the WIMES application
  
  # 1 - Introduction ----
  logInfo("'runSimuBasinsFlowsGR' - 1 - Introduction")
  
  # .. 1.1 - Definition of time step ----
  logInfo("Substep 1.1 - Definition of time step depending on the model GR4J or GR4H")
  
  if (ModelTool=="GR4J"){
    tstep <- as.difftime(24 , units="hours")  #timestep is the simulation timestep '%H' means 'hours'.
  } else if (ModelTool=="GR4H") {
    tstep <- as.difftime(1 , units="hours")
  }
  
  logInfo(paste0("The model chosen is: ",ModelTool," with a time step of ",as.character(tstep,units="hours"), " hour(s)."))
  
  
  # .. 1.2 - MODEL simulation TIME WINDOW (the final time window could be shorter depending on data availability) -------------------------------------------
  logInfo("'runSimuBasinsFlowsGR' - 1.2 - MODEL simulation TIME WINDOW")
  now_date=Sys.time() # get the system time of 'right now"
  attributes(now_date)$tzone<- "UTC" # convert the time_zone to UTC
  forecast_days=15 # number of days of the forecast period, we usually should take 15 days
  if (is.null(currentDateTime)) simu_end = round(now_date, units="hours") + as.difftime(forecast_days,units="days") #the date_time is rounded to the rearest 'hour'
  else simu_end = currentDateTime + as.difftime(forecast_days,units="days")
  #simu_end = round(now_date, units="hours") + as.difftime(forecast_days,units="days") #the date_time is rounded to the rearest 'hour'
  simu_end = as.POSIXct(simu_end)
  #lookback_days=365 # number of days of the lookback period, we usually should take one year
  simu_start = simu_end - as.difftime(lookback_days,units="days")
  simu_start = as.POSIXct(simu_start)
  simu_start = floor_date(simu_start, unit="days") # round down to the nearest day. That way, we are sure we start at time 00:00:00
  warmup_duration= 30 #warmup duration in days. During this period, the simulated flows are not used for calibration (if calibration...)
  logInfo(paste0("The desired simulation time window is FROM ",simu_start," TO ",simu_end))

  # .....INPUT PARAMETERS --------------------------------------------------------
  CalibMatrix_DF <- getGRCalibParam(Basin_Sites = Basin_Sites) # get the GR4H calibrated parameters for the basin included in Basin_Sites
  Code_stations_calib <- CalibMatrix_DF[,1]
  
  # 2 - READ OBSERVED BASIN RAINFALL TIME SERIES from WIMES ----------------------------
  logInfo("'runSimuBasinsFlowsGR' -  2 - READ BASIN RAINFALL TIME SERIES from WIMES")
  
  #get KMD basin rainfall
  obsBasinRains1 = getObsDataByTScodepattern(simu_start, 
                                            simu_end, 
                                            Basin_Sites, 
                                            obsRainTSpattern1, 
                                            tstep)
  
  obsBasinRains1 = convertDFtimestep(df=obsBasinRains1, 
                                    newTimeStep=tstep, 
                                    cumul = TRUE, outVerbose) #cumul is set to true because rainfall are cumulative
  
  #get GPM basin rainfall
  obsBasinRains2 = getObsDataByTScodepattern(simu_start, 
                                            simu_end, 
                                            Basin_Sites, 
                                            obsRainTSpattern2, 
                                            as.difftime(30 , units="mins"))
  
  #aggregate at 1h time step the 30min GPM basin rainfall
  obsBasinRains2<-aggreg_timestep(obsBasinRains2,as.difftime(30,unit="mins"),as.difftime(1,unit="hours"),previous=TRUE)
  obsBasinRains2<-obsBasinRains2[order(obsBasinRains2[,1],decreasing=TRUE),]
  
  #get the number of gages that were available to compute areal rainfall
  nRainGages<-getObservedValuesByChunks("KMD_AVAILABLE_RAIN_GAGES_1H",simu_start,simu_end)
  if (combineOption=="KMD") nRainGages$value<-20
  if (combineOption=="GPM") nRainGages$value<-0
  if (combineOption=="mix") nRainGages$value<-NA
  
  #combine KMD and GPM basin rainfall
  obsBasinRains<-combine_KMD_GPM(obsBasinRains1,obsBasinRains2,nRainGages,as.difftime(1,unit="hours"))
  #put data frame from most recent to least recent date (to comply with was initially sent by RWIMES)
  obsBasinRains<-subset(obsBasinRains,obsBasinRains[,1]>=simu_start&&obsBasinRains[,1]<=simu_end)
  obsBasinRains<-obsBasinRains[order(obsBasinRains[,1],decreasing=TRUE),]
  #logInfo(paste("the least recent observed basin rainfall is ",min(obsBasinRains[,1]),sep=""))
  #logInfo(paste("the most recent observed basin rainfall is ",max(obsBasinRains[,1]),sep=""))
  #dfTemp<-subset(obsBasinRains,obsBasinRains[,1]>=as.POSIXct("2020-06-01 00:00:00",tz="UTC"))
  #cTemp<-sapply(dfTemp[,2:dim(dfTemp)[2]],"sum",1,na.rm=TRUE)
  #nTemp<-names(cTemp)
  #for (u in 1:length(cTemp)) logInfo(paste("combined rainfall accumulation for ",nTemp[u]," is ",cTemp[u],sep=""))
  
  m_na_rain  <- which(is.na(obsBasinRains[,2])) #get the indexes corresponding to NA's value
  logInfo(paste("there were ",length(m_na_rain)," NA values in obs rainfall",sep=""))
  if (length(m_na_rain)>0) obsBasinRains <- obsBasinRains[-m_na_rain,] #Eject lines with NA values
  obsBasinRainsDates   <- obsBasinRains[,1] #first column corresponds to dates
  #logInfo(paste("minimum date for obs rainfall is ",min(obsBasinRainsDates),sep=""))
  
  # 3 - READ FORECAST BASIN RAINFALL TIME SERIES from WIMES ---------------------------- 
  # ==> to modify to be able to deal with hindcast (maybe put an IF before steps 3 and 4 and an ELSE to use only observation as basin_rains??
  if (!is.null(fcstRainTSpattern)) {
  logInfo("'runSimuBasinsFlowsGR' -  3 - READ FORECASTED BASIN RAINFALL TIME SERIES from WIMES")
  
  # .. 3.1 - Identify the more recent production date ----
  logInfo("'runSimuBasinsFlowsGR' -  3.1 - Identify the more recent production date")
  
  FrcstRain_TS = getAllTimeSeries(sitesCodes = Basin_Sites$code)
  m_frcst = which((FrcstRain_TS$forecast=="TRUE") & (FrcstRain_TS$parameterNameCode=="P"))
  FrcstRain_TS = FrcstRain_TS[m_frcst,]
  logInfo(paste0("A total number of ",length(m_frcst), " forecast basin rainfall time series have been retrieved from WIMES"))
  
  m_GFS = grep(pattern=fcstRainTSpattern, x=FrcstRain_TS$code) # select forecast time series for which the code match the given pattern
  FrcstRain_TS = FrcstRain_TS[m_GFS,] # Time series of only required
  
  if (is.null(currentDateTime)) 
    {
    Start_ProdDate = as.POSIXct(Sys.time(),tz="UTC") #start production date of forecast grids is now
    attributes(Start_ProdDate)$tzone<-"UTC"
    maxProdDate <- Start_ProdDate 
    }
  else 
    {
    Start_ProdDate = currentDateTime
    maxProdDate <- Start_ProdDate #restrict maxProdDate to be prior to currentDateTime when you do reforecast
    }
  #Start_ProdDate = as.POSIXct(Sys.Date()) #start production date of forecast grids
  attributes(Start_ProdDate)$tzone <- "UTC" # convert the date time into "UTC" time zone
  Start_ProdDate    = Start_ProdDate - as.difftime(100, units="days")
  Start_ProdDate    = as.POSIXct(Start_ProdDate) # make sure the date is a POSIXct object (and not a POSIXct)
  LastProdDateS     = getProductionDates(FrcstRain_TS$code[1], beginDateTime=Start_ProdDate)$productionDateTime
  #only keep forecasts before currentDateTime
  LastProdDateS     = LastProdDateS[LastProdDateS<=maxProdDate]
  LastProdDate      = max(LastProdDateS)
  if (LastProdDate==-Inf) logWarning(paste0("There was no production date more recent than ",Start_ProdDate," forecasts will be made with future rainfall equal to 0"))
  else logInfo(paste0("For the TS basin rainfall forecast ",FrcstRain_TS$code[1]," last production date found is : ",LastProdDate ))
  
  # .. 3.2 - Read forecast precipitation data from WIMES: loop on basins' code ----
  logInfo("'runSimuBasinsFlowsGR' -  3.2 - Read forecast precipitation data from WIMES: loop on basins' code")
  
  fcstBasinRains = getFcstDataByTScodepattern(date_prod      = LastProdDate, 
                                              date_start     = simu_start, 
                                              date_end       = simu_end, 
                                              Sites          = Basin_Sites, 
                                              TScode_pattern = fcstRainTSpattern, 
                                              tstep = as.difftime(tstepFcstHour, units="hours")) # TODO: adapt to the timestep
  
  # convert to the working time step
  fcstBasinRains = convertDFtimestep(df=fcstBasinRains, 
                                     newTimeStep=tstep, 
                                     cumul=FALSE, outVerbose) # cumul is set to false, because the GFS forecast are intensities (big TODO!!)
  }
  
  # 4 - BINDING observed rainfall and forecasted rainfall ---- 

  basin_rains_dates = seq(from=simu_start, to = simu_end, by=tstep) # initialization of the global rainfall date time sequence. It will gather both observed and forecasted rainfall
  attributes(basin_rains_dates)$tzone = "UTC" # make sure we are in "UTC" time zone
  basin_rains_dates = as.POSIXct(basin_rains_dates) # convert (if needed) to POSIXct format (sometimes it is in POSIXct format)
  basin_rains_dates = rev(basin_rains_dates) # we have to reverse dates because the RWIMES API provide data from the most recent to the less recent observation
  #logInfo(paste("minimum date for basin rainfall is ",min(basin_rains_dates),sep=""))
  basin_rains = matrix(NA, nrow=length(basin_rains_dates), ncol=length(Basin_Sites$code))
  colnames(basin_rains)= Basin_Sites$code # Assign timeseries codes to each column's name. It is easier for debugging the code afterwards
  
  # 4.1 - deal with observed data first
  #if hindcast was selected (i.e. currentDateTime!=NULL and fcstRainTSpattern==NULL), observations are likely to cover the whole simulation time period
  m_date = match(as.character(basin_rains_dates, format="%Y%m%dT%H%M%S"), 
                 as.character(obsBasinRains$date, format="%Y%m%dT%H%M%S"))
  for (cd in seq(along=Basin_Sites$code)){
    m_TS = grep(pattern = Basin_Sites$code[cd], x=colnames(obsBasinRains)) # correspondance between the name of the global rainfall data and the observed one
    if (length(m_TS)==0){
      logWarning(paste0("No observed rainfall time series was found corresponding to basin: ",Basin_Sites$code[cd]))
    }
    if (length(m_TS)>=2){
      logWarning(paste0("At least 2 observed rainfall time series were found corresponding to basin: ",Basin_Sites$code[cd]))
    }
    basin_rains[,cd] = obsBasinRains[m_date,m_TS[1]]
  }
  
  if (!is.null(fcstRainTSpattern)) { #case where you do forecast or reforecast, otherwise rainfall will remain NA (and then 0 for the sake of GR4 computations)
  # 4.2 - get only the most recent forecasted values. It is possible that last production of forecast is some hours earlier
  if (is.null(currentDateTime)) dateLastObs = max(obsBasinRains$date) # get the most recent date with observed data
  else dateLastObs = currentDateTime
  #dateLastObs = max(obsBasinRains$date) # get the most recent date with observed data
  m_date = which(fcstBasinRains$date <= dateLastObs  | fcstBasinRains$date > simu_end) #deselect  forecasted data before that datetime (last observed data)
  fcstBasinRains = fcstBasinRains[-m_date,] # delete forecast data older than the dateLastObs
  
  # 4.3 - secondly, deal with forecasted data
  m_date_inv = match(as.character(fcstBasinRains$date, format="%Y%m%dT%H%M%S"),
                     as.character(basin_rains_dates, format="%Y%m%dT%H%M%S"))
  for (cd in seq(along=Basin_Sites$code)){
    m_TS = grep(pattern = Basin_Sites$code[cd], x=colnames(fcstBasinRains)) # correspondance between the name of the global rainfall data and the observed one
    if (length(m_TS)==0){
      logWarning(paste0("No forecasted rainfall time series was found corresponding to basin: ",Basin_Sites$code[cd]))
    }
    if (length(m_TS)>=2){
      logWarning(paste0("At least 2 forecasted rainfall time series were found corresponding to basin: ",Basin_Sites$code[cd]))
    }
    basin_rains[m_date_inv,cd] = fcstBasinRains[,m_TS[1]]
  }
  }
  
  
  
  # 5 - READ GR INITIAL STATES FROM WIMES ----
  logInfo("'runSimuBasinsFlowsGR' - 5 - READ GR INITIAL STATES FROM WIMES")
  
  # .. 5.1 - Get the list of GR Production store time series related to each basin ----
  logInfo("'runSimuBasinsFlowsGR' -  5.1 - Get the list of GR Production store time series related to each basin")
  #Production store 
  Prod_TS=getAllTimeSeries(  sitesCodes = Basin_Sites$code)
  m_Prod=which(Prod_TS$parameterNameCode=="Ze")
  Prod_TS = Prod_TS[m_Prod,]
  m_Prod=grep(pattern="S_sim", x=Prod_TS$code)
  Prod_TS = Prod_TS[m_Prod,] # Time series of only required
  logInfo(paste0("A total number of ",length(m_Prod), " storage elevation time series have been retrieved from WIMES"))
  m_GR4 = grep(pattern="GR4", x=Prod_TS$code)
  Prod_TS = Prod_TS[m_GR4,] # Time series of only required
  
  # .. 5.2 - Check whether the interpolation period of each TS match with the timestep (argument of the function) ----
  for (i in 1:length(Prod_TS$code)) {
    checkInterpolationPeriod(   Prod_TS$code[i], tstep)
  }
  
  # .. 5.3 - Read GR4 production store data from WIMES: loop on basins' code ----
  Prod_dates = seq(from=simu_start, to = simu_end, by=tstep)
  attributes(Prod_dates)$tzone = "UTC" # make sure we are in "UTC" time zone
  Prod_dates = rev(Prod_dates) # we have to reverse dates because the RWIMES API provide data from the most recent to the less recent observation
  Prod_past = matrix(NA, nrow=length(Prod_dates), ncol=length(Prod_TS$code))
  colnames(Prod_past) = Prod_TS$code # Assign timeseries codes to each column's name. It is easier for debugging the code afterwards
  
  # read simulated GR4 production store data from WIMES: loop on basins' code
  for (i in 1:length(Prod_TS$code)) {
    #restrict the search to 15 days before and 15 days after simu_start
    storagedata = getObservedValues(Prod_TS$code[i], beginDateTime=simu_start-as.difftime(15,unit="days"), endDateTime=simu_start+as.difftime(15,unit="days"))
    if (is.null(dim(storagedata))){
      logInfo(paste0("No data could be retrieved from TS '",Prod_TS$code[i], "' during the specified simulation period" ))
    } else {
      storagedata_date   = storagedata$observationDateTime
      m_data             = match(as.character(Prod_dates, format="%Y%m%dT%H%M%S"), 
                                 as.character(storagedata_date, format="%Y%m%dT%H%M%S"))
      Prod_past[,i]      = storagedata$value[m_data]
    }
  }
  
  # .. 5.4 - Get the list of GR Routing store time series related to each basin ----
  logInfo("'runSimuBasinsFlowsGR' -  5.4 - Get the list of GR Routing store time series related to each basin")
  #Production store 
  Rout_TS=getAllTimeSeries(  sitesCodes = Basin_Sites$code)
  m_Rout=which(Rout_TS$parameterNameCode=="Ze")
  Rout_TS = Rout_TS[m_Rout,]
  m_Rout=grep(pattern="R_sim", x=Rout_TS$code)
  Rout_TS = Rout_TS[m_Rout,] # Time series of only required
  logInfo(paste0("A total number of ",length(m_Rout), " Routing store time series have been retrieved from WIMES"))
  m_GR4 = grep(pattern="GR4", x=Rout_TS$code)
  Rout_TS = Rout_TS[m_GR4,] # Time series of only required
  
  # .. 5.5 - Check whether the interpolation period of each TS match with the timestep (argument of the function) ----
  logInfo("'runSimuBasinsFlowsGR' -  5.5 - Check whether the interpolation period of each TS match with the timestep (argument of the function)")
  for (i in 1:length(Rout_TS$code)) {
    checkInterpolationPeriod(   Rout_TS$code[i], tstep)
  }
  
  # .. 5.6 - Read GR4 routing store data from WIMES: loop on basins' code ----
  logInfo("'runSimuBasinsFlowsGR' -  5.6 - Read GR4 routing store data from WIMES: loop on basins' code")
  Rout_dates = seq(from=simu_start, to = simu_end, by=tstep)
  attributes(Rout_dates)$tzone = "UTC" # make sure we are in "UTC" time zone
  Rout_dates = rev(Rout_dates) # we have to reverse dates because the RWIMES API provide data from the most recent to the less recent observation
  Rout_past=matrix(NA, nrow=length(Rout_dates), ncol=length(Rout_TS$code))
  colnames(Rout_past)=Rout_TS$code # Assign timeseries codes to each column's name. It is easier for debugging the code afterwards
  
  # read routing store data from WIMES: loop on basins' code
  for (i in 1:length(Rout_TS$code)) {
    #restrict the search to 15 days before and 10 days after simu_start
    storagedata = getObservedValues(Rout_TS$code[i], beginDateTime=simu_start-as.difftime(15,unit="days"), endDateTime=simu_start+as.difftime(15,unit="days"))
    if (is.null(dim(storagedata))){
      logInfo(paste0("No data could be retrieved from TS '",Rout_TS$code[i], "' during the specified simulation period" ))
    } else {
      storagedata_date   = storagedata$observationDateTime
      m_data             = match(as.character(Rout_dates, format="%Y%m%dT%H%M%S"), 
                                 as.character(storagedata_date,format="%Y%m%dT%H%M%S"))
      Rout_past[,i]      = storagedata$value[m_data]
    }
  }
  
  # 6 - CALCULATE Potential Evapotranspiration based on Oudin equation ----
  logInfo("'runSimuBasinsFlowsGR' - 6 - Compute Potential Evapotranspiration based on Oudin equation")
  dates_Oudin_start <- round(min(basin_rains_dates), units="days") - as.difftime(1, units = "days") # start date depends on available rain data. And we substract 1 day to give buffer time
  dates_Oudin_end   <- round(max(basin_rains_dates), units="days") + as.difftime(1, units = "days") # end date depends on available rain data. And we add 1 day to give extra buffer time
  dates_ETP_Oudin <- seq(from= dates_Oudin_start, to= dates_Oudin_end , by="day") #Oudin's equation provide daily ETP (not hourly)
  attributes(dates_ETP_Oudin)$tzone = "UTC" 
  dates_ETP_Oudin <- as.POSIXlt(dates_ETP_Oudin) #the PEdaily_Oudin function requires dates to be in POSIXlt format
  temperature = rep(x = 25, times = length(dates_ETP_Oudin))
  ETP_Oudin <- PEdaily_Oudin(JD = dates_ETP_Oudin$yday, Temp = temperature, Lat = 0, LatUnit = "deg")
  dates_ETP_Oudin <- as.POSIXct(dates_ETP_Oudin) #
  
  basin_ETP=data.frame(date=dates_ETP_Oudin, ETP=ETP_Oudin) # include ETP data in a dataframe
  
  #convert the time step of the ETP data to the working time step
  basin_ETP = convertDFtimestep(df=basin_ETP, 
                                newTimeStep=tstep, 
                                cumul=TRUE, outVerbose) # cumul is set to TRUE because evapotrasnpiration is cumulative
  
  # 7 - SIMULATION of INDEPENDENT SUB-BASINS - loop on Nat_ID or sub-basins ------------------------------------
  logInfo("'runSimuBasinsFlowsGR' -  7 - SIMULATION of INDEPENDENT SUB-BASINS")
  
  # .. 7.1 - Initialization of the data.frame of simulated flows for each subbasin ---------------------
  logInfo("'runSimuBasinsFlowsGR' -  7.1 - INITIALIZATION of the data.frame of simulated flows for each subbasin")
  qq_dates <- seq(from = simu_start , to = simu_end , by = tstep)
  attributes(qq_dates)$tzone = "UTC" # make sure we are in "UTC" time zone
  qq_sim   <- matrix(nrow=length(qq_dates), ncol=length(Basin_Sites$code))
  colnames(qq_sim) <- Basin_Sites$code
  Prod_sim=qq_sim #initialization of the matrix Prod_sim (same size as qq_sim)
  Rout_sim=qq_sim #initialization of the matrix Rout_sim (same size as qq_sim)
  
  # .. 7.2 - GR simulation : loop on each basin ----
  logInfo("'runSimuBasinsFlowsGR' -  7.2 - GR simulation : start loop on each basin")
  for (cd in seq(along = Basin_Sites$code)) {
    if (outVerbose){
      logInfo(paste0("Starts running GR model for basin ",Basin_Sites$code[cd]))
    }
    #cd_ETP=which(paste0(Code_stations_ETP,"_BAS")==Basin_Sites$code[cd])
    cd_calib=which(paste0(Code_stations_calib,"_BAS")==Basin_Sites$code[cd])
    if (outVerbose){
      logInfo(paste0("cd_calib=",cd_calib))
    }
    
    #### Get the rain data of a specific basin
    Rain=data.frame(date=rev(basin_rains_dates),value=rev(basin_rains[,cd])) # up to now, we were using observed data in anti-chronological order, so use 'rev' to put them in chronological order
    # print(Rain[1:10,]) # to be commented
    dates_P <- as.POSIXct(Rain$date)
    #logInfo(paste("minimum date for rainfall is ",min(dates_P),sep=""))
    tstep_P <- min(abs(dates_P[2:length(dates_P)]-dates_P[1:(length(dates_P)-1)])) # time step of Precipitation data
    m_na_P  <- which(is.na(Rain$value)) #get the indexes corresponding to NA's value
    if (length(m_na_P)>0){
      Rain    <- Rain[-m_na_P,] #Eject lines with NA values
    }
    dates_P <- as.POSIXct(Rain$date) # get the dates once more. Indeed, some indexes may have been deleted
    logInfo(paste("minimum date for rainfall is ",min(dates_P),sep=""))
    
    #### Get the ETP data of a specific basin
    ETP = data.frame(date=(basin_ETP$date),value=(basin_ETP$ETP)) # ETP data are already in chronological order
    dates_ETP <- as.POSIXct(ETP$date)
    tstep_ETP <- min(abs(dates_ETP[2:length(dates_ETP)]-dates_ETP[1:(length(dates_ETP)-1)])) # time step of Precipitation data
    m_na_ETP  <- which(is.na(ETP$value)) #get the indexes corresponding to NA's value
    if (length(m_na_ETP)>0){
      ETP    <- ETP[-m_na_ETP,] #Eject lines with NA values
    }
    dates_ETP <- as.POSIXct(ETP$date) # get the dates once more. Indeed, some indexes may have been deleted
    logInfo(paste("minimum date for PET is ",min(dates_ETP),sep=""))
 
    # #### READ ETP data from file
    # #TODO: get ETP data from the WIMES platform
    # if (ETP_input == "Oudin") {
    #   dates_ETP=dates_ETP_Oudin
    #   ETP <- data.frame(dates_ETP_Oudin, ETP_Oudin)
    # }
    # 
    # tstep_ETP=min(abs(dates_ETP[2:length(dates_ETP)]-dates_ETP[1:length(dates_ETP)-1])) # time step of ETP data
    if ((tstep_P!=tstep_ETP) & outVerbose){
      logInfo("Precipitation and ETP data do not have the same time step")
    }
    
    #selection of the common TIME WINDOW
    date_start<-max(min(dates_P), min(dates_ETP)) # defining the start date of the GR simulation
    date_end<-min(max(dates_P), max(dates_ETP)) # defining the end date of the GR simulation
    if (outVerbose){
      logInfo(paste0("In basin ", Basin_Sites$code[cd],", max date for ETP data is: ",max(dates_ETP)))
      logInfo(paste0("In basin ", Basin_Sites$code[cd],", max date for Rainfall data is: ",max(dates_P)))
    }
    
    if (date_start>date_end) {
      logWarning(paste0("In basin", Basin_Sites$code[cd],", NO common time WINDOW is available for P and ETP data"))
    } else {
      logInfo(paste0("In basin ", Basin_Sites$code[cd],", common time WINDOW of P and ETP data FROM: ",date_start, ", TO: ", date_end))
    }
    
    # selection of dates corresponding to date_start and date_end (for Rains data)
    # m_start_P  <- max(  which( dates_P<=(trunc(date_start, units = units(tstep_P))) )   ) # assume dates are in chronological order
    # m_end_P    <- min( which(dates_P >= (trunc(date_end, units = units(tstep_P))) ) )        # assume dates are in chronological order
    m_com_P = which(dates_P >= date_start & dates_P <= date_end)
    Rain = Rain[m_com_P,]
    dates_P <- as.POSIXct(Rain$date)
    
    # selection of dates corresponding to date_start and date_end (for ETP data)
    # m_start_ETP <- max( which(dates_ETP <= (trunc(date_start, units = units(tstep_ETP)))  ) )
    # m_end_ETP   <- min( which(dates_ETP >= (trunc(date_end, units = units(tstep_ETP)) + tstep_ETP)  ) )
    m_com_ETP = which(dates_ETP >= date_start & dates_ETP <= date_end)
    ETP = ETP[m_com_ETP,]
    dates_ETP <- as.POSIXct(ETP$date)
    
    #A vector with the whole list of dates that are included in the time window
    # input_dates <- dates_P[m_start_P:m_end_P]
    input_dates_tstep <- seq(from = date_start , to = date_end , by = tstep) # same time window but we use the timestep of simulation: 'tstep'
    attributes(input_dates_tstep)$tzone = "UTC"
    
    #check length of input rain data and input PET data
    if (length(dates_P) != length(dates_ETP)){
      logWarning("Precipitation dates and PET dates have not the same length")
      logInfo(paste0("Precipitation dates have a length of: ", length(dates_P)))
      logInfo(paste0("PET dates have a length of: ", length(dates_ETP)))
      logInfo(paste0("Input_dates_tstep dates have a length of: ", length(input_dates_tstep)))
      if (length(dates_P) < length(input_dates_tstep)){
        mDates = match(input_dates_tstep,dates_P)
        mGapDates = which(is.na(mDates)) # get the indexes of dates_P that are not included within input_dates_tstep
        logInfo(paste0("A total number of ",length(mGapDates)," value(s) are to be added to input Rain dataframe."))
        P_temp = data.frame(date = input_dates_tstep, value = Rain$value[mDates]) # create a temporary dataframe which has the "good" length
        P_temp$value[mGapDates] = 0 # set the missing rainfall values to zero
        Rain = P_temp
        dates_P <- as.POSIXct(Rain$date)
        
      } else if ( length(dates_ETP) < length(input_dates_tstep)){
        # in theory, this case should never happen
        mDates = match(input_dates_tstep,dates_ETP)
        mGapDates = which(is.na(mDates)) # get the indexes of dates_ETP that are not included within input_dates_tstep
        logInfo(paste0("A total number of ",length(mGapDates)," value(s) are to be added to input ETP dataframe."))
        ETP_temp = data.frame(date = input_dates_tstep, value = ETP$value[mDates]) # create a temporary dataframe which has the "good" length
        ETP_temp$value[mGapDates] = mean(ETP$value, na.rm=TRUE) # set the missing value to the averaged value of evapotranspiration
        ETP = ETP_temp
        dates_ETP <- as.POSIXct(ETP$date)
      }
    } else {
      if (outVerbose){
        logInfo(paste0("In basin ", Basin_Sites$code[cd]," Ok, precipitation dates and PET dates do have the same length."))
      }
    }
    
    # CONVERT data to the SIMULATION timestep defined at the beginning of the script ('tstep')
    don_tstep=array(NA,dim=c(length(input_dates_tstep),2))
    don_tstep[,1] = Rain$value
    don_tstep[,2] = ETP$value
    
       
    # Check that there is no NA values within don_tstep
    m_na=which(is.na(don_tstep))
    if (length(m_na)!=0) {
      logWarning(paste0(Basin_Sites$code[cd],": there are NA values (P or ETP) within the common time window"))
    }
    
    ## preparation of InputsModel object
    if (ModelTool=="GR4J"){
      InputsModel <- CreateInputsModel(FUN_MOD=RunModel_GR4J, DatesR=input_dates_tstep ,Precip=don_tstep[,1],PotEvap=don_tstep[,2])
    } else if (ModelTool=="GR4H"){
      InputsModel <- CreateInputsModel(FUN_MOD=RunModel_GR4H, DatesR=input_dates_tstep ,Precip=don_tstep[,1],PotEvap=don_tstep[,2])
    }
    
    ## calibration period selection (note that the warmup period was set in days, so we have to convert it)
    ## check also whether there the simulation period is long enough to take into account the warmup period
    if ( (max(input_dates_tstep)-min(input_dates_tstep))<=as.difftime(2*warmup_duration, units="days") ){
      logInfo("Warmup period longer than half of the simulation period: warmup deleted ")
      Ind_Run <- seq( min( which(input_dates_tstep >= simu_start) + 2*tstep ) ,
                      max( which(input_dates_tstep <= simu_end)   )      )
      iswarmup=FALSE
    } else {
      Ind_Run <- seq( min( which(input_dates_tstep >= simu_start) ) + warmup_duration*(24/as.numeric(tstep, units="hours")),
                      max( which(input_dates_tstep <= simu_end)   )      )
      iswarmup=TRUE
    }
    
    
    #Create initial states
    if (ModelTool=="GR4J"){
      IniStates=CreateIniStates(FUN_MOD=RunModel_GR4J, InputsModel=InputsModel, 
                                ProdStore = CalibMatrix_DF$X1[cd_calib]*0.25, 
                                RoutStore = CalibMatrix_DF$X3[cd_calib]*0.25)
    } else if (ModelTool=="GR4H"){
      notNA <- which(!is.na(Prod_past[,cd_calib]))
      m_S0 <- which.min(abs(Prod_dates[notNA]-min(input_dates_tstep)))
      #m_S0 <- max( which((Prod_dates>=min(input_dates_tstep)) & (!is.na(Prod_past[,cd])))) # get the closest date to the oldest not NA value
      #take the maximum index because Prod_dates are decreasing
      if (!identical(m_S0, integer(0))) {
        S0 <- Prod_past[notNA[m_S0],cd_calib]
        if (is.na(S0)) {
          S0=CalibMatrix_DF$X1[cd_calib]*0.25
          logInfo("S0 will be initialized with a value of 25% of X1 because there are only NA values")
        }
        else logInfo(paste("S0 will be initialized with a value of ",S0," computed for time ",Prod_dates[notNA[m_S0]],sep=""))
      } else {
        S0=CalibMatrix_DF$X1[cd_calib]*0.25
        logInfo("S0 will be initialized with a value of 25% of X1 because there was no value inside the allowed time window")
      }
      
      notNA <- which(!is.na(Rout_past[,cd_calib]))
      m_R0 <- which.min(abs(Rout_dates[notNA]-min(input_dates_tstep)))
      #m_R0 <- max( which((Rout_dates>=min(input_dates_tstep)) & (!is.na(Rout_past[,cd])))) # get the closest date to the oldest not NA value
      #take the maximum index because Prod_dates are decreasing
      if (!identical(m_R0, integer(0))) {
        R0 <- Rout_past[notNA[m_R0],cd_calib]
        if (is.na(R0)) {
          R0=CalibMatrix_DF$X3[cd_calib]*0.25
          logInfo("S0 will be initialized with a value of 25% of X3 because there are only NA values")
        }
        else logInfo(paste("R0 will be initialized with a value of ",R0," computed for time ",Rout_dates[notNA[m_R0]],sep=""))
      } else {
        R0=CalibMatrix_DF$X3[cd_calib]*0.25
        logInfo("R0 will be initialized with a value of 25% of X3 because there was no value inside the allowed time window")
      }
      
      IniStates=CreateIniStates(FUN_MOD=RunModel_GR4H, InputsModel=InputsModel, 
                                ProdStore = S0, 
                                RoutStore = R0)
    }
    
    ## preparation of RunOptions object
    if (ModelTool=="GR4J"){
      if (iswarmup) {
        RunOptions <- CreateRunOptions(FUN_MOD=RunModel_GR4J,InputsModel=InputsModel,IndPeriod_Run=Ind_Run, IndPeriod_WarmUp=1:(Ind_Run[1]-1), IniStates = IniStates)
      } else {
        RunOptions <- CreateRunOptions(FUN_MOD=RunModel_GR4J,InputsModel=InputsModel,IndPeriod_Run=Ind_Run, IndPeriod_WarmUp=1:(Ind_Run[1]-1), IniStates = IniStates)
      }
      
    } else if (ModelTool=="GR4H"){
      if (iswarmup) {
        RunOptions <- CreateRunOptions(FUN_MOD=RunModel_GR4H,InputsModel=InputsModel,IndPeriod_Run=Ind_Run, IndPeriod_WarmUp=1:(Ind_Run[1]-1), IniStates = IniStates)
      } else {
        RunOptions <- CreateRunOptions(FUN_MOD=RunModel_GR4H,InputsModel=InputsModel,IndPeriod_Run=Ind_Run, IndPeriod_WarmUp=1:(Ind_Run[1]-1), IniStates = IniStates)
      }
    }
    
    ## RUN the GR4J/GR4H model
    if (ModelTool=="GR4J"){
      OutputsModel <- RunModel(InputsModel=InputsModel,RunOptions=RunOptions,
                               Param=as.numeric(CalibMatrix_DF[cd_calib,2:5]), FUN=RunModel_GR4J)
    } else if (ModelTool=="GR4H"){
      logInfo("the simulation will be run with:")
      logInfo(paste("X1 = ",CalibMatrix_DF[cd_calib,2],sep=""))
      logInfo(paste("X2 = ",CalibMatrix_DF[cd_calib,3],sep=""))
      logInfo(paste("X3 = ",CalibMatrix_DF[cd_calib,4],sep=""))
      logInfo(paste("X4 = ",CalibMatrix_DF[cd_calib,5],sep=""))
      logInfo(paste("the basin area is ",Basin_Sites$Surf_area_km2[cd],sep=""))
      OutputsModel <- RunModel(InputsModel=InputsModel,RunOptions=RunOptions,
                               Param=as.numeric(CalibMatrix_DF[cd_calib,2:5]), FUN=RunModel_GR4H)
    }
    
    
    #### Conversion of 'mm/tstep'  into 'm^3/s'. where tstep is the timestep in hours
    QQ <- OutputsModel$Qsim/1000 *(Basin_Sites$Surf_area_km2[cd]*10^6)/(3600*as.numeric(tstep,units="hours")) 
    
    
    m_qqQQ=match(as.character.Date(qq_dates, format="%Y%m%dT%H%M%S"),
                 as.character.Date(input_dates_tstep[Ind_Run], format="%Y%m%dT%H%M%S"))
    qq_sim[,cd] <- QQ[m_qqQQ]
    Prod_sim[,cd] <- OutputsModel$Prod[m_qqQQ] # save simulated values of the production store
    Rout_sim[,cd] <- OutputsModel$Rout[m_qqQQ] # save simulated values of the routing store
    
  }
  
  if (is.null(currentDateTime)) {
  # 8 - WRITE FORECASTED FLOWS INTO WIMES ----
  logInfo("'runSimuBasinsFlowsGR' - 8 - WRITE FORECASTED FLOWS INTO WIMES")
  #.. 8.1 - Get the list of forecast FLOW time series related to each basin ----
  logInfo("'runSimuBasinsFlowsGR' -  8.1 - Get the list of forecast FLOW time series related to each basin")
  
  FrcstFlow_TS=getAllTimeSeries(  sitesCodes = Basin_Sites$code)
  m_frcst=which((FrcstFlow_TS$forecast=="TRUE") & (FrcstFlow_TS$parameterNameCode=="Q"))
  FrcstFlow_TS = FrcstFlow_TS[m_frcst,]
  logInfo(paste0("A total number of ",length(m_frcst), " forecast basin rainfall time series have been retrieved from WIMES"))
  
  m_GR4=grep(pattern="GR4", x=FrcstFlow_TS$code)
  FrcstFlow_TS = FrcstFlow_TS[m_GR4,] # Time series of only required
  
  # .. 8.2 - choose the indexes/dates that we want to write onto WIMES ----
  lookback_forecast = 5 # lookback duration period in days. Usually 5 days. The prupose is to give some hindcast data to the forecast expert at KMD
  export_date_start = now_date - as.difftime(lookback_forecast, units="days")
  export_date_start = as.POSIXct(export_date_start) #make sure it is a POSIXct object
  m_export=which(qq_dates>=export_date_start & qq_sim[,1]) # search indexes of dates more recent than the export_date_start, and for which there are no 'NA' values
  
  # .. 8.3 - Write in WIMES ----
  m_bvs = match(Basin_Sites$code,FrcstFlow_TS$siteCode)
  n_upsert = 0
  internProgress=0 # internal progress variable
  updateProgression(0, ".. start sending data to WIMES..")
  
  for (cd in seq(along = FrcstFlow_TS$siteCode)) {
    #create a data frame with data to be exported to WIMES
    dischargeExport2WIMES_df = data.frame(timeSeriesCode   = FrcstFlow_TS$code[m_bvs[cd]], 
                                          productionDateTime = now_date, 
                                          forecastDateTime   = qq_dates[m_export],
                                          value              = qq_sim[m_export,cd])
    n_upsert = n_upsert + length(qq_sim[m_export,cd])
    
    internProgress = (cd /length(FrcstFlow_TS$siteCode))*100
    if (internProgress<100){
      updateProgression(floor(internProgress), ".. sending data to WIMES..")
    } else {
      updateProgression(floor(internProgress), ".. END sending data to WIMES.")
    }
    
    #Use the specific RWIMES function to upload several values at once
    upsertQuantForecastValues(dischargeExport2WIMES_df)
    
  }
  logInfo(paste0("A total number of ",as.character(n_upsert), " values have just been sent to WIMES for forecasted basin flows."))
  
  
  # 9 - WRITE SIMULATED PRODUCTION and ROUTING STORES ELEVATION INTO WIMES ----
  logInfo("'runSimuBasinsFlowsGR' - 9 - WRITE SIMULATED PRODUCTION and ROUTING STORES ELEVATION INTO WIMES")
  if(exportStorDurationDays<0){
    logWarning("__ 'WRITE SIMULATED PRODUCTION and ROUTING': The parameter exportStorDurationDays has a negative value, its absolute part will be kept")
    exportStorDurationDays = abs(exportStorDurationDays)
  }
  if(outVerbose){
    logInfo(paste0("__ 'WRITE SIMULATED PRODUCTION and ROUTING': a total duration of ",exportStorDurationDays," days will be sent to WIMES."))
  }
  prodRoutStartExportDuration = as.difftime(exportStorDurationDays, units = "days") # only the last 'exportStorDurationDays' days of simulation will be sent to WIMES (by default = 10)
  prodRoutStartExportDate = now_date - prodRoutStartExportDuration
  
  n_upsert = 0 # it counts how many values are sent to WIMES
  internProgress=0 #D internal progress variable
  updateProgression(0, ".. start sending data to WIMES")
  
  for (cd in seq(along = Prod_TS$code)){
    
    # define the time window (or indexes within qq_dates) of export
    m_export = which((qq_dates < now_date) 
                     & (qq_dates > prodRoutStartExportDate)
                     & (!is.na(Prod_sim[,cd])) 
                     & (!is.na(Rout_sim[,cd])))
    
    #Create a data frame of simulated GR4 Production store level that will be sent to WIMES
    prodExport2WIMES_df = data.frame(timeSeriesCode      = Prod_TS$code[cd],
                                     observationDateTime = qq_dates[m_export],
                                     value               = Prod_sim[m_export,cd])
    
    #Create a data frame of simulated GR4 Routing store level that will be sent to WIMES
    routExport2WIMES_df = data.frame(timeSeriesCode      = Rout_TS$code[cd],
                                     observationDateTime = qq_dates[m_export],
                                     value               = Rout_sim[m_export,cd])
    
    #upload the dataframe to the WIMES server
    upsertQuantValuesBig(rbind(prodExport2WIMES_df, routExport2WIMES_df))
    n_upsert = n_upsert + length(m_export)
    internProgress = internProgress + (1 /(length(Prod_TS$code)))*100
    internProgress = min(internProgress, 100)
    
    updateProgression(floor(internProgress), ".. sending data to WIMES..")
    
  }
  logInfo(paste0("A total number of ",as.character(n_upsert), " values have just been sent to WIMES for production and routing stores."))
  }
  # 9 - END of the function ----
  qq_sim_df=data.frame(date=qq_dates, qq_sim) # create a dataframe to be returned
  
  logInfo("END of the function 'runSimuBasinsFlowsGR', a data frame is returned.")
  logInfo("====================")
  logInfo(" ")
  
  return(qq_sim_df)
  
}

# _----

#' Run cumulated routed flows
#'
#' @param qq_sim_df a data frame object returned by the function 'runSimuBasinsFlowsGR', first column relates to date time info
#' @param outVerbose logical (optionnal, default FALSE) - defines whether information messages should be printed (e.g. max celerity, dx, etc..
#' @param obsTScode_pattern string vector - it defines the pattern of observed timeseries codes where we want to write (e.g. observed flow timeseries codes are like 'WRA_1ED01_LUS_Q_1h')
#' @param fcstTScode_pattern string - it defines the pattern of forecasted timeseries codes where we want to write (e.g. observed rainfall timeseries codes are like 'GR4_1EF01_NZO_Q_sim_1h')
#' @param correctForecast logical - indicates whether you use real time observations to correct forecast
#' @param currentDateTime POSIXct - date when observation are supposed to stop (set to NULL in case of real time forecast insofar observed data will automatically stop at actual currentDateTime)
#' @param outVerbose logical - tells whether you print verbose log information
#' @return list - list containing two data frames, one with the flows actually routed, the other one with auxiliary flows (ie. "locally" corrected ones if correctForecast==FALSE, "locally" raw flows otherwise)
#' @note
#' if currentDateTime==NULL ==> forecast 
#' if currentDateTime!=NULL ==> hindcast or reforecast
#' if correctForecast==TRUE ==> use observation instead of "forecast" before current date time and correct forecast after currentDateTime according to "correcFcstWithObs" function
#' @export
#'
runCumulatedRoutedFlows=function(qq_sim_df, obsTScode_pattern, fcstTScode_pattern, correctForecast=TRUE, currentDateTime=NULL, outVerbose=FALSE){
  logInfo("The function 'runCumulatedRoutedFlows' has just been called.")
  # 1 - Introduction ----
  logInfo("'runCumulatedRoutedFlows' - 1 - Introduction")
  qq_dates = qq_sim_df$date
  tstep        = min(abs(qq_dates[2:length(qq_dates)]-qq_dates[1:length(qq_dates)-1])) # time step of simulated flow data
  logInfo(paste0('... the timestep of simulated independent basin flows is ', as.character(tstep, units="hours")," hour(s)."))
  qq_sim   = qq_sim_df[,2:length(qq_sim_df)]
  
  # .. 1.2 - Delete NA values ----
  for (cd in 1: dim(qq_sim)[2]){
    m_na=which(is.na(qq_sim[,cd]))
    if (length(m_na)!=0){
      qq_sim=qq_sim[-m_na,]
      qq_dates=qq_dates[-m_na]
    }
  }
  
  
  
  ########## .. 1.4 - Input parameters  ----
  logInfo("'runCumulatedRoutedFlows' - 1.4 - Input parameters")
  
  # Node_routed_file="Node_routedlink_Matrix.csv"
  # routing_param_file = "Flow_Routing_Param.csv"
  
  Node_routedlinks <- getNodeRoutedLinks()
  Node_routedlinks_mat <- raster::as.matrix(Node_routedlinks[,2:dim(Node_routedlinks)[2]])
  Code_routedlinks <- as.character(Node_routedlinks$Nat_ID)
  
  # 2 - Get the list of river water level  stations --------------------------------------------- 
  logInfo("'runCumulatedRoutedFlows' - 2 - Get the list of AWL stations")
  Sites <- getSites(  ) # get all the sites within the WIMES application
  m_AWL_sites=grep(pattern="*Water level station", x=Sites$name)
  AWL_Sites = Sites[m_AWL_sites,] # rain_Sites is only the 'Point' sites of which the code finishes with the pattern "Weather Station"
  
  # CHECK that all selected sites (AWL_Sites) are of Point type
  m_nopoly=rep(NA,length(AWL_Sites$code)) #initialization of a vector of same length as rain_Sites
  for (i in 1:length(AWL_Sites$code)) {
    site <- getSite(AWL_Sites$code[i])
    if (site$geometry$type!="Point"){
      logWarning(paste0("The site: ",site$code," is NOT of type 'Point', while required")) # if the geometry type is NOT a MultiPolygon, then we send a warning
      m_nopoly[i]=i
    } 
  }
  if (is.na(m_nopoly)==FALSE) {AWL_Sites = AWL_Sites[-m_nopoly,]} #reject every sites that is not recognised as a 'Point' 
  
  
  # 3 - READ FORECAST AWL Flow TIME SERIES from WIMES ----------------------------
  logInfo("'runCumulatedRoutedFlows' -  3 - READ FORECAST AWL Flow TIME SERIES from WIMES")
  
  # .. 3.1 - Get the list of forecast AWL flow time series ----
  logInfo("'runCumulatedRoutedFlows' -  3.1 - Get the list of forecast AWL flow time series")
  m_AWL     = which(nchar(Code_routedlinks)==5) #indexes of column names within Code_routedlinks that have 5 characters: e.g. "1EF01"
  AWL_codes = Code_routedlinks[m_AWL]
  Qsim_TS   = getAllTimeSeries(  sitesCodes=AWL_Sites$code)
  m_frcst   = which((Qsim_TS$forecast=="TRUE") & (Qsim_TS$parameterNameCode=="Q"))
  Qsim_TS   = Qsim_TS[m_frcst,]
  m_GR4   = grep(pattern=fcstTScode_pattern, x=Qsim_TS$code)
  Qsim_TS = Qsim_TS[m_GR4,] # Time series of only required
  logInfo(paste0("A total number of ",length(Qsim_TS), " forecasted flow time series codes have been retrieved from WIMES"))
  
  # 4 - READ Observed flow from WIMES ----
  logInfo("'runCumulatedRoutedFlows' -  4 - READ Observed flow from WIMES")
  # .. 4.1 - Get the list of observed flow timeseries corresponding to the given pattern ----
  Qobs_TS = getAllTimeSeries(sitesCodes=AWL_Sites$code) # get all timeseries for which the site code correspond to one of the codes of the automatic water level station 
  m_obs   = which((Qobs_TS$forecast=="FALSE") & (Qobs_TS$parameterNameCode=="Q")) # select only flow timeseries which are NOT forecast TS
  Qobs_TS   = Qobs_TS[m_obs,]
  for (i in 1:length(obsTScode_pattern)){
    if (i==1){
      m_obs   = grep(pattern=obsTScode_pattern[i], x=Qobs_TS$code)
      if (outVerbose){
        logInfo(paste0("A total number of ",length(m_obs)," time series were found corresponding to TS code pattern: ",obsTScode_pattern[i]))
      }
      Qobs_TSfinal = Qobs_TS[m_obs,] # Time series of only required
    } else {
      m_obs   = grep(pattern=obsTScode_pattern[i], x=Qobs_TS$code)
      Qobs_TStemp = Qobs_TS[m_obs,] # Time series of only required
      if (outVerbose){
        logInfo(paste0("A total number of ",length(m_obs)," time series were found corresponding to TS code pattern: ",obsTScode_pattern[i]))
      }
      Qobs_TSfinal = rbind(Qobs_TSfinal,Qobs_TStemp)
    }
  }
  logInfo(paste0("A total number of ",length(Qobs_TSfinal$code), " observed flow time series codes have been retrieved from WIMES"))
  
  # .. 4.2 - Get data of observed flow timeseries ----
  date_start = min(qq_dates)
  if (is.null(currentDateTime)) date_end = max(qq_dates) #by default use the maximum date of input data (observation will anyway stop at actual currentDateTime!)
  else date_end = currentDateTime #if a currentDateTime is actually provided (for hindcast purposes typically), use it 
  Qobs_data = getObsDataByTScodepatternS(date_start = date_start, 
                                         date_end = date_end, 
                                         Sites = AWL_Sites, 
                                         TScode_pattern = obsTScode_pattern, 
                                         tstep)
  Qobs_data = cleaningRawData(Qobs_data, Qmin=0, Qmax=1500) # rough data cleaning, we reject very bad flow data
  
  # 5 - SUM sub-basin contribution to compute flows -----------------
  logInfo("'runCumulatedRoutedFlows' -  5 - SUM sub-basin contribution to compute flows")
  
  # FLOW ROUTING
  # READ the CSV file containing flow routing parameters
  routing_param=getRoutingParam()
  
  # .. 5.1 - Flow routing order ----
  logInfo("'runCumulatedRoutedFlows' -  5.1 - Flow routing order")
  # WARNING, we MUST COMPUTE FLOW ROUTING IN upstream-downstream manner
  # So first we define the order for which flows will be computed
  Order_df = data.frame(Code=Code_routedlinks, order=c(1:length(Code_routedlinks)))
  #call the orderingNodes local function
  Order_df <- orderingNodes(Order_df, Node_routedlinks_mat, Node_routedlinks, Code_routedlinks)
  
  # INITIALIZATION of the data.frame of simulated ROUTED flows
  qq_rout   <- matrix(0, nrow=length(qq_dates), ncol=length(Order_df$Code))
  colnames(qq_rout) <- Order_df$Code
  # INITIALIZATION of the data.frame of auxiliary flows
  qq_aux   <- matrix(0, nrow=length(qq_dates), ncol=length(Order_df$Code))
  colnames(qq_aux) <- Order_df$Code
  
  # .. 5.2 - COMPUTATION OF CUMULATED ROUTED FLOWS ----
  logInfo("'runCumulatedRoutedFlows' -  5.2 - COMPUTATION OF CUMULATED ROUTED FLOWS")
  #call the local function 'cumulatedRoutedFlows'
  N=length(Order_df$Code)
  correcMatrix<-getCorrecMatrix()
  for (i in 1:N){
    code_i=as.character(Order_df$Code[i])
    m_code=which(Code_routedlinks==code_i)
    type_node=substr(code_i,1,1)
    
    if (type_node == "1"){
      logInfo(paste0(code_i," is a station node"))
      m_qq = grep(x= colnames(qq_sim), pattern=code_i) 
      qq_rout[,i]=qq_sim[,m_qq]  #first : add the simulated flows from the subbasin itself
      qq_aux[,i]=qq_sim[,m_qq]  #first : add the simulated flows from the subbasin itself
      
      m_depend = which(Node_routedlinks_mat[,m_code] == 1)
      if (length(which(m_depend==m_code))!=0){
        m_depend = m_depend[-which(m_depend==m_code)] # delete the index corresponding to the node 'code_i' itself
      }
      if (length(m_depend) !=0){
        m_depend_df = match(Node_routedlinks$Nat_ID[m_depend] , colnames(qq_rout)) # corresponding indexes in qq_rout (same as Order_df$Code)
        for (j in m_depend_df) {
          qq_rout[,i]= qq_rout[,i] + qq_rout[,j]  # add the data from corresponding dependent nodes (upstream nodes. It can be a reach node)
          qq_aux[,i]= qq_rout[,i]  # copy qq_rout
        }
      } # END IF. Else: do nothing. we have already added the subbasin itself some lines before
      # .. .. 5.2.2 - combine the simulated and routed (qq_rout[,i]) with the observed flows using Romain's function ----
      m_obsTS_i = grep(x=colnames(Qobs_data), pattern=code_i)
      if (length(m_obsTS_i)!=1){
        logWarning(paste0("No observed TS could be found on node: ", code_i))
        logInfo("The routed flows could not be corrected using the observed flows")
      } else{
        Qobs_data_selected = data.frame(observationDateTime = Qobs_data$date, value = Qobs_data[, m_obsTS_i]) # build a data frame
        Q_rout_df = data.frame(forecastDateTime = qq_dates, value = qq_rout[,i])
        qq_rout_cor = correcFcstWithObs(obsValues = Qobs_data_selected, fcstValues = Q_rout_df,correcCoeff = correcMatrix[[code_i]])
        #logInfo(paste("the corrected flow has ",length(qq_rout_cor$value)," values, out of which ",length(which(is.na(qq_rout_cor$value)))," are NA",sep=""))
        if (correctForecast) qq_rout[,i] = qq_rout_cor$value #if correctForecast then propagate corrected flows (and keep raw forecast in auxiliary flows)
        else qq_aux[,i] = qq_rout_cor$value #otherwise save corrected flows in auxiliary flows (and keep raw forecast for downstream propagation)
      }
      # TODO 2 : combine the simulated and routed (qq_rout[,i]) with the observed flows using Romain's function
      
      
      
    } else if (type_node == "R") {
      logInfo(paste0(code_i," is a routed reach node"))
      #what is the upstream node of the reach
      temp=unlist(strsplit(code_i,"_"))
      code_upstream = temp[2]
      logInfo(paste0("The upstream node is ",code_upstream))
      indx_ups=which(colnames(qq_rout)==code_upstream)
      
      cd_rout=which(routing_param$Code==code_i)
      
      Lx=routing_param$Length_m[cd_rout] #length of the river reach (in metres)
      I0=routing_param$Slope_I[cd_rout] #slope of the river reach (in m/m)
      K_RB=routing_param$K_RightBank[cd_rout]
      K_LB=routing_param$K_LeftBank[cd_rout]
      K_MC=routing_param$K_MainChannel[cd_rout]
      K=c(K_LB,K_LB,K_MC,K_MC,K_MC,K_RB,K_RB)#strickler des 7 panels elementaires
      x=unlist(routing_param[cd_rout,2:9])#vecteur des 8 abscisses des points du profil en travers type
      z=unlist(routing_param[cd_rout,10:17])#vecteur des 8 cotes (tout est relatif...)
      
      #lecture du fichier de debits journaliers sur Nzoia
      Qe=qq_rout[,indx_ups]
      m_na=which(is.na(Qe))
      if (length(m_na)!=0){
        logInfo(paste("there were ",length(m_na)," NA values out of ",length(Qe)," values",sep=""))
        Qs0=Qe[m_na[length(m_na)]+1] # the function Muskingum does not like when there are zero flows
        Qe[m_na]=Qs0
      } else {
        Qs0=Qe[1]
      }
      #passage au pas de temps horaire pour essayer de voir le dephasage...
      #Qe=approx(seq(0,length(Qe)-1)*24,Qe,seq(0,length(Qe)*24-24))$y
      #Qe=c(Qe,rep(Qe[length(Qe)],length(Qe))) # this line is to build a Qe vector twice as long as Qe (for routing), but not necessary
      if (as.numeric(tstep,units="hours") == 24){
        qq_dates_24h <- seq(from = min(qq_dates), to = max(qq_dates), by = "hour")
        attributes(qq_dates_24h)$tzone = "UTC" # make sure dates are in UTC zone
        Qe <- approx(x=qq_dates,y=Qe, xout=qq_dates_24h)$y
        m_na=which(is.na(Qe))
        if (length(m_na)!=0){
          Qe[m_na]=Qs0 # the function Muskingum does not like when there are zero flows
        }
        dt0=as.integer(difftime(qq_dates_24h[2],qq_dates_24h[1], units="secs")) #apres re-echantillonnage de fait... on peut s'amuser a "accelerer" la crue pour voir l'effet sur la deformation de l'hydrogramme (prendre par exemple dt0=360)
        dtc0=dt0
        logInfo(paste("the minimum flow to route is ",min(Qe)," and the maximum flow to route is ",max(Qe),sep=""))
        Qs=muskingum8(Qe,x,z,K,I0,Lx,dt0,dtc0,Qs0)
        
        QQs=tapply(Qs,format(qq_dates_24h,format="%Y-%m-%d"),mean) #we get back daily mean values
        qq_rout[,i]=QQs[1:length(qq_rout[,indx_ups])] #add the data to the global qq_rout matrix. when using the approx functions and tapply, there can be an additional date at the end, that is why we use the '1:length(qq_sim[,indx_ups]' 
        qq_aux[,i]=qq_rout[,i]
        
      } else {
        dt0=as.integer(difftime(qq_dates[2],qq_dates[1], units="secs")) #apres re-echantillonnage de fait... on peut s'amuser a "accelerer" la crue pour voir l'effet sur la deformation de l'hydrogramme (prendre par exemple dt0=360)
        dtc0=dt0
        logInfo(paste("the minimum flow to route is ",min(Qe)," and the maximum flow to route is ",max(Qe),sep=""))
        Qs=muskingum8(Qe,x,z,K,I0,Lx,dt0,dtc0,Qs0)
        #logInfo(paste("the routed flow has ",length(Qs)," values, out of which ",length(which(is.na(Qs)))," are NA",sep=""))
        qq_rout[,i]=Qs[1:length(qq_rout[,indx_ups])] #add the data to the global qq_rout matrix. when using the approx functions and tapply, there can be an additional date at the end, that is why we use the '1:length(qq_sim[,indx_ups]' 
        qq_aux[,i]=qq_rout[,i] #copy qq_rout
      }
      
    } else if (type_node == "J") {
      logInfo(paste0(code_i," is a junction node"))
      m_depend = which(Node_routedlinks_mat[,m_code] == 1)
      if (length(m_depend) !=0){
        m_depend_df = match(Node_routedlinks$Nat_ID[m_depend] , colnames(qq_rout)) # corresponding indexes in qq_rout (same as Order_df$Code)
        for (j in m_depend_df) {
          qq_rout[,i]= qq_rout[,i] + qq_rout[,j]  # add the data from corresponding dependent nodes (upstream nodes. It can be a reach node)
          qq_aux[,i]= qq_rout[,i]
        }
      } # END IF. 
      
    } else {
      logWarning(paste0(code_i," is not a recognised type of node"))
    }
    
  }
  
  # 6 - WRITE simulated flows into WIMES -----------------------------------
  if (is.null(currentDateTime)) { #write data only if we are actually running a forecast (don't write anything in case you do hindcast or reforecast)
  logInfo("'runCumulatedRoutedFlows' -  6 - WRITE simulated flows into WIMES")
  # .. 6.1 - choose the indexes/dates that we want to write onto WIMES ----
  now_date=Sys.time() # get the system time of 'right now"
  attributes(now_date)$tzone<- "UTC" # convert the time_zone to UTC
  lookback_forecast = 5 # lookback duration period in days. Usually 5 days. The prupose is to give some hindcast data to the forecast expert at KMD
  export_date_start = now_date - as.difftime(lookback_forecast, units="days")
  export_date_start = as.POSIXct(export_date_start)
  
  # .. 6.2 - Send data to WIMES ----
  n_upsert=0
  internProgress=0 # internal progress variable
  updateProgression(0, ".. start sending data to WIMES..")
  
  qq_rout_names=colnames(qq_rout)
  for (cd in seq(along = qq_rout_names)){
    m_cd=grep(pattern=qq_rout_names[cd], x=Qsim_TS$code)
    if (length(m_cd)==0){
      logInfo(paste0("No forecast time series was found for routed simulated flow at node ",qq_rout_names[cd]))
    } else {
      m_export = which(qq_dates>=export_date_start & !is.na(qq_rout[,cd])) # search indexes of dates more recent than the export_date_start
      cumFlowExport2WIMES_df = data.frame(timeSeriesCode    = Qsim_TS$code[m_cd],
                                          productionDateTime= now_date, 
                                          forecastDateTime  = qq_dates[m_export] ,
                                          value             = qq_rout[m_export,cd])
      
      #Use the specific RWIMES function to upload several values at once
      upsertQuantForecastValues(cumFlowExport2WIMES_df)
      
      n_upsert = n_upsert + length(m_export) #increment n_upsert
      
      internProgress = (cd /length(qq_rout_names))*100
      if (internProgress<100){
        updateProgression(floor(internProgress), ".. sending data to WIMES..")
      } else {
        updateProgression(floor(internProgress), ".. END sending data to WIMES.")
      }
    }
  }
  logInfo(paste0("A total number of ",as.character(n_upsert), " values have just been sent to WIMES for routed flows."))
  }
  output_df_rout=data.frame(qq_dates, qq_rout) # create a dataframe that contains both dates and routed flows
  output_df_aux=data.frame(qq_dates, qq_aux) # create a dataframe that contains both dates and auxiliary flows

  
  # 7 - END of the function ----
  logInfo("END of the function 'runCumulatedRoutedFlows', a data frame is returned.")
  logInfo("====================")
  logInfo(" ")
  
  return(list(routed_flows = output_df_rout, auxiliary_flows = output_df_aux))
  
}



###########################################################################

# LOCAL FUNCTIONS ---------------------------------------------------------
#Below are the local functions that can be called from main functions above.

upsertQuantValuesBig=function(valuesDF){
  maxPostVal = 500
  sizeValuesDF = length(valuesDF$value)
  nPost = ceiling(sizeValuesDF / maxPostVal)
  
  if(nPost>1){
    logInfo(paste0("Send a large amount of obs data to WIMES: A total number of ",nPost," Post request(s) will be sent to WIMES"))
  }
  for (i in 1:nPost){
    logInfo(paste0("... sending data to WIMES, pack ",i,"/",nPost))
    indexStart = 1+(i-1)*maxPostVal
    indexEnd   = (i)*maxPostVal
    indexEnd = min(indexEnd, sizeValuesDF)
    upsertQuantValues(valuesDF[indexStart:indexEnd,])
  }
}

# LOCAL FUNCTION: Check whether the interpolation period of each TS match with the timestep (argument of the function)
checkInterpolationPeriod = function (code, timestep) {
  timeSeries <- getOneTimeSeries(code) #get the details of every timeseries
  if (timestep=="1h"){
    if (timeSeries$interpolationPeriodType != "HOUR"){ logWarning(paste0("Timeseries ",timeSeries$code," is not of the good interpolation period type ",timestep, " is required"))}
  } else if (timestep=="5m"){
    if (timeSeries$interpolationPeriodType != "MINUTE"){ logWarning(paste0("Timeseries ",timeSeries$code," is not of the good interpolation period type"))}
  }
}

# LOCAL FUNCTION
textToDifftime = function (timestepString){
  if (!is.character(timestepString)){
   logWarning(paste0("Cannot convert ",as.character(timestepString), " because, it is not a character object. Input is returned.")) 
    return(timestepString)
  }
  
  nCharacters = nchar(timestepString)
  
  if (nCharacters>3){
    logWarning(paste0("Cannot convert into difftime, a character of max length 3 expected : ", timestepString))
  }
  
  timeUnit = substr(x=timestepString, nCharacters, nCharacters)
  timeValue = substr(x=timestepString, 1, nCharacters-1)
  timeUnitTab = c("h"="H", "m"="M", "d"="d")
  tstep = as.difftime((timeValue), paste0("%",timeUnitTab[timeUnit]))
  tstep = abs(tstep) # necessary to add the absolute value because when dealing with timeUnit "d", the difftime is negative
  return(tstep)
}

# LOCAL FUNCTION: Check whether the geometry is the expected one
checkGeom = function (WimesSite, geometry) {
  if (WimesSite$geometry$type!=geometry){
    logWarning(paste0("The site: ",WimesSite$code," is NOT a ",geometry,", while required")) # if the geometry type is NOT a MultiPolygon, then we send a warning
    return(FALSE)
  } else {
    return(TRUE)
  }
}



# LOCAL FUNCTION
#' Get the list of all basin sites (of polygon type) with a specific pattern within the site code
#'
#' @param label_pattern string - defines the basin sites code pattern (e.g. basin codes are like: 1EF01_BAS, so the pattern is "*_BAS" or "....._BAS")
#'
#' @return data frame - it has the same structure as the data frame returned by the RWIMES function 'getSites'
#' @export
#' 
getBasinSitesList=function(label_pattern){
  # 1 - Get the list of Sites
  logInfo("getBasinSitesList- 1 - Get the list of Sites")
  Sites <- getSites(  ) # get all the sites within the WIMES application
  
  # 2 - Get the list of basins Polygon Sites
  logInfo("getBasinSitesList- 2 - Get the list of basins Polygon Sites")
  m_bas_sites=grep(pattern=label_pattern, x=Sites$code)
  Basin_Sites = Sites[m_bas_sites,] # Basin_Sites is only the Polygon sites of which the code finishes with the pattern "_BAS"
  
  # CHECK that all selected sites (Basin_Sites) are of Polygon type
  m_nopoly=rep(NA,length(Basin_Sites$code)) #initialization of a vector of same length as Basin_Sites
  for (i in 1:length(Basin_Sites$code)) {
    site <- getSite(Basin_Sites$code[i])
    if (!checkGeom(site, "MultiPolygon")){
      m_nopoly[i]=i
    } 
  }
  if (length(which(is.na(m_nopoly)))<length(Basin_Sites$code)) {Basin_Sites= Basin_Sites[-m_nopoly,]}
  return(Basin_Sites)
}

# LOCAL FUNCTION
#' Title: getBasinsGeometry
#'
#' @param Basin_Sites data frame - output of the function 'getBasinSitesList' (same structure as the data frame returned by the RWIMES function 'getSites')
#' @param working_crs string - defines the geographic coordinates system to be used for the output
#'
#' @return a SpatialPolygon object with all the basin polygons in it
#' 
#' @export
#'
getBasinsGeometry=function(Basin_Sites, working_crs){
  logInfo("The function 'getBasinsGeometry' has just been called")
  wgs84_crs="epsg:4326" #This is the geographic projection WGS84 with longitude and latitude details in decimal degrees. Sites within WIMES are stored in WGS84
  # 1 - Read basin shp from WIMES
  logInfo("getBasinsGeometry - 1 - Read basins shp from WIMES")
  bvs <- list() # polygon coordinates as a list
  for (i in 1:length(Basin_Sites$code))
  {
    bv <- getGeometry(Basin_Sites$code[i])
    bvs[[i]]=bv[1]@polygons[[1]] # we extract only the first polygon (usually, there should be only one polygon within the SpatialPolygonsDataFrame)
    bvs[[i]]@ID = Basin_Sites$code[i]
  }
  bvs=SpatialPolygons(Srl = bvs, proj4string = CRS(paste0("+init=",wgs84_crs))) # create an object of class SpatialPolygons from a list of objects of class Polygons
  logInfo("getBasinsGeometry - 2 - Re-Project polygons onto the working CRS projection")
  bvs=spTransform(bvs, CRS(paste0("+init=",working_crs))) #use the working CRS as a new projection
  logInfo("END of the function 'getBasinsGeometry'")
  return(bvs)
}

# LOCAL FUNCTION
#' Get the polygon geometry of a site
#'
#' @param site string - site code in the WIMES application
#'
#' @return SpatialPolygon object: typically the watershed polygon
#'
#' @details This function is used to get the geomtry of polygon site from the WIMES application
getGeometry = function (site){
  Rgeom = getSite(site)$geometry$coordinates
  level=1
  if (is.null(dim(Rgeom))) {
    coord_mat=matrix(NA, ncol=2,nrow=0)
    l1=length(Rgeom)
    for (j in 1: l1) {
      l2=length(Rgeom[[j]])
      for (k in 1: l2) {
        l3=dim(Rgeom[[j]][[k]])[1]
        if (l3>5) {
          coord_mat=rbind(coord_mat,Rgeom[[j]][[k]][1:l3,])
        }
      }
    }
    coord=lapply(X=seq_len(nrow(coord_mat)), FUN = function(x) coord_mat[x,])
  } else {
    nb = length(Rgeom[,,,1])
    coord = lapply(1:nb, FUN = function(x) Rgeom[,,x,])
  }
  # creates json object
  init = TO_GeoJson$new()
  json = init$MultiPolygon(list(list(coord)), stringify = TRUE)
  
  # reads json file as OGR geometry
  bv <- readOGR(json$json_dump, "OGRGeoJSON", verbose = F)
  return(bv)
}


# LOCAL FUNCTION

# compute the coverage percentage % of each basin for every pixel

#' Get the basin coverage with the weight of each of the domaine cell
#'
#' @param bvs SpatialPolygon object - created by the local function 'getBasinsGeometry'
#' @param domaine extent object
#'
#' @return list of matrices, one matrix per each polygon within the object "bvs"
#'
getBasinCoverage=function(bvs, domaine, outVerbose=FALSE) {
  logInfo("---- The function 'getBasinCoverage' has just been called ----")
  
  # option 1 ----
  # matrix_bv=array(NA,dim=c(dim(domaine)[1:2],length(bvs))) #initialization of the matrix
  # 
  # for (cd in 1:length(bvs))
  # {
  #   bv<-rasterize(bvs[cd,],domaine,getCover=T)
  #   matrix_bv[,,cd]=raster::as.matrix(bv)
  # }
  
  # option2 ----
  # matrix_bv = extract(x=domaine, y=bvs, small=TRUE, weights=TRUE, normalizeWeights=FALSE)
  # logInfo("END of the function 'getBasinCoverage', a list of matrices is returned.")
  # return(matrix_bv)
  
  #option 3b ----
  # ext_domaine = extent(domaine)
  # CRS_domaine = crs(domaine)
  # RES_domaine = res(domaine)
  # old_domaine = domaine
  # CRS_bvs = crs(bvs)
  # bvs = spTransform(bvs, CRS_domaine)
  # matrix_bv = extract(x=domaine, y=bvs, small=TRUE, weights=TRUE, normalizeWeights=FALSE)
  # resolutionFactor=0
  # resolutionFactorNew=1
  # while (resolutionFactor<resolutionFactorNew & resolutionFactor<=2){
  #   resolutionFactor = resolutionFactorNew
  #   res_NEW = RES_domaine/(resolutionFactor)
  #   res_NEW = min(res_NEW)
  #   res_NEW = round(res_NEW, digits = 8)
  #   res_NEW = c(res_NEW, res_NEW)
  #   if (outVerbose){
  #     logInfo(paste0("...resolutionFactor = ", resolutionFactor))
  #     logInfo(paste0(".....new resolution = ", res_NEW))
  #   }
  #   
  #   domaine = projectRaster(old_domaine, crs = CRS_domaine, res = res_NEW)
  #   matrix_bv = extract(x=domaine, y=bvs, small=TRUE, weights=TRUE, normalizeWeights=FALSE)
  #   for (cd in 1:length(bvs)){
  #     # we want at list one cell entirely included within each basin polygon, so at least one weight equal to 1
  #     if (max(matrix_bv[[cd]][,2])<1){
  #       if(resolutionFactor==resolutionFactorNew){
  #         resolutionFactorNew = resolutionFactorNew *2
  #       }
  #     }
  #   }
  # }
  # 
  # domaine = crop(domaine, old_domaine)
  # Option 4: same as option 1 but return a raster ≡stack instead of a matrix ----
  
  CRS_domaine = crs(domaine)
  CRS_bvs = crs(bvs)
  bvs_newCRS = spTransform(bvs, CRS_domaine)
  stack_bv = stack()
  for (cd in 1:length(bvs_newCRS))
  {
    bv_raster <- rasterize(bvs_newCRS[cd],domaine,getCover=T)
    stack_bv = stack(stack_bv, bv_raster)
  }
  #crop(x=stack_bv, y =ext_domaine) # crop to make sure, that the output has the same size as the input
  # stack_bv = projectRaster(stack_bv, crs = CRS_domaine, res = RES_domaine)
  names(stack_bv)=names(bvs)
  logInfo("---- END of the function 'getBasinCoverage', a raster stack is returned. ----")
  return(stack_bv)
}

# Local function
#' Get Observed Data By several time series code pattern from WIMES.
#'
#' @param date_start POSIXct object - it is the start date of the time window of observation
#' @param date_end POSIXct object - it is the end date of the time window of observation
#' @param Sites data frame - it has the same structure as the data frame returned by the RWIMES function 'getSites', or the local function 'GetBasinSitesList'
#' @param TScode_pattern string vector - it defines the pattern of timeseries codes taht we want to select (e.g. observed rainfall timeseries codes are like 'KMD_8935170_TUR_P_obs_1h', so the pattern could be "P_obs_1h")
#' @param tstep difftime class object - it defines the duration of the timestep
#'
#' @return data frame - first column is the timestamp (POSIXct format), the 1st column name is 'date'. 
#' Then there are as many columns as the number of timesreis matching the 'TScode_pattern'. 
#' If no time series are found, a NULL is returned.
#' @export
#'
#' @examples
getObsDataByTScodepatternS = function(date_start, date_end, Sites, TScode_pattern, tstep){
  logInfo(paste0("The function 'getObsDataByTScodepatternS' has just been called with ",
                 length(TScode_pattern),
                 " TS code patterns"))
  for (j in 1:length(TScode_pattern)){
    if (j==1){
      outValues_df = getObsDataByTScodepattern(date_start, date_end, Sites, TScode_pattern[j], tstep)
      nbTS = dim(outValues_df)[2]-1
      logInfo(paste0("A total number of ", nbTS, " timeseries have just been retrieved based on ",TScode_pattern[j]))
    } else if (j>1){
      outValues_df_temp = getObsDataByTScodepattern(date_start, date_end, Sites, TScode_pattern[j], tstep)
      nbTS = dim(outValues_df_temp)[2]-1
      addedColnames = colnames(outValues_df_temp)[2:(nbTS+1)]
      logInfo(addedColnames)
      logInfo(paste0("A total number of ", nbTS, " timeseries have just been retrieved based on ", TScode_pattern[j]))
      outValues_df_temp = outValues_df_temp[,-1]
      outValues_df = cbind(outValues_df, outValues_df_temp)
      lengTot = dim(outValues_df)[2]
      colnames(outValues_df)[(lengTot-nbTS+1):lengTot] = addedColnames
    }
    logInfo("The function 'getObsDataByTScodepatternS' has just ended")
  }
  return(outValues_df)
}


# LOCAL FUNCTION
#' Get Observed Data By time series code pattern from WIMES.
#'
#' @param date_start POSIXct object - it is the start date of the time window of observation
#' @param date_end POSIXct object - it is the end date of the time window of observation
#' @param Sites data frame - it has the same structure as the data frame returned by the RWIMES function 'getSites', or the local function 'GetBasinSitesList'
#' @param TScode_pattern string - it defines the pattern of timeseries codes taht we want to select (e.g. observed rainfall timeseries codes are like 'KMD_8935170_TUR_P_obs_1h', so the pattern could be "P_obs_1h")
#' @param tstep difftime class object - it defines the duration of the timestep
#'
#' @return data frame - first column is the timestamp (POSIXct format), the 1st column name is 'date'. 
#' Then there are as many columns as the number of timesreis matching the 'TScode_pattern'. 
#' If no time series are found, a NULL is returned.
#' 
#' @export
#'
#' @examples
#' date_start <- as.POSIXct("2019-03-01", format="%Y-%m-%d", tz="UTC")
#' date_end   <- as.POSIXct("2019-05-25", format="%Y-%m-%d", tz="UTC")
#' tstep      <-  as.difftime(1, units="hours")
#' label_pattern <- "....._BAS"
#' Sites <- GetBasinSitesList(label_pattern)
#' TScode_pattern <- "obs_1h"
#' getObsDataByTScodepattern(url, key, date_start, date_end, Sites, TScode_pattern, tstep)
getObsDataByTScodepattern = function(date_start, date_end, Sites, TScode_pattern, tstep){
  #BIG TODO: if the TS is of type CorrectableQuant, then we should check the quality status of the data, and reject dubious or bad data
  logInfo("The function 'getObsDataByTScodepattern' has just been called")
  obs_TS     <- getAllTimeSeries(sitesCodes=Sites$code)
  m_obs       <- grep(pattern=TScode_pattern, obs_TS$code)
  if (length(m_obs)==0){
    logWarning(paste0(".. No observed time series were found for the label pattern: '", TScode_pattern,"' a NULL is returned."))
    return(NULL)
  }
  obs_TS     <- obs_TS[m_obs,]
  
  # Check whether the interpolation period of each TS match with the tstep (argument of the function)
  tstep_minutes=as.numeric(tstep, units="mins") #convert the time step (input argument of the function), which is a difftime class object, into a numeric in minutes
  tstep_hours=as.numeric(tstep, units="hours") #convert the time step (input argument of the function), which is a difftime class object, into a numeric in hours
  if (tstep_minutes<60){
    timestep=paste0(tstep_minutes, "m") # create the string argument for the Check_InterpolationPeriod function
  } else if (tstep_hours<24){
    timestep=paste0(tstep_hours, "h") # create the string argument for the Check_InterpolationPeriod function
  } else {
    timestep="1d" # we assume there are no longer time step than daily time step
  }
  
  for (i in 1:length(obs_TS$code)) {
    checkInterpolationPeriod(obs_TS$code[i], timestep)
  }
  
  outDates = seq(from=date_start, to = date_end, by=tstep)
  attributes(outDates)$tzone = "UTC" #make sure we are in "UTC" zone
  outDates = rev(outDates) # we have to reverse dates because the RWIMES API provide data from the most recent to the less recent observation
  outValues=matrix(NA, nrow=length(outDates), ncol=length(obs_TS$code))
  colnames(outValues)=obs_TS$code # Assign timeseries codes to each column's name. It is easier for debugging the code afterwards
  nbTstep = length(outDates) # number of time step within the vector of dates 'outDates'
  nb5000tstep = ceiling(nbTstep/2000) # because the max number of data sent by WIMES is: 5000 # 2021/03/01: limit set up to 2000 to have wimes.meteo.go.ke working
  logInfo(paste0(".. For each observed time serie, there is a need to send ", nb5000tstep, " request(s) to WIMES to accomodate with the max limit of 5000 values"))
  
  # read water level data from WIMES: loop on AWL code
  logInfo(".. start a loop on TS codes ..")
  for (i in 1:length(obs_TS$code)) {
    logInfo(paste0(".. obs_TS$code[i] = ", obs_TS$code[i]))
    for (j in 1:nb5000tstep){
      min_dates_index = 1 + 2000*(j-1)
      max_dates_index = 2000 * j
      max_dates_index = min(max_dates_index, length(outDates))
      sub_date_start = min(outDates[min_dates_index:max_dates_index])
      sub_date_end = max(outDates[min_dates_index:max_dates_index])
      logInfo(paste0(".. sub_date_start = ", sub_date_start))
      logInfo(paste0(".. sub_date_end = ", sub_date_end))
      obsData = getObservedValues(obs_TS$code[i], beginDateTime=sub_date_start , endDateTime=sub_date_end)
      if (is.null(dim(obsData)) | is.null(obsData$value)){
        logInfo(paste0("No data could be retrieved from observation TS '",obs_TS$code[i], "' during the specified time period" ))
      } else {
        obsDates   = obsData$observationDateTime
        m_data          = match(as.character(outDates[min_dates_index:max_dates_index], format="%Y%m%dT%H%M%S"),
                                as.character(obsDates, format="%Y%m%dT%H%M%S"))
        outValues[min_dates_index:max_dates_index,i] = obsData$value[m_data]
      }
    }
  }
  outValues_df = data.frame(date=outDates, outValues)
  logInfo("The function 'getObsDataByTScodepattern' has just ended")
  return(outValues_df)
}


# LOCAL FUNCTION
#' Get forecast data by time serie code pattern
#'
#' @param date_prod POSIXct object - it is the production date of the forecast
#' @param Sites data frame - it has the same structure as the data frame returned by the RWIMES function 'getSites', or the local function 'GetBasinSitesList'
#' @param TScode_pattern string - it defines the pattern of timeseries codes taht we want to select (e.g. observed rainfall timeseries codes are like 'KMD_8935170_TUR_P_obs_1h', so the pattern could be "P_obs_1h")
#' @param tstep difftime class object - it defines the duration of the timestep
#' @param date_start POSIXct object - it is the start date of the time window of forecast
#' @param date_end POSIXct object - it is the end date of the time window of forecast
#'
#' @return data frame - first column is the timestamp (POSIXct format), the 1st column name is 'date'. Then there are as many columns as the number of timesreis matching the 'TScode_pattern'
#'
#' @export
#'
#' @examples
#' date_prod <- as.POSIXct("2019-03-05", format="%Y-%m-%d", tz="UTC")
#' date_start <- as.POSIXct("2019-03-01", format="%Y-%m-%d", tz="UTC")
#' date_end   <- as.POSIXct("2019-05-20", format="%Y-%m-%d", tz="UTC")
#' tstep      <-  as.difftime(1, units="hours")
#' label_pattern <- "....._BAS"
#' Sites <- GetBasinSitesList(label_pattern)
#' TScode_pattern <- "GR4_....._BAS_Q_sim_1h"
#' getFcstDataByTScodepattern(url, key, date_start, date_end, Sites, TScode_pattern, tstep)
getFcstDataByTScodepattern = function(date_prod, date_start, date_end, Sites, TScode_pattern, tstep){
  logInfo("The function 'getFcstDataByTScodepattern' has just been called")
  fcst_TS     <- getAllTimeSeries(sitesCodes=Sites$code)
  m_fcst       <- grep(pattern=TScode_pattern, fcst_TS$code)
  fcst_TS     <- fcst_TS[m_fcst,]
  
  # Check whether the interpolation period of each TS match with the tstep (argument of the function)
  tstep_minutes=as.numeric(tstep, units="mins") #convert the time step (input argument of the function), which is a difftime class object, into a numeric in minutes
  tstep_hours=as.numeric(tstep, units="hours") #convert the time step (input argument of the function), which is a difftime class object, into a numeric in hours
  if (tstep_minutes<60){
    timestep=paste0(tstep_minutes, "m") # create the string argument for the Check_InterpolationPeriod function
  } else if (tstep_hours<24){
    timestep=paste0(tstep_hours, "h") # create the string argument for the Check_InterpolationPeriod function
  } else {
    timestep="1d" # we assume there are no longer time step than daily time step
  }
  
  for (i in 1:length(fcst_TS$code)) {
    checkInterpolationPeriod(fcst_TS$code[i], timestep)
  }
  
  outDates = seq(from=date_start, to = date_end, by=tstep)
  attributes(outDates)$tzone = "UTC" #make sure we are in "UTC" zone
  outDates = rev(outDates) # we have to reverse dates because the RWIMES API provide data from the most recent to the less recent forecast
  outValues=matrix(NA, nrow=length(outDates), ncol=length(fcst_TS$code))
  colnames(outValues)=fcst_TS$code # Assign timeseries codes to each column's name. It is easier for debugging the code afterwards
  
  # read water level data from WIMES: loop on AWL code
  if (date_prod==-Inf)
	{
	logWarning("The production date doesn't exist, forecast data will be replaced by 0")
	outValues[,] <-0
	}
  else
	{
  for (i in 1:length(fcst_TS$code)) {
    fcstData = getForecastValuesByProductionDate(fcst_TS$code[i], productionDateTime = date_prod)
    if (is.null(dim(fcstData)) | is.null(fcstData$value)){
      logInfo(paste0("No data could be retrieved from forecast TS '",fcst_TS$code[i], "' during the specified time period" ))
    } else {
      fcstDates   = fcstData$forecastDateTime
      m_data          = match(as.character(outDates, format="%Y%m%dT%H%M%S"), 
                              as.character(fcstDates, format="%Y%m%dT%H%M%S"))
      outValues[,i] = fcstData$value[m_data]
    }
  }
  }
  outValues_df = data.frame(date=outDates, outValues)
  logInfo("The function 'getFcstDataByTScodepattern' has just ended")
  return(outValues_df)
}


# LOCAL FUNCTION
#' Convert the time step of a data.frame, such as precipitation or even for flow data
#'
#' @param df dataframe object - it defines the input data. The first column should be the dates in POSIXct format
#' @param newTimeStep difftime object - it defines the new timestep
#' @param cumul logical - if true, the input time serie is considered a cumulated time series such as precipitation. Otherwise it is a flow or an intensity
#' @param outVerbose 
#'
#' @return a dataframe object - its 1st column correponds to  the dates with the new timestep.
#' 
#' @details Make use of the 'lubridate' package.
#' @export
#'
convertDFtimestep=function(df, newTimeStep, cumul=TRUE, outVerbose=FALSE){
  if (outVerbose){
    logInfo("Start running local function: convertDFtimestep...")
  }
  if (!inherits(df[1,1], what="POSIXct")){
    logWarning("In function converDFtimestep, the first column should be a POSIXct object")
    logInfo("So, the function 'convertDFtimestep' returns a NULL value.")
    return(NULL)
  }
  if(!is.logical(cumul)){
    logWarning("Argument 'cumul' is required to be a logical.")
    logInfo("So, the function 'convertDFtimestep' returns a NULL value.")
    return(NULL)
  }
  # TODO: check that the data.frame is ordered (chronologically or antichrono). Re-order if necessary
  # TODO: check whether the timestep is regular
  
  # estimation of the old time step
  oldTimeStep = min(abs(df[2:length(df),1] - df[1:(length(df)-1),1])) # estimate the old timestep
  if (outVerbose){
    logInfo(paste0("oldTimeStep=", as.numeric(oldTimeStep,units="mins")," minutes"))
    logInfo(paste0("newTimeStep=", as.numeric(newTimeStep,units="mins")," minutes"))
  }
  
  date_start = min(df[,1]) # define the oldest date time
  date_end = max(df[,1]) # define the most recent date time
  
  if (oldTimeStep == newTimeStep) {
    newDF = df
  } else if (oldTimeStep > newTimeStep){
    #initialization of a new date sequence
    date_start = date_start
    if (outVerbose){
      logInfo(paste0("the date_start is: ",date_start))
      logInfo(paste0("the first estimate of date_end is: ",date_end))
    }
    date_end = date_end + oldTimeStep - newTimeStep # redefine the end date-time
    newDates = seq(from=date_start, to=date_end, by=newTimeStep) #create a new sequence of date with the new time step
    if (outVerbose){
      logInfo(paste0("the new date_end is: ",date_end))
      logInfo(paste0("the new date sequence is of length: ",length(newDates)," time steps."))
    }
    
    #conversion of dates into minutes. We do not expect data to be at a shorter time step.
    newDatesMins = as.numeric(newDates - date_start, units="mins") # convert dates to difftime objects in minutes
    oldDatesMins = as.numeric(df[,1]- date_start, units="mins") # convert dates to difftime objects in minutes
    
    #initialization of the new dataframe
    dfValues = matrix(NA, nrow = length(newDates), ncol=(dim(df)[2])-1)
    newDF = data.frame(date=newDates, dfValues)
    names(newDF) <- names(df)
    
    newDatesMins = newDatesMins / as.numeric(oldTimeStep, units="mins") #
    newDatesMins = floor(newDatesMins)
    # print(newDatesMins[1:30]) # to be commented : only for debugging
    oldDatesMins = oldDatesMins / as.numeric(oldTimeStep, units="mins")
    oldDatesMins = floor(oldDatesMins)
    # print(oldDatesMins[1:30]) # to be commented : only for debugging
    m = match(newDatesMins, oldDatesMins)
    # print(m[1:30]) # to be commented : only for debugging
    
    for (j in 2:dim(df)[2]){
      newDF[,j] = df[m,j]
      if (cumul){
        newDF[,j] = newDF[,j] * as.numeric(newTimeStep, units="mins")/as.numeric(oldTimeStep, units="mins")
      } else {
        #do nothing
      }
    }
  } else if (oldTimeStep < newTimeStep){
    
    #initialization of a new date sequence
    if (outVerbose){
      logInfo(paste0("the first estimate of date_start is: ",date_start))
    }
    
    if (newTimeStep <= as.difftime(1, units="hours")){
      # the new start date is the next hour
      date_start = ceiling_date(date_start, unit="hours") # make use of a 'lubridate' ceiling function
    } else if (newTimeStep <= as.difftime(1, units="days")){
      # the new start date is the next day
      date_start = ceiling_date(date_start, unit="days") # make use of a 'lubridate' ceiling function
    }
    if (outVerbose){
      logInfo(paste0("the new date_start is: ",date_start))
      logInfo(paste0("the first estimate of date_end is: ",date_end))
    }
    
    date_end = date_end + oldTimeStep - newTimeStep # redefine the end date-time
    newDates = seq(from=date_start, to=date_end, by=newTimeStep) #create a new sequence of date with the new time step
    newdate_end = max(newDates)
    
    if (outVerbose){
      logInfo(paste0("the new date_end is: ",date_end))
      logInfo(paste0("the new date sequence is of length: ",length(newDates)," time steps."))
    }
    
    
    # delete data of the initial data.frame which are older than date_start
    m_tooOld = which(df[,1]<date_start | df[,1] >= (newdate_end + newTimeStep))
    if (length(m_tooOld)>0){
      df = df[-m_tooOld,]
      if (outVerbose){
        logInfo(paste0("A total number of ",length(m_tooOld)," dates were taken out of the initial data.frame."))
      }
    }
    
    #conversion of dates into minutes. We do not expect data to be at a shorter time step.
    newDatesMins = as.numeric(newDates- date_start, units="mins") # convert dates to difftime objects in minutes
    oldDatesMins = as.numeric(df[,1]- date_start, units="mins") # convert dates to difftime objects in minutes
    
    #initialization of the new dataframe
    dfValues = matrix(NA, nrow = length(newDates), ncol=(dim(df)[2])-1)
    newDF = data.frame(date=newDates, dfValues)
    names(newDF) <- names(df)
    
    newDatesMins = newDatesMins / as.numeric(newTimeStep, units="mins")
    newDatesMins = floor(newDatesMins)
    # print("newDatesMins[1:30] =") # to be commented : only for debugging
    # print(newDatesMins[1:30]) # to be commented : only for debugging
    oldDatesMins = oldDatesMins / as.numeric(newTimeStep, units="mins")
    oldDatesMins = floor(oldDatesMins)
    # print("oldDatesMins[1:30] =") # to be commented : only for debugging
    # print(oldDatesMins[1:30]) # to be commented : only for debugging
    
    # loop on each column of the old dataframe (first column is reserved for dates)
    for (j in 2:dim(df)[2]){
      if (cumul){
        newDF[,j] = tapply(df[,j], oldDatesMins, FUN=sum)
      } else {
        newDF[,j] = tapply(df[,j], oldDatesMins, FUN=mean)
      }
    }
  }
  if (outVerbose){
    logInfo("... END running local function: convertDFtimestep. A data frame is returned.")
  }
  return(newDF)
}

# LOCAL FUNCTION
#' Title: Inverse Distance Weighting rainfall estimation GSTAT method
#'
#' @param rainfall_df - dataframe, column 1 is the date column in POSIXct format, and column 2 is the rainfall amount
#' @param stationP - SpatialPoints object, corresponds to the location of rainfall weather stations, it should have an attribute 'code'
#' @param rain_TS - dataframe object, corresponds to the list of sites codes that WIMES API sends to the R script.
#' @param domaine - extent object, (package raster), it is the extent of the rainfall raster we 
#' @param dateFormat - string - it defines the date format to be used wihtin the name of each raster layer
#'
#' @return raster stack object - a stack of rainfall raster. One raster per date/time (warning, when nodata in the raster, there is no raster created in the stack) The date/time info is stored in the attribute of each raster of the stack
#'
#' @note RUN IDW interpolation (GSTAT package) for each single date
runIdwGstatMethod = function (rainfall_df, stationP, rain_TS, domaine, dateFormat="D%Y%m%dT%H%M%SZ") {
  logInfo("The function 'runIdwGstatMethod' has just been called")
  # get the coordinates of every pixel  
  xy=data.frame(xyFromCell(domaine, seq(1, ncell(domaine))))
  n_steps = dim(rainfall_df)[1]  #number of time steps within the dataframe rainfall_df
  stationP_code=as.character(stationP$code) #get the code of each rainfall ground station (as a character)
  m_stationP=match(rain_TS$siteCode,stationP_code) #get the list of index from stationP which match the station codes within rain_TS (and thus within rainfall_df)
  
  #IDW_raster_stack <- stack() # create an empty stack
  IDW_raster_stack <- list() # create an empty stack
  
  # create a data frame from the input parameter 'xy'
  grilleIN = data.frame(X=xy[,1],Y=xy[,2],RES=1)
  # Data frame have to be recongnised as a geospatial entity 
  coordinates(grilleIN) = ~X+Y
  crs(grilleIN) <- crs(stationP) #assign the working CRS
  n_not_NA<-rep(NA,n_steps)
  # Start loop on each timestep (there are n_steps number of time step) 
  logInfo(paste("there are ",n_steps," IDW rasters to compute",sep=""))
  for ( j in 1:n_steps)
  {
    # print of the date
    #print(paste("Hi, today-now is ",rainfall_df[j,1]))
    
    
    # rainfall_df temp 
    rainfall_df_temp=as.numeric(rainfall_df[j,2:length(rainfall_df)])  #we start the index at '2', because the first column is 'date'
    indx_na=which(!is.na(rainfall_df_temp))             #vector of indexes where data is different from 'NA'
    n_not_NA[j]<-length(indx_na)
    #CHECK whether there isn't any rainfall data available at this timestep
    if (length(indx_na)==0){
      #DO NOT perform the IDW interpolation
      temp_raster_rain=raster(domaine) # fill with a raster with 'NA' values (remind that domaine is an object of class 'extent')
    } else {
      # DO perform the IDW interpolation
      
      # creer une fonction qui va chercher les postes du jour j en NA et les enleve de la spatialisation sinon ca plante !!!!!! :-)
      x_temp=coordinates(stationP)[m_stationP[indx_na],1]
      y_temp=coordinates(stationP)[m_stationP[indx_na],2]
      rainfall_df_temp=rainfall_df_temp[indx_na]
      
      # il cree une data frame ou il y a les coordonnees + les valeurs des pluies 
      grilleIN2 = data.frame(X=as.numeric(x_temp), Y=as.numeric(y_temp), RAIN_IDW=rainfall_df_temp)
      # the data frame is converted into an object of class 'SpatialPointsDataFrame' by using the function 'coordinates' 
      coordinates(grilleIN2) = ~X+Y
      crs(grilleIN2) <- crs(stationP) #assign the working CRS
      # methode de spatialisation des pluies par inverse de la distance au carre 
      RAIN_SPA_IDW=idw(RAIN_IDW~1, locations=grilleIN2, newdata=grilleIN, idp=2, debug.level=0) #debug.level=0 is to suppress all printed information  
      # creation du raster de pluie interpolee  
      gridded(RAIN_SPA_IDW)<-TRUE
      temp_raster_rain<-raster(RAIN_SPA_IDW)
      crs(temp_raster_rain)<-crs(stationP)
      #temp_raster_rain=rasterFromXYZ(data.frame(X=xy[,1],Y=xy[,2], RAIN_SPA_IDW$var1.pred), crs=crs(stationP) )
      # on lui a impose le systeme de projetion 'working_crs' defini au debut du script.
    }
    names(temp_raster_rain)  = strftime(rainfall_df[j,1], format=dateFormat, tz="UTC")
    #temp_raster_rain@title = as.character(rainfall_df[j,1])  # give a title to the raster: the date time stamp
    #IDW_raster_stack <- stack(IDW_raster_stack, temp_raster_rain) #add the interpolated rainfall raster layer (or the NA raster) to the stack
    IDW_raster_stack[[j]] <- temp_raster_rain
    
  }  # END of the loop on dates/time 
  IDW_raster_stack <- stack(IDW_raster_stack) #much faster than "stacking" the raster at each step!
  upsertQuantValuesByChunks(data.frame(timeSeriesCode=rep("KMD_AVAILABLE_RAIN_GAGES_1H",n_steps),
                                       observationDateTime=rainfall_df[,1],
                                       value=n_not_NA),
                            500)
  logInfo("END of the function 'runIdwGstatMethod'")
  return(IDW_raster_stack) #that is the object that is returned by 'runIdwGstatMethod'
  
}   # End of the IDW method


#' Compute basin areal rainfall based on a rainfall raster stack.
#'
#' @param bvs SpatialPolygon object - created by the local function 'getBasinsGeometry'
#' @param domaine extent object
#' @param RainRasterStack Raster stack object - created by the functions 'getForecastRasterStack' or 'getObservRasterStack' or 'runIDWGstatMethod'
#' @param Rain_TS 
#' @param outVerbose logical (optionnal, default FALSE) - defines whether information messages should be printed
#' @param dateFormat string - it defines the date format to be used wihtin the name of each raster layer
#'
#' @export
#'
getBasinAreaRainfall=function(bvs, domaine, RainRasterStack, Rain_TS, dateFormat, outVerbose=FALSE){
  logInfo("The function 'getBasinAreaRainfall' has just been called... ")
  CRS_rainRaster = crs(domaine)
  #bvs_newCRS = spTransform(bvs, CRS_rainRaster)
  #matrix_bv = getBasinCoverage(bvs_newCRS, domaine) # compute the coverage percentage % of each basin for every pixel
  
  stack_bv = getBasinCoverage(bvs, domaine, outVerbose)  #compute the coverage percentage % of each basin as a raster stack
  stack_bv = crop(stack_bv, domaine)
  bv_weights = cellStats(x=stack_bv, stat='sum', na.rm=TRUE) #sum of cell stats within each raster layer of stack_bv
  
  if (outVerbose){
    logInfo(paste0("stack_bv is the raster stack with basins coverage in percent for each pixel"))
    logInfo(paste0("stack_bv resolution is: ",res(stack_bv)))
  }
  
  m_bvs = match(names(bvs), Rain_TS$siteCode) # list of index that match betwwen the names of the polygons of basin contours (bvs) and the siteCode within basin_TS
  
  n_steps = dim(RainRasterStack)[3]
  #n_steps <- length(RainRasterStack) if we switch from stack to list
  basinRains = matrix(NA, nrow=n_steps, ncol=length(bvs)) # initialization of the matrix containing basin rainfall amounts
  basinRainsDF = data.frame(date = rep(as.POSIXct("1990-01-01", tz="UTC"), times=n_steps), basinRains) # the date 1990-01-01 is fictitious. the only purpose is initialization of the dataframe
  names(basinRainsDF)[2:(length(bvs)+1)] <- Rain_TS$code[m_bvs] # assign column names to the data frame
  
  logInfo("'getBasinAreaRainfall': start a FOR loop and extract rainfall at each timestep")
  for ( j in (1:n_steps)) #start a loop on each raster layer within the raster stack 'FcstRasterStack'
  {
    rain_raster = RainRasterStack[[j]] #extract the raster layer no. 'j' within the raster stack 'FcstRasterStack'
    #example from runInterpolationIDW: date_raster = IDW_result[[j]]@title
    date_raster = names(rain_raster) # the date information is stored in the title attribute of the raster layer
    date_out = as.POSIXct(date_raster, format=dateFormat, tz="UTC")
    basinRainsDF$date[j] = date_out # write the dates within the data frame
    if (outVerbose) {
      logInfo(paste0("The current raster layer date time is: ",date_out))
    }
    
    #rainExtract = extract(x=rain_raster, y=bvs_newCRS, small=TRUE, weight=TRUE) # extract the rainfall values depending on 'bvs' which is a SpatialPolygons object
    
    stack_bv_rain = stack_bv * rain_raster # multiply the rain_raster to each raster layer within the stack_bv
    names(stack_bv_rain) = names(stack_bv)
    bv_rains = cellStats(x=stack_bv_rain, stat='sum', na.rm=TRUE) # for each raster layer of stack_bv_rains, we compute the sum of cell values
    
    bv_rains = bv_rains / bv_weights
    names(bv_rains) = names(stack_bv)
    basinRainsDF[j, 2:(length(bvs)+1)] <- bv_rains
    
  }
  logInfo("End of local function 'getBasinAreaRainfall', a dataframe is returned. ")
  return(basinRainsDF)
}

#LOCAL FUNCTION
#' Title
#'
#' @param Order_df dataframe object - 
#' @param Node_routedlinks_mat matrix object -
#' @param Node_routedlinks 
#' @param Code_routedlinks 
#' @param outVerbose 
#'
orderingNodes = function(Order_df, Node_routedlinks_mat, Node_routedlinks, Code_routedlinks, 
                         outVerbose=FALSE){
  logInfo("The function 'orderingNodes' has just been called.")
  i=1
  N=length(Order_df$Code)
  start_time<- Sys.time()
  duration_time=0
  
  while ((i < N) && (duration_time<10)) {
    code_i=Order_df$Code[i]
    m_code=which(Code_routedlinks==code_i)
    order_max=max(Order_df$order)
    order_i=Order_df$order[i]
    
    if ((substr(Order_df$Code[i],1,1) == "1")){
      if(outVerbose){
        logInfo(paste0(Order_df$Code[i]," is a station node"))
      }
    } else if ((substr(Order_df$Code[i],1,1) == "R")) {
      if(outVerbose){
        logInfo(paste0(Order_df$Code[i]," is a routed reach node"))
      }
    } else if ((substr(Order_df$Code[i],1,1) == "J")) {
      if(outVerbose){
        logInfo(paste0(Order_df$Code[i]," is a junction node"))
      }
    } else {
      logWarning(paste0(Order_df$Code[i]," is not a recognised type of node (1, R or J expected)"))
    }
    
    # what are the indexes of dependent nodes?
    m_depend = which(Node_routedlinks_mat[,m_code] == 1)
    if (length(which(m_depend==m_code))!=0){
      m_depend = m_depend[-which(m_depend==m_code)] # delete the index corresponding to node itself
    }
    if (length(m_depend)==0) {
      # in this case, the node does not depend on others, so we do not have to change its order
      #so let's go to the next node
      i=i+1
    } else {
      m_depend_df = match(Node_routedlinks$Nat_ID[m_depend] , Order_df$Code)
      order_max=min(order_max, max(Order_df$order[m_depend_df]))
      
      if (order_i<order_max) {
        #let's permute both nodes
        code_max=Order_df$Code[order_max]
        #TODO: comment the following message
        #message(paste0(" --> Permutation of ", code_i," (order ",order_i,") with ", code_max, " (order ",order_max,")"))
        Order_df$Code[order_max] <- code_i
        Order_df$Code[i] <- code_max
      } else {
        # ELSE: nodes are already in the good order, so go to next node
        i=i+1
      }
    }
    end_time <- Sys.time()
    duration_time=end_time-start_time
  } #END of the WHILE LOOP
  logInfo("END of the function 'orderingNodes'.")
  return(Order_df)
}





#LOCAL FUNCTION

#' Get forecast rainfall as a raster stack object
#'
#' @param TS_code character string giving the WIMES time series code of the forecast Grid
#' @param ProductionDate POSIXct date/time object - it refers to the production date of the forecast grid
#' @param dateFormat string - it defines the date format to be used wihtin the name of each raster layer
#'
#' @return Raster stack object (package raster), each layer of the stack represent a forecast time step
getForecastRasterStack=function(TS_code, ProductionDate, dateFormat="D%Y%m%dT%H%M%SZ"){
  logInfo("The function 'getForecastRasterStack' has just been called")
  wgs84_crs="epsg:4326" #This is the geographic projection WGS84 with longitude and latitude details in decimal degrees. Sites within WIMES are stored in WGS84
  
  forecastValues <- getForecastValuesByProductionDate(TS_code, ProductionDate)
  if (!is.null(dim(forecastValues)))
    {
    #keep only the records with grids in CoreDB
    nRawRaster<-length(forecastValues$value)
    forecastValues<-subset(forecastValues,value!="")
    nTrueRaster<-length(forecastValues$value)
    if (nTrueRaster<nRawRaster) { logWarning(paste0("there were ",nRawRaster-nTrueRaster," with missing files")) }
    }
  FcstRasterStack <- stack() # create an empty raster stack
  internProgress=0
  
  for (j in seq(along=forecastValues$value)){
    FcstRasterTiff <- getGrid(forecastValues$value[j]) #read the grid from WIMES server
    
    internProgressTemp = (j/length(forecastValues$value))*100
    if (internProgressTemp>=(internProgress+10)){
      internProgress = internProgressTemp
      updateProgression(floor(internProgress), paste0(".. getting forecast grid data from WIMES ",floor(internProgress),"%.."))
    }
    if(file.exists(FcstRasterTiff)) {
      FcstRaster     <- tryCatch(raster(FcstRasterTiff), 
                                 error=function(e) {
                                   logWarning(paste0(forecastValues$value[j]," - The forecast TIFF file: ",FcstRasterTiff," could not be converted into raster"))
                                   return(NULL)
                                 }
      )
      if (inherits(FcstRaster, "RasterLayer")){
        if (length(getValues(FcstRaster))==0) {
          logWarning(paste0("the raster file: ", FcstRasterTiff, " has no value in it, trying with getGridCurl"))
          #FcstRasterTiff = getGridCurl(forecastValues$value[j])
          FcstRaster     <- tryCatch(raster(FcstRasterTiff), 
                                     error=function(e) {
                                       logWarning(paste0("The forecast TIFF file: ",FcstRasterTiff," could not be converted into raster"))
                                       return(NULL)
                                     }
          )
        }
        # example from runIdwGstatMethod: temp_raster_rain@title = as.character(rainfall_df[j,1])  # give a title to the raster: the date time stamp
        names(FcstRaster)  = strftime(forecastValues$forecastDateTime[j], format=dateFormat, tz="UTC")
        crs(FcstRaster)=CRS(paste0("+init=",wgs84_crs)) # assign the projection information to the raster layer
        FcstRasterStack = stack(FcstRasterStack, FcstRaster)
      } 
    } else {
      logWarning(paste0("the file: ", FcstRasterTiff, " was not found"))
    }
  }
  logInfo("END of the function 'getForecastRasterStack', a raster stack is returned")
  return(FcstRasterStack)
}


#LOCAL FUNCTION
#' Get observed rainfall as a raster stack object
#'
#' @param Grid_TS 
#' @param ObsStartDate POSIXct object
#' @param ObsEndDate POSIXct object
#' @param dateFormat string - it defines the date format
#'
getObservRasterStack=function(Grid_TS, ObsStartDate, ObsEndDate, dateFormat="D%Y%m%dT%H%M%SZ"){
  logInfo("The function 'getObservRasterStack' has just been called")
  wgs84_crs="epsg:4326" #This is the geographic projection WGS84 with longitude and latitude details in decimal degrees. Sites within WIMES are stored in WGS84
  
  
  
  obsValues=getObservedValues(timeSeriesCode=Grid_TS, beginDateTime=ObsStartDate, endDateTime=ObsEndDate)
  if (!is.null(dim(obsValues)))
    {
    #keep only the records with grids in CoreDB
    nRawRaster<-length(obsValues$value)
    obsValues<-subset(obsValues,value!="")
    nTrueRaster<-length(obsValues$value)
    if (nTrueRaster<nRawRaster) { logWarning(paste0("there were ",nRawRaster-nTrueRaster," with missing files")) }
    }
  ObsRasterStack <- stack() # create an empty raster stack
  internProgress=0
  for (j in seq(along=obsValues$value)){
    ObsRasterTiff <- getGrid(obsValues$value[j])
    
    internProgressTemp = (j/length(obsValues$value))*100
    if (internProgressTemp>=(internProgress+10)){
      internProgress = internProgressTemp
      updateProgression(floor(internProgress), paste0(".. getting observed grid data from WIMES ",floor(internProgress),"%.."))
    }
    
    if(file.exists(ObsRasterTiff)) {
      ObsRaster     <- tryCatch(raster(ObsRasterTiff), 
                                error=function(e) {
                                  logWarning(paste0(obsValues$value[j]," - The observed TIFF file: ",ObsRasterTiff," could not be converted into raster"))
                                  return(NULL)
                                }
      )
      if (inherits(ObsRaster, "RasterLayer")){
        if (length(getValues(ObsRaster))==0) {
          logWarning(paste0("the raster file: ", ObsRasterTiff, " has no value in it, trying with getGridCurl"))
          ObsRaster     <- tryCatch(raster(ObsRasterTiff), 
                                    error=function(e) {
                                      logWarning(paste0("The observed TIFF file: ",ObsRasterTiff," could not be converted into raster"))
                                      return(NULL)
                                    }
          )
        }
        names(ObsRaster) = strftime(obsValues$observationDateTime[j], format=dateFormat, tz="UTC")
        crs(ObsRaster)=CRS(paste0("+init=",wgs84_crs)) # assign the projection information to the raster layer
        ObsRasterStack = stack(ObsRasterStack, ObsRaster)
      } 
    } else {
      logWarning(paste0("the file: ", ObsRasterTiff, " was not found"))
    }
  }
  logInfo("END of the function 'getObservRasterStack', a raster stack is returned")
  return(ObsRasterStack)
  
}

#local FUNCTION
#' Get the last available value
#' @description Get the last available value of TS based on time series code pattern
#' @param date_now POSIXct object - it defines the date-time of "today". It means we will search data that are equal or older than this "today" date.
#' @param Sites data frame - it has the same structure as the data frame returned by the RWIMES function 'getSites', or the local function 'GetBasinSitesList'
#' @param TScode_pattern string - it defines the pattern of timeseries codes that we want to select for bias factor
#'
#' @return data frame - first column is the timestamp (POSIXct format), the 1st column name is 'date'. Then there are as many columns as the number of timeseries matching the 'TScode_pattern'. If no time series are found, a NULL is returned.
#' @export
#'
getLastValByTScodepattern = function(date_now, Sites, TScode_pattern){
  logInfo("The function 'getLastValByTScodepattern' has just been called...")
  obs_TS     <- getAllTimeSeries(sitesCodes=Sites$code)
  m_obs       <- grep(pattern=TScode_pattern, obs_TS$code)
  if (length(m_obs)==0){
    logWarning(paste0("No observed time series were found for the label pattern: '", TScode_pattern,"' a NULL is returned."))
    return(NULL)
  }
  obs_TS     <- obs_TS[m_obs,]
  
  outValues = matrix(NA, nrow=1, ncol=length(obs_TS$code))
  colnames(outValues)=obs_TS$code # Assign timeseries codes to each column's name. It is easier for debugging the code afterwards
  
  # read water level data from WIMES: loop on AWL code
  for (i in 1:length(obs_TS$code)) {
    obsData = getObservedValues(timeSeriesCode = obs_TS$code[i], 
                                beginDateTime = NULL , 
                                endDateTime   = date_now, 
                                maxItems = 1) #get the last value before 'date_now'
    if (is.null(dim(obsData))){
      logInfo(paste0("No value could be retrieved from observation TS '",obs_TS$code[i], "' before ",as.character(date_now, format="%Y-%m-%d %H:%M:%S")))
      logInfo("A NA value is returned for this TS.")
    } else {
      outValues[,i] = obsData$value[1]
    }
  }
  outValues_df = data.frame(date=date_now, outValues)
  logInfo("... the function 'getLastValByTScodepattern' has just ended")
  return(outValues_df)
}

# LOCAL FUNCTION
#' Clean raw data (flow or rainfall) based on min and max threshold
#'
#' @param Q_df Dataframe object - first column is assumed to be a datetime column
#' @param Qmin numerical - minimum threshold. below this threshold the data is set to "NA"
#' @param Qmax numerical - maximum threshold. Above this threshold the data is set to "NA"
#'
#' @return data frame the same structure as input Q_df
#'
#' @examples
cleaningRawData = function(Q_df, Qmin, Qmax){
  numCol = dim(Q_df)[2]
  for (k in 2:numCol){
    m = which((Q_df[,k] < Qmin | Q_df[,k] > Qmax) & !is.na(Q_df[,k]))
    if (length(m)>0){
      Q_df[m,k] = NA
    }
  }
  return(Q_df)
}

#LOCAL FUNCTION
computeContinuityWeights<-function(nWeights,tStart,tEnd,initialWeight,finalWeight){
	#nWeights<-max(0,nWeights)
	if (tEnd==0)
		{
		continuityWeights<-rep(0,nWeights)
		}
	else
		{
		#quadratic function implemented previously
		#continuityWeights<-c(rep(initialWeight,tStart),initialWeight-(1:tEnd)^2/tEnd^2*(initialWeight-finalWeight),finalWeight+rep(0,nWeights-(tEnd+tStart)))
		#gaussian function newly implemented
		continuityWeights<-c(rep(initialWeight,tStart),initialWeight-(1-exp(-((0:(nWeights-tStart-1))/tEnd)^2))*(initialWeight-finalWeight))
		}
	return(continuityWeights)
}


#LOCAL FUNCTION
#' Correct forecasted data based on observed data
#'
#' @param obsValues data.frame object - Observed data. first column is the date column in POSIXct format. 1st column name is "observationDateTime", second column is "value"
#' @param fcstValues data.frame object - Observed data. first column is the date column in POSIXct format. 1st column name is "forecastDateTime", second column is "value"
#' @param correcCoeff vector of 5 coefficients used to correct the forecast data (t0,t1,t2,finalOffset,qmin)
#'
#' @return data.frame object which has the same length as fcstValues, and also the same column names
#' @export
#'
#' @examples
correcFcstWithObs <- function(obsValues,fcstValues,correcCoeff){
	t0<-correcCoeff[1]
	t1<-correcCoeff[2]
	t2<-correcCoeff[3]
	finalOffset<-correcCoeff[4]
	qmin<-correcCoeff[5]
	initNames<-names(fcstValues)
	#rename colnames so as to align with considered format
	fcstValues$productionDateTime<-NULL #in case you start from a "real" forecast time serie
	names(fcstValues)<-c("forecastDateTime","value")
	#sort data frames in increasing order
	fcstValues<-fcstValues[order(fcstValues$forecastDateTime),]
	#keep trace of the initial range of forecast date time
	forecastDateTime<-fcstValues$forecastDateTime
	if (!is.null(names(obsValues)))
		{
		names(obsValues)<-c("observationDateTime","value")
		obsValues<-obsValues[order(obsValues$observationDateTime),]
		#clean spurious data
		obsValues$value[obsValues$value>1500]<-NA
		#check whether you eventually have at least two non NA values
		n_nonNA<-length(obsValues$value[!is.na(obsValues$value)])
		if (n_nonNA>=2)
			{
			#align observation dates and times on forecast dates and times
			obsValuesInterp<-approx(obsValues$observationDateTime,obsValues$value,fcstValues$forecastDateTime)
			obsValuesInterp<-data.frame(observationDateTime=obsValuesInterp$x,value=obsValuesInterp$y)
			obsValuesInterp<-subset(obsValuesInterp,!is.na(obsValuesInterp$value))
			nObs<-length(obsValuesInterp$value)
			}
		else
			{
			nObs<-0
			}
		}
	else
		{
		nObs<-0
		}
	if (nObs>=1)
		{
		logInfo("trying to apply correction...")
		#save past dates and time of forecast 
		pastDateTime<-fcstValues$forecastDateTime[fcstValues$forecastDateTime<=max(obsValuesInterp$observationDateTime)]
		#keep only forecast in future
		fcstValues<-subset(fcstValues,fcstValues$forecastDateTime>=max(obsValuesInterp$observationDateTime))
		#compute derivative of order 1 and 2
		dFcst1<-diff(fcstValues$value)
		dFcst2<-diff(dFcst1)
		nFcst<-length(fcstValues$value)
		if (nFcst<2)
			{
			logWarning(paste0("this time serie only have ",nFcst," values available for continuity correction"))
			logInfo(paste0("the maximum observation date time is ",max(obsValuesInterp$observationDateTime)))
			logInfo(paste0("the maximum forecast date time is ",max(fcstValues$forecastDateTime)))
			}
		#compute weights (initial and final weights hard coded)
		w2<-computeContinuityWeights(nFcst-2,0,t2,0.2,0.0)
		w1<-computeContinuityWeights(nFcst-1,t2,t1-t2,0.75,0.0)
		w0<-computeContinuityWeights(nFcst,t1,t0-t1,1.0,0.0)
		if (nObs>=3)
			{
			logInfo("ensuring continuity of derivative of order 2...")
			#ensure continuity of derivative 2
			d2<-obsValuesInterp$value[nObs]-2*obsValuesInterp$value[nObs-1]+obsValuesInterp$value[nObs-2]
			#apply decreasing correction down to 0 for t2
			dFcst2<-w2*(d2-dFcst2[1])+dFcst2
			dFcst12<-dFcst1[1]+c(0,cumsum(dFcst2))
			#recompute weighted average so as to avoid overall offset
			dFcst1<-w1*dFcst12+(1-w1)*dFcst1
			}
		if (nObs>=2)
			{
			logInfo("ensuring continuity of derivative of order 1...")
			#ensure continuity of derivative 1
			d1<-obsValuesInterp$value[nObs]-obsValuesInterp$value[nObs-1]
			#apply linearly decreasing correction down to 0 for t1
			dFcst1<-w1*(d1-dFcst1[1])+dFcst1
			fcst1<-fcstValues$value[1]+c(0,cumsum(dFcst1))
			#recompute weighted average so as to avoid overall offset
			fcst0<-w0*fcst1+(1-w0)*fcstValues$value
			}
		logInfo("ensuring continuity of flow...")
		#ensure continuity
		d0<-obsValuesInterp$value[nObs]
		#apply linearly decreasing correction down to intermediate difference for t0
		w0<-computeContinuityWeights(nFcst,t1,t0-t1,1.0,finalOffset)
		fcst00<-w0*(d0-fcst0[1])+fcst0
		logInfo("correction applied up to t0...")
		#apply linearly decreasing correction down to 0 for tmax
		#w0<-computeContinuityWeights(nFcst,t0,nFcst-t0,1.0,0.0)
		w0<-c(rep(1.0,t0),((nFcst-t0):1)/(nFcst-t0))
		fcstValues$value<-w0*fcst00+fcstValues$value*(1-w0) #need to check that vectors have the same length!!!!!
		#ensure that the correction hasn't led to unrealistic flow values
		fcstValues$value<-pmax(fcstValues$value,qmin)
		logInfo("correction applied")
		#ensure that observation covers the full range of forecast date time
		if (min(obsValuesInterp$observationDateTime)>min(forecastDateTime))
			{
			logInfo("extending forecast in the past to cover the expected time period...")
			#insert date at the begining of the time serie
			tempObservationDateTime<-c(min(pastDateTime),obsValuesInterp$observationDateTime)
			tempValue<-c(obsValuesInterp$value[1],obsValuesInterp$value)
			obsValuesInterp<-approx(tempObservationDateTime,tempValue,pastDateTime)
			names(obsValuesInterp)<-c("observationDateTime","value")
			}
		#concatenate observed and forecast values into a composite TS
		fcstFlow<-c(obsValuesInterp$value,fcstValues$value[2:length(fcstValues$value)])
		#define a lower bound for flows to avoid problems with Muskingum propagation
		fcstFlow<-pmax(qmin,fcstFlow)
		fcstValues<-data.frame(forecastDateTime=forecastDateTime,
								value=fcstFlow)
		}
	else
		{
		logInfo("there was no observed data to correct the forecast")
		}
	names(fcstValues)<-initNames
	return(fcstValues)
}


#LOCAL FUNCTION
# temporary solution: GR4H calibration parameters are hard written
#' Get GR4H calibrated parameters
#'
#' @param Basin_Sites data frame (optional) - output of the function 'getBasinSitesList' (same structure as the data frame returned by the RWIMES function 'getSites')
#'
#' @return a dataframe with values of X1, X2, X3 and  X4 parameters. Each line of the dataframe correspond to one basin
#' @export
#'
#' @examples
getGRCalibParam=function(Basin_Sites=NULL){
  logInfo("The local function 'getGRCalibParam' has just been called...")
  if (!is.null(Basin_Sites)){
    date_now=Sys.time()
    as.POSIXct(date_now) # make sure the date is in POSIXct format
    attributes(date_now)$tzone<- "UTC"
    calib_mat = matrix(NA, ncol=4, nrow=length(Basin_Sites$code)) # we create an empty matrix, it will receive the calibrated parameters values
    for (i in 1:4){
      GR4H_TScode_pattern = paste0("GR4H_....._BAS_X",i,"_cal")
      calib_df_temp=getLastValByTScodepattern(date_now=date_now, 
                                         Sites=Basin_Sites, 
                                         TScode_pattern=GR4H_TScode_pattern)
      calib_df_temp=calib_df_temp[,-1] # we delete the first column which corresponds to the now_date
      calib_mat[,i]=t(calib_df_temp[1,]) # we have to transpose the dataframe to put it into the matrix
    }
    m_na=which(is.na(calib_mat)) # looking for NA values within the matrix
    if (length(m_na)>0){
      logWarning(paste0("In function 'getGRCalibParam', a total number of ",length(m_na)," value(s) are NA."))
      logInfo("-->It may lead to a crash of the script.")
    }
    #basinID = substr(Basin_Sites$code,start=1,stop=5) # we extract just the ID of each basin, it means a 5-cahracters long identifier
    basinID = substr(names(calib_df_temp),start=6,stop=10)
    calib_df = data.frame(basinID, calib_mat)
  } else {
    # if no basin sites is defined as an input argument of the function, then we take some value by defalut
    logInfo("No basin sites specified. Default values will be used.")
    calib_mat=c(3261.68757,0.544054728,96.01586687,105.6936937,
                1582.699178,0.148678869,128.0923569,115.1655942,
                3261.68757,0.544054728,96.01586687,105.6936937,
                1326.103206,-0.476542632,229.1813148,54.63063062,
                828.2742567,0.047318249,297.4698026,41.14779794,
                656.2839828,-0.366757177,283.4282719,51.31329682,
                1326.103206,-0.476542632,229.1813148,54.63063062,
                632.7022928,-0.183696666,294.2746332,52.05405406,
                561.1565939,0.651996131,377.8561084,25.81981982,
                1261.428389,1.596765177,268.9467642,30.03603605,
                1261.428389,1.596765177,268.9467642,30.03603605,
                1261.428389,1.596765177,268.9467642,30.03603605)
    calib_mat=matrix(calib_mat, ncol=4, byrow=TRUE)
    basin_names=c("1BC01",
                  "1BD02",
                  "1BE06",
                  "1BG07",
                  "1CB05",
                  "1CE01",
                  "1DA02",
                  "1DB01",
                  "1DD01",
                  "1ED01",
                  "1EF01",
                  "1FG02")
    calib_df=data.frame(basin_names,calib_mat)
  }
  names(calib_df)=c("Nat_ID","X1","X2","X3","X4")
  logInfo("The local function 'getGRCalibParam' has just ended. A data frame is returned.")
  return(calib_df)
}


#LOCAL FUNCTION
# this function is aimed at providing an interim solution in order to get the routed links matrix
# each line indicates the nodes that depend on the considered node in order to be computed
getNodeRoutedLinks=function(){
  routed_mat=c(1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,
               0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,
               0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,
               0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,
               0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,
               0,0,0,0,0,1,0,0,1,0,0,0,0,0,0,
               0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,
               #0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,
               0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,
               0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
               1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
               0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,
               0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,
               0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,
               0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,
               0,0,0,0,0,0,0,1,0,0,0,0,0,0,0)
			   #0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0) # to add 1EE03 as an intermediate node
  routed_mat=matrix(routed_mat, nrow=15, byrow=TRUE)
  link_name=c("1BD02",
              "1BE06",
              "1CB05",
              "1CE01",
              "1DA02",
              "1DD01",
              "1ED01",
			  #"1EE03",
              "1EF01",
              "Jun1DD01y1ED01",
              "R_1BE06_to_1BD02",
              "R_1BD02_to_1DA02",
              "R_1CB05_to_1CE01",
              "R_1CE01_to_1DA02",
              "R_1DA02_to_1DD01",
              "R_Jun1DD01y1ED01_to_1EF01") #"R_Jun1DD01y1ED01_to_1EE03"
			  #R_1EE03_to_EF01)
  routed_df=data.frame(link_name,routed_mat)
  names(routed_df)=c("Nat_ID","Q_1BD02","Q_1BE06","Q_1CB05","Q_1CE01","Q_1DA02","Q_1DD01","Q_1ED01","Q_1EF01","Q_Jun1DD01y1ED01","R_1BE06_to_1BD02","R_1BD02_to_1DA02","R_1CB05_to_1CE01","R_1CE01_to_1DA02","R_1DA02_to_1DD01","R_Jun1DD01y1ED01_to_1EF01")
  return(routed_df)
}


#LOCAL FUNCTION
getRoutingParam=function(){
  #old parameters
  #param_mat=c(0,1,7,8,22,23,27,28,1792,1790,1789,1786,1786,1789,1790,1792,5,25,5,0.00163482,47100,
              #0,0.5,2,3,25,26,27,28,1720,1718,1717,1715,1715,1717,1717.5,1720,5,25,5,0.004423317,54484,
              #0,3,9,10.7,28,29.6,33,35,3,1,0.75,-1,-0.9,0,2,4,5,25,5,0.007296488,18365,
              #0,3,9,10.7,28,29.6,33,35,3,1,0.75,-1,-0.9,0,2,4,5,25,5,0.005639674,32626,
              #0,0.01,5.6,11.6,54.58,61.6,65,70,5,1.5,1,-1.5,-1,2,2.2,5,5,25,5,0.003521368,58500,
              #-1,0,4.5,6.5,54,59,64,65,1180,1178,1177,1176,1176,1177,1177.5,1180,5,25,5,0.001806897,72500)
  #updated parameters (20/09/2020)
  param_mat=c(0,1,7,8,22,23,27,28,1792,1790,1789,1786,1786,1789,1790,1792,5,28,8,0.00137,48100,
              0,0.5,2,3,25,26,27,28,1720,1718,1717,1715,1715,1717,1717.5,1720,5,28,8,0.0042,50800,
              0,3,9,10.7,28,29.6,33,35,3,1,0.75,-1,-0.9,0,2,4,5,28,8,0.0079,19500,
              0,3,9,10.7,28,29.6,33,35,3,1,0.75,-1,-0.9,0,2,4,5,28,8,0.0048,27800,
              0,0.01,5.6,11.6,54.58,61.6,65,70,5,1.5,1,-1.5,-1,2,2.2,5,5,28,8,0.0034,70100,
              -100,-99,4.5,6.5,54,59,164,165,1182,1179,1177,1175,1175,1177,1179,1182,5,28,8,0.0017,75000) # weighted average between 1DD01 / 1ED01 to 1EF01 since there is no reach to Jun1DD01y1ED01
			  #-100,-99,4.5,6.5,54,59,164,165,1182,1179,1177,1175,1175,1177,1179,1182,5,28,8,0.0017,45000)
  param_mat=matrix(param_mat, nrow=6, byrow=TRUE)
  link_name=c("R_1BE06_to_1BD02",
              "R_1BD02_to_1DA02",
              "R_1CB05_to_1CE01",
              "R_1CE01_to_1DA02",
              "R_1DA02_to_1DD01",
              "R_Jun1DD01y1ED01_to_1EF01") #"R_Jun1DD01y1ED01_to_1EE03"
			  #R_1EE03_to_EF01)
  param_df=data.frame(link_name, param_mat)
  names(param_df)=c("Code","x1","x2","x3","x4","x5","x6","x7","x8","z1","z2","z3","z4","z5","z6","z7","z8","K_RightBank","K_MainChannel","K_LeftBank","Slope_I","Length_m")
  return(param_df)
}

#LOCAL FUNCTION
#format = duration of continuity smoothing (in hours)
#         duration of continuity smoothing for the first derivative (in hours)
#         duration of continuity smoothing for the second derivative (in hours)
#         ratio of initial difference kept at the end of duration of continuity smoothing
#         minimum discharge to maintain after correction (to avoid meaningless results)
getCorrecMatrix=function(){
	correcMatrix<-list("1DA02"=c(16,2,1,0.5,14),
						"1EF01"=c(20,4,1,0.6,20),
						"1ED01"=c(12,2,0,0.6,8),
						"1BD02"=c(16,2,1,0.4,6),
						"1BE06"=c(16,2,1,0.6,1),
						"1CB05"=c(10,2,0,0.3,3),
						"1CE01"=c(12,2,0,0.4,8))
						#"1EE03"=c(20,4,1,0.6,20))
	return(correcMatrix)
}

#LOCAL FUNCTION
#' Function to get large observed data sets
#'
#' @export
#' @param TS string - name of the input time serie to retrieve
#' @param deb POSIXct - start date of data to retrieve
#' @param fin POSIXct - end date of data to retrieve
#' @return data frame - simplified format with two attributes 'observationDateTime' and 'value'
#' 
#' @examples
#' 
#' @note
#' 
getObservedValuesByChunks <- function(TS,deb,fin)
	{
	datesw<-seq(deb,fin,by="weeks")
	datesw[1]<-datesw[1]-1
	if (datesw[length(datesw)]<fin) datesw<-c(datesw,fin)
	init<-TRUE
	valuesh<-data.frame(observationDateTime=as.POSIXct(character()),value=numeric())
	for (i in (length(datesw)-1):1)
		{
		values <- getObservedValues(TS, datesw[i]+1,datesw[i+1])
		if (init)
			{
			if (!is.null(dim(values)))
				{
				if ((dim(values)[2]==1)||(dim(values)[2]==4))
					{
					values$value<-rep(NA,length(values$observationDateTime))
					}
				valuesh<-data.frame(observationDateTime=values$observationDateTime,value=values$value)
				init<-FALSE
				}
			}
		else
			{
			if (!is.null(dim(values)))
				{
				if ((dim(values)[2]==1)||(dim(values)[2]==4))
					{
					values$value<-rep(NA,length(values$observationDateTime))
					}
				valuesh<-rbind(valuesh,data.frame(observationDateTime=values$observationDateTime,value=values$value))
				
				}
			}
		}
	return(valuesh)
	}

#LOCAL FUNCTION
#' Function to aggregate time series from one time step to a (supposedly) larger time step 
#'
#' @export
#' @param df data frame - object returned by the function 'getObsDataByTScodepattern' (first column are dates (POSIXct), subsequent columns are values)
#' @param oldTimeStep difftime - old (input) time step
#' @param newTimeStep difftime - new (output) time step
#' @param previous logical - set to TRUE if data are of type "preceeding total"
#' @return data frame - data aggregated at newTimeStep
#' 
#' @examples
#' 
#' @note
#' 
aggreg_timestep <- function(df,oldTimeStep,newTimeStep,previous=TRUE)
	{
	oldNames<-names(df)
	#make sure that the ratio between new and old time step is greater than one and an integer
	rTimeStep<-as.numeric(newTimeStep,units="secs")/as.numeric(oldTimeStep,units="secs")
	if (rTimeStep<1) logInfo("the new time step for aggregation is smaller thant the old time step!")
	if (round(rTimeStep)<rTimeStep) logInfo("the new time step is not a multiple of the old time step!")
	rTimeStep<-as.integer(round(rTimeStep))
	#define the upper and lower bounds so that all old time steps are located inside them
	if (newTimeStep<as.difftime(1,unit="hours")) newTimeStep_string<-paste(as.numeric(newTimeStep,units="mins")," mins",sep="")
	else newTimeStep_string<-paste(as.numeric(newTimeStep,units="hours")," hours",sep="")
	tMin<-floor_date(min(df[,1]),newTimeStep_string)
	tMax<-ceiling_date(max(df[,1]),newTimeStep_string)
	#define regular time sequences
	tOld<-seq(tMin,tMax,by=oldTimeStep)
	tNew<-seq(tMin,tMax,by=newTimeStep)
	attributes(tOld)$tzone<-"UTC"
	attributes(tNew)$tzone<-"UTC"
	attributes(df[,1])$tzone<-"UTC"
	#fill in all the old time steps for the input time serie (to provide consistent aggregated values)
	dfOld<-merge(data.frame(datesh=tOld),df,by.x="datesh",by.y=names(df)[1],all.x=TRUE,all.y=FALSE)
	nBas<-dim(dfOld)[2]-1
	#apply aggregation (sliding filter) NOTE: added namespace because of conflict with dplyr package
	if (previous) valuesNew<-stats::filter(dfOld[,2:(nBas+1)],rep(1,2),sides=1,method="convolution")
	else valuesNew<-stats::filter(dfOld[,2:(nBas+1)],rep(1,2),sides=2,method="convolution")
	#keep only matching values with respect to new time steps
	dfNew<-merge(data.frame(datesh=tNew),data.frame(datesh=tOld,valuesNew),by.x="datesh",by.y="datesh",all.x=TRUE,all.y=FALSE)
	names(dfNew)<-oldNames
	return(dfNew)
	}

#LOCAL FUNCTION
#' Function to compute combined KMD and GPM basin rainfall according to the number of available rain gages 
#'
#' @export
#' @param df_KMD data frame - object returned by the function 'getObsDataByTScodepattern'
#' @param df_GPM data frame - object returned by the function 'aggreg_timestep'
#' @param nRainGages data frame - object returned by the function 'getObservedValuesByChunks'
#' @param timestep difftime - expected timestep of the result data frame
#' @param defaultValue float - default value to use when input is NA
#' @return data frame - combined basin rainfall
#' 
#' @examples
#' 
#' @note
#' 
combine_KMD_GPM <- function(df_KMD,df_GPM,nRainGages,timeStep,defaultValue=NA)
	{
	nBas<-dim(df_KMD)[2]-1
	#define the upper and lower bounds so that all old time steps are located inside them
	tMin<-min(min(df_KMD[,1]),min(df_GPM[,1]))
	tMax<-max(max(df_KMD[,1]),max(df_GPM[,1]))
	#define regular time sequence
	tStep<-seq(tMin,tMax,by=timeStep)
	#create no gaps time series
	df_KMD2<-merge(data.frame(datesh=tStep),df_KMD,by.x="datesh",by.y=names(df_KMD)[1],all.x=TRUE,all.y=FALSE)
	nRainGages2<-merge(data.frame(datesh=tStep),nRainGages,by.x="datesh",by.y=names(nRainGages)[1],all.x=TRUE,all.y=FALSE)
	df_GPM2<-merge(data.frame(datesh=tStep),df_GPM,by.x="datesh",by.y=names(df_GPM)[1],all.x=TRUE,all.y=FALSE)
	#if there less than 3 rain gages, apply some areal reduction factor to KMD rainfall in order to avoid uniform heavy rainfall
	nR<-nRainGages2[,2]
	nR[is.na(nR)]<-3 #if no information about the number of available rain gages, make the assumption that there are 30%
	for (j in 1:nBas) df_KMD2[nR==2,j+1]<-df_KMD2[nR==2,j+1]*0.5 #apply a 1/2 correction factor if there are only 2 gages available throughout the basin
	for (j in 1:nBas) df_KMD2[nR==1,j+1]<-df_KMD2[nR==1,j+1]*0.333 #apply a 1/3 correction factor if there are only 1 gage available throughout the basin
	#compute weights: if at least 10 rain gages were available, take a 0.85 weight for KMD (keep a 0.15 weight for GPM just in case KMD data are spurious)
	KMDWeights<-pmax(0,(pmin(10,nRainGages2[,2])/10)^0.5-0.15)
	#if nRaingages is NA, take a 0.5 weight (prior fifty-fifty confidence)
	KMDWeights[is.na(nRainGages2[,2])]<-0.5
	valCombined<-KMDWeights*df_KMD2[,2:(nBas+1)]+(1-KMDWeights)*df_GPM2[,2:(nBas+1)]
	dfCombined<-data.frame(df_KMD2[,1],valCombined)
	names(dfCombined)<-names(df_KMD)
	#if KMD is NA, take GPM 
	for (j in 1:nBas) dfCombined[is.na(df_KMD2[,j+1]),j+1]<-df_GPM2[is.na(df_KMD2[,j+1]),j+1]
	#if GPM is NA, take KMD
	for (j in 1:nBas) dfCombined[is.na(df_GPM2[,j+1]),j+1]<-df_KMD2[is.na(df_GPM2[,j+1]),j+1]
	#if result is NA, replace by defaultValue (modified since it was not consistent with the way last observation date is computed in function "runSimuBasinsFlowsGR")
	for (j in 1:nBas) dfCombined[is.na(dfCombined[,j+1]),j+1]<-defaultValue
	return(dfCombined)
	}

#LOCAL FUNCTION
#' Function to upsert large observed data sets (equivalent to upsertQuantValuesBig?) 
#'
#' @export
#' @param df data frame - data frame of data to upsert (same format as expected input of upsertQuantValues)
#' @param chunkSize integer - size of successive chunks to upsert
#' @return logical - TRUE
#' 
#' @examples
#' 
#' @note
#' 
upsertQuantValuesByChunks <- function (df,chunkSize)
	{
	nChunks<-ceiling(dim(df)[1]/chunkSize)
	logInfo(paste("data will be upserted in ",nChunks," chunks",sep=""))
	for (i in 1:nChunks)
		{
		logInfo(paste(".... upserting chunk ",i," / ",nChunks,sep=""))
		upsertQuantValues(df[(1+(i-1)*chunkSize):min(dim(df)[1],i*chunkSize),])
		}
	return(TRUE)
	}



